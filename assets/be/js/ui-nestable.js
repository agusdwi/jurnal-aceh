var UINestable = function () {
    var zind = 0;
    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            if (!output) {
                console.log(output);
            }else{
                output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
                if (zind > 0) {
                    save_sort();
                };
                zind++;
            }
        } else {
            //output.val('JSON browser support required for this demo.');
        }
    };


    return {
        //main function to initiate the module
        init: function () {

            // activate Nestable for list 1
            $('#nestable_list_1').nestable({
                group: 1,
                maxDepth:2
            })
                .on('change', updateOutput);

            $('#nestable_list_11').nestable({
                group: 1,
                maxDepth:1
            })
                .on('change', updateOutput);

            // activate Nestable for list 2
            $('#nestable_list_2').nestable({
                group: 1
            })
                .on('change', updateOutput);

            // output initial serialised data
            updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));
            updateOutput($('#nestable_list_2').data('output', $('#nestable_list_2_output')));
            updateOutput($('#nestable_list_11').data('output', $('#nestable_list_11_output')));

            $('#nestable_list_menu').on('click', function (e) {
                var target = $(e.target),
                    action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });

            $('#nestable_list_3').nestable();

        }

    };

}();