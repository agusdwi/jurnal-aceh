<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('encryptkey()'))
{
	function encryptkey()
	{
		$CI =& get_instance();
		return $CI->config->item('encryption_key');
	}
}

if ( ! function_exists('is_login()'))
{
	function is_login()
	{
		$CI =& get_instance();
		return $CI->session->userdata('user_'.encryptkey());
	}
}

if ( ! function_exists('get_user()'))
{
	function get_user($a='')
	{
		$CI =& get_instance();
		$ses = array(
			'user_id'		=> $CI->session->userdata('user_id'),
			'email'			=> $CI->session->userdata('email'),
			'username' 		=> $CI->session->userdata('username'),
			'jenis' 		=> $CI->session->userdata('jenis')
		);
		if(!$CI->session->userdata('user_'.encryptkey()))
			return false;
			else{
				if($a == ''){
					return $ses;
				} else return $ses[$a];
			}
	}
}

if ( ! function_exists('login_cek()'))
{
	function login_cek($uname='',$pwd='')
	{
		$CI =& get_instance();
		$ds = $CI->db->get_where('gs_users', array('username' => $uname,'status'=>1));
		if($ds->num_rows() == 1){
			$ds = $ds->result_array();
			$ds = $ds[0];
			if(md5(encryptkey().$pwd) == $ds['password']){
				sess_register($ds);
				return true;
			}else return false;
		}else return false;
	}
}

if ( ! function_exists('sess_register()'))
{
	function sess_register($data=array())
	{
		$CI =& get_instance();
		$CI->session->set_userdata(array('user_'.encryptkey() => true));
		$CI->session->set_userdata($data);
	}
}

if ( ! function_exists('logout()'))
{
	function logout()
	{
		$CI =& get_instance();
		$CI->session->set_userdata(array('user_'.encryptkey() => false));
		$CI->session->sess_destroy();
	}
}