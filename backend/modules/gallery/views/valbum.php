<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Foto Gallery<small> mengatur foto & album</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<?if (isset($imgs)): ?>
					<li>
						<a href="<?=base_url('admin/gallery')?>">Foto Gallery</a><span class="divider">&nbsp;</span>
					</li>
					<li>
						<a href="#"><?=$album_info->album_nama;?></a> <span class="divider-last">&nbsp;</span>
					</li>	
				<?else:?>
					<li>
						<a href="#">Foto Gallery</a> <span class="divider-last">&nbsp;</span>
					</li>	
				<?endif;?>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span3">
			<div class="widget">
				<div class="widget-title">
					<h4>Album Items</h4>
				</div>
				<div class="widget-body">
					<div class="dd" id="nestable_list_1">
						<?=form_open('gallery/album_sort',array('id'=>'frm_sort','style="display:none"'))?>
							<textarea name="sort" id="nestable_list_1_output" class="m-wrap span12" style="display:none"></textarea>
						</form>
						<?=form_open('gallery/add_album',array('id'=>'frm_add'))?>
							<input type="text" name="album_nama" class="input-small">
							<button style="margin-top:-9px" type="submit" class="btn blue"><i class="icon-plus"></i> add</button>
						</form>
						<ol class="dd-list">
							<?foreach ($album->result() as $key): ?>
								<li class="dd-item" data-id="<?=$key->album_id;?>">
									<div class="dd-handle"></div>
									<a class="hh <?=(isset($imgs) ? (($key->album_id == $album_info->album_id) ? 'mactive' : '') : '');?>" href="<?=base_url('admin/gallery/show/'.$key->album_id)?>"><?=$key->album_nama;?><span uid="<?=$key->album_id;?>" class="deletemenu">X</span></a>
									<div class="clearfix"></div>
								</li>
							<?endforeach;?>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<div class="span9">
			<div class="widget">
				<div class="widget-title">
					<h4>List Foto <?=(isset($imgs)) ? '[ '.$album_info->album_nama.' ]' : '';?></h4>
				</div>
				<?if (isset($imgs)): ?>
					<div style="background:#fbfbfb;border-bottom:1px solid #E5E5E5;padding:10px 20px">
						<center><b>Upload Foto</b></center>
						<?=form_open_multipart('gallery/add',array('id'=>"frm_gallery",'style'=>"width:630px"))?>
							<input type="hidden" name="ds[album_id]" value="<?=$album_info->album_id;?>">
							<div class="fileupload fileupload-new" data-provides="fileupload" style="float:left;margin-right:5px">
								<input type="hidden" value="" name="">
								<div class="input-append">
									<div class="uneditable-input">
										<i class="icon-file fileupload-exists"></i>
										<span class="fileupload-preview"></span>
									</div>
									<span class="btn btn-file">
										<span class="fileupload-new">Select file</span>
										<span class="fileupload-exists">Change</span>
										<input type="file" class="default" name="userfile">
									</span>
									<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
								</div>
							</div>
							<input type="text" placeholder="deskripsi" name="ds[gallery_description]" class="input-large">
							<button style="margin-top:-10px" type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
						</form>
					</div>
					<div class="widget-body">
						<div class="row-fluid">
							<?foreach ($imgs->result() as $key): ?>
								<div class="span2">
									<div class="thumbnail">
										<div class="item">
											<a class="fancybox-button" data-rel="fancybox-button" title="<?=$key->gallery_description;?>" href="<?=base_url()?>media/gallery/<?=$key->gallery_url;?>">
												<div class="zoom">
													<img src="<?=base_url()?>media/gallery/thumbs/<?=$key->gallery_url;?>" alt="<?=$key->gallery_description;?>" />
													<div class="zoom-icon"></div>
												</div>
												<a href="<?=base_url('admin/gallery/delete/'.$key->gallery_id)?>" class="delete_image">
													<i class="icon-remove-sign"></i>
												</a>
											</a>
										</div>
									</div>
								</div>
							<?endforeach;?>
						</div>
					</div>
				<?else:?>
					<center><h4>Silahkan pilih album</h4></center><br>
				<?endif;?>	
			</div>
		</div>
	</div>
</div>
<script src="<?=base_url()?>assets/be/assets/nestable/jquery.nestable.js"></script>
<script src="<?=base_url()?>assets/be/js/ui-nestable-album.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/be/assets/nestable/jquery.nestable.css">	
<script type="text/javascript">
	$(function(){
		UINestable.init();
		$(".delete_image").click(function(){
			return confirm('apakah anda yakin menghapus data ini ?');
		})
		$("#frm_add").validate({
			rules: {album_nama	: "required"},
			messages: {album_nama	: ""}
		});
		$(".deletemenu").die('click').live('click',function(){
			if(confirm('apakah anda yakin menghapus album ini ?')){
				window.location="<?=base_url()?>admin/gallery/delete_album/"+$(this).attr('uid');
				return false;
			}else return false;
		})
	})

	function save_sort(){
		var url  = $("#frm_sort").attr('action');
		var data = $("#frm_sort").serialize();
		$.post(url,data, function(data) {});
	}
</script>
<style type="text/css">
	label.error{
		display: none !important;
	}
	.hh:hover .deletemenu{
		opacity: 1;
	}
	.deletemenu{
		opacity: 0;
		position: absolute;
		z-index: 999;
		right: 5px;
		top: 6px;
		font-weight: bold;
		color: #e74955;
	}
</style>