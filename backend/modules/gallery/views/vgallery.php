<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Front Page Slideshow<small> mengatur slideshow halaman depan</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Front Page Slideshow</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>List Slideshow</h4>
				</div>
				<div style="background:#fbfbfb;border-bottom:1px solid #E5E5E5;padding:10px 20px">
					<center><b>Upload Slideshow</b></center>
					<?=form_open_multipart('slideshow/add',array('id'=>"frm_gallery"))?>
						<div class="fileupload fileupload-new" data-provides="fileupload" style="float:left;margin-right:5px">
							<input type="hidden" value="" name="">
							<div class="input-append">
								<div class="uneditable-input">
									<i class="icon-file fileupload-exists"></i>
									<span class="fileupload-preview"></span>
								</div>
								<span class="btn btn-file">
									<span class="fileupload-new">Select file</span>
									<span class="fileupload-exists">Change</span>
									<input type="file" class="default" name="userfile">
								</span>
								<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
							</div>
						</div>
						<input type="text" placeholder="link" name="ds[ss_link]" class="input-medium">
						<input type="text" placeholder="deskripsi" name="ds[ss_description]" class="input-large">
						<button style="margin-top:-10px" type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
					</form>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<?foreach ($ss->result() as $key): ?>
							<div class="span2">
								<div class="thumbnail">
									<div class="item">
										<a class="fancybox-button" data-rel="fancybox-button" title="<?=$key->ss_description;?>" href="<?=base_url()?>media/slideshow/<?=$key->ss_url;?>">
											<div class="zoom">
												<img src="<?=base_url()?>media/slideshow/thumbs/<?=$key->ss_url;?>" alt="<?=$key->ss_description;?>" />
												<div class="zoom-icon"></div>
											</div>
											<a href="<?=base_url('admin/slideshow/delete/'.$key->ss_id)?>" class="delete_image">
												<i class="icon-remove-sign"></i>
											</a>
										</a>
									</div>
								</div>
							</div>
						<?endforeach;?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$(".delete_image").click(function(){
			return confirm('apakah anda yakin menghapus data ini ?');
		})
	})
</script>