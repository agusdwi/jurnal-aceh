<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {	
	
	var $width 	= "800";
	var $heigth	= "480";
	var $path	= "media/gallery";

	function index(){
		$data['main']	= "valbum";
		$data['cur']	= "mn_6";
		$data['album']	= $this->db->order_by('album_sort')->get('gs_gallery_album');
		$this->load->view('template',$data);
	}

	public function show($id){
		$data['main']	= "valbum";
		$data['cur']	= "mn_6";
		$data['imgs']	= $this->db->get_where('gs_gallery',array('album_id'=>$id));
		$data['album']	= $this->db->order_by('album_sort')->get('gs_gallery_album');
		$data['album_info']	= $this->db->get_where('gs_gallery_album',array('album_id'=>$id))->row();
		$this->load->view('template',$data);
	}

	function add(){
		$_FILES['userfile']['name']	= strtolower($_FILES['userfile']['name']);
		$config['upload_path']		= $this->path;
		$config['allowed_types']	= 'jpg';
		$config['max_size']			= '10000';
		$config['max_width']		= '10000';
		$config['max_height']		= '6000';
		$config['encrypt_name']		= true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload()){
			$this->session->set_flashdata('message',"Gambar gagal di upload, gambar harus bertipe jpg dengan ukuran $this->width px x $this->heigth px");
			$this->session->set_flashdata('log',$this->upload->display_errors());
			redirect(base_url('admin/slideshow'));
		}else{
			$a = $this->upload->data();
			$this->load->library('image_moo');
			$this->image_moo
				->load($this->path.'/'.$a['file_name'])
				->resize($this->width,$this->heigth)
				->save($this->path."/".$a['file_name'],true)
				->load($this->path.'/'.$a['file_name'])
				->resize_crop(157,117)
				->save($this->path."//thumbs/".$a['file_name'],true);
			
			$data = $this->input->post('ds');
			$data['gallery_url'] = $a['file_name'];
			$this->db->insert('gs_gallery',$data);
			$this->session->set_flashdata('message','Gambar berhasil di upload');
			redirect($_SERVER['HTTP_REFERER']);
		}	
	}

	function delete($id){
		$file = $this->db->get_where('gs_gallery',array('gallery_id'=>$id))->row();
		unlink($this->path."/".$file->gallery_url);
		unlink($this->path.'//thumbs/'.$file->gallery_url);
		$this->db->delete('gs_gallery', array('gallery_id' => $id)); 
		$this->session->set_flashdata('message','Gambar berhasil di hapus');
		redirect($_SERVER['HTTP_REFERER']);	
	}

	function add_album(){
		$data = $this->input->post();
		$this->db->insert('gs_gallery_album',$data);

		$data = array('album_sort' =>  $this->db->insert_id());
		$this->db->where('album_id', $this->db->insert_id());
		$this->db->update('gs_gallery_album', $data); 

		$this->session->set_flashdata('message','Album berhasil di simpan');
		redirect($_SERVER['HTTP_REFERER']);
	}

	function delete_album($id){
		$this->db->delete('gs_gallery_album', array('album_id' => $id)); 
		$this->session->set_flashdata('message','Album berhasil di hapus');
		redirect($_SERVER['HTTP_REFERER']);			
	}

	function album_sort(){
		$data = json_decode($this->input->post('sort'));
		$i = 0;
		foreach ($data as $key ) {
			$i++;
			$data = array('album_sort' => $i );
			$this->db->where('album_id', $key->id);
			$this->db->update('gs_gallery_album', $data); 
		}
	}
}