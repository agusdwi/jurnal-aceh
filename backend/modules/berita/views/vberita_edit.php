<?=tiny_mce('mce');?>
<?
	$cnfg = $this->config->item('blog_img');
?>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Berita<small> mengatur kabar berita terbaru website</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="<?=base_url('admin/berita')?>">Berita</a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Edit Berita</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>Form Edit Berita</h4>
				</div>
				<div class="widget-body">
					<?=form_open_multipart('',array("class"=>"form-horizontal"))?>
						<div class="control-group">
							<label class="control-label">Judul Berita</label>
							<div class="controls">
								<input type="text" class="input-xlarge" name="berita_judul" id="berita_judul" value="<?=$ds['info']->berita_judul;?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Gambar</label>
							<div class="controls">
								<img width="65" height="65" style="margin-right:10px" class="pull-left" src="<?=base_url()?>media/blog/thumbs/<?=$ds['info']->berita_img;?>">
								<div class="pull-left fileupload fileupload-new" data-provides="fileupload" style="float:left;margin-right:5px">
									<input type="hidden" value="" name="">
									<div class="input-append">
										<div class="uneditable-input">
											<i class="icon-file fileupload-exists"></i>
											<span class="fileupload-preview"></span>
										</div>
										<span class="btn btn-file">
											<span class="fileupload-new">Select file</span>
											<span class="fileupload-exists">Change</span>
											<input type="file" class="default" name="userfile">
										</span>
										<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
									</div>
									<br>*gambar berukuran <?=$cnfg[0];?>px ,<?=$cnfg[1];?>px bertipe .jpg
								</div>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Meta Keyword</label>
							<div class="controls">
								<input type="text" class="input-xlarge" name="berita_keyword" id="berita_keyword" value="<?=$ds['info']->berita_keyword;?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Meta Description</label>
							<div class="controls">
								<input type="text" class="input-xxlarge" name="berita_description" id="berita_description" value="<?=$ds['info']->berita_description;?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Kategori Berita</label>
							<div class="controls">
								<ul id="myULTags">
									<?foreach ($ds['tags']->result() as $key): ?>
										<li><?=$key->kategori;?></li>
									<?endforeach;?>
								</ul>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Isi Berita</label>
							<div class="controls">
								<textarea class="mce" name="berita_isi"><?=$ds['info']->berita_isi;?></textarea>
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
							<a href="<?=base_url('admin/berita')?>" class="btn"><i class=" icon-remove"></i> Cancel</a>
						</div>
					</form>
				</div>
			</div>	
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		var sampleTags = <?=$tag;?>;
		$('#myULTags').tagit({
			availableTags: sampleTags,
           	itemName: 'item',
			fieldName: 'tags[]',
			allowSpaces: true,
			
       	});
       	$("form").validate({
       		rules: {
       			berita_judul		: "required",
       			berita_keyword		: "required",
       			berita_description	: "required"
       		}
       	});
	})		
</script>