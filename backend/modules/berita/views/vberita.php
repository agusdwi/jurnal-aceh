<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Berita<small> mengatur kabar berita terbaru website</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Berita</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>List Berita</h4>
				</div>
				<div class="widget-body">
					<div class="pull-left" style="opacity:0">
						<?=form_open(base_url('admin/berita/search'))?>
							<label>Search: <input type="text" class="input-medium" name="q"></label>
						</form>
					</div>
					<div class="pull-right">
						<a class="btn btn-success btn-mini" href="<?=base_url('admin/berita/add')?>"><i class="icon-plus"></i> Tambah Berita</a>
					</div>
					<div class="clearfix"></div>
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>No</th>
								<th>Judul</th>
								<th>Deskripsi</th>
								<th>Tanggal</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?$i=$this->page;foreach ($ds->result() as $key): $i++;?>
								<tr>
									<td><?=$i;?></td>
									<td style="font-weight:bold">
										<img class="pull-left" src="<?=base_url()?>media/blog/thumbs/<?=$key->berita_img;?>" width="65" heigh="65">
										<div class="pull-left" style="padding-left:10px">
											<?=$key->berita_judul;?>
										</div>
									</td>
									<td><?=smart_trim($key->berita_isi,50)?></td>
									<td><?=format_date_time($key->berita_date,false);?></td>
									<td class="tcenter" style="width:110px">
										<a href="<?=base_url()?>admin/berita/edit/<?=$key->berita_id;?>" class="edit btn btn-mini purple tooltips" title="ubah data"><i class="icon-edit"></i> Edit</a>
										<a href="<?=base_url()?>admin/berita/delete/<?=$key->berita_id;?>" class="edit btn btn-danger btn-mini purple tooltips delete" title="hapus data"><i class="icon-trash"></i> Delete</a>
									</td>
								</tr>
							<?endforeach;?>
						</tbody>
					</table>
					<div class="pull-right pagin">
						<?=$pagin;?>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>	
		</div>
	</div>
</div>