<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berita extends CI_Controller {	
	
	var $perpage = 10;

	function index($page="0"){
		$this->load->library('pagination');

		$config['base_url'] 	= base_url().'admin/berita/index/';
		$config['total_rows'] 	= $this->db->get('gs_berita')->num_rows();
		$config['per_page'] 	= $this->perpage; 

		$this->pagination->initialize($config); 

		$data['main']	= "vberita";
		$data['cur']	= "mn_6";
		$data['pagin']	= $this->pagination->create_links();
		$data['ds']		= $this->db->order_by('berita_id','desc')->limit($this->perpage,$page)->get('gs_berita');
		$this->page = $page;
		$this->load->view('template',$data);
	}

	function add(){
		if (is_post()) {
			$upload = $this->upload();
			if (!$upload[0]){
				$this->session->set_flashdata('log',$upload[1]);
				$this->session->set_flashdata('message','Maaf gambar gagal di upload, hanya menerima gambar format jpg');
				redirect(base_url('admin/berita/add'));
			}
			$data = $this->input->post();
			$data['berita_date'] = DATE('Y-m-d');
			$tags = $data['tags'];
			unset($data['tags']);
			$data['berita_img'] = $upload[1]['file_name'];
			$data['berita_link'] = get_unique($data['berita_judul'],'gs_berita','berita_link');
			$this->db->insert('gs_berita',$data);

			$id = $this->db->insert_id();

			$this->save_tag($tags,$id);

			$this->session->set_flashdata('message','Berita baru berhasil di simpan');
			redirect(base_url('admin/berita'));
		}
		$data['main']	= "vberita_add";
		$data['cur']	= "mn_6";
		$data['tag']	= $this->get_tag();
		$this->load->view('template',$data);
	}

	function edit($id){
		if (is_post()) {
			$data = $this->input->post();
			if (!empty($_FILES['userfile']['name'])) {
				$upload = $this->upload();
				if (!$upload[0]){
					$this->session->set_flashdata('log',$upload[1]);
					$this->session->set_flashdata('message','Maaf gambar gagal di upload, hanya menerima gambar format jpg');
					redirect(base_url('admin/berita/edit/'.$id));
				}
				$data['berita_img'] = $upload[1]['file_name'];
			}
			
			$tags = $data['tags'];
			unset($data['tags']);
			
			$data['berita_link'] = get_unique_edit($data['berita_judul'],'gs_berita','berita_link','berita_id',$id);
			
			$this->db->where('berita_id', $id);
			$this->db->update('gs_berita', $data); 

			$this->save_tag($tags,$id);

			$this->session->set_flashdata('message','Berita berhasil di perbarui');
			redirect(base_url('admin/berita'));
		}
		$data['main']	= "vberita_edit";
		$data['cur']	= "mn_6";
		$data['tag']	= $this->get_tag();
		$data['ds']		= array(
				'info' 	=> $this->db->get_where('gs_berita',array('berita_id'=>$id))->row(),
				'tags'	=> $this->db->get_where('v_b_kat',array('berita_id'=>$id))
			);
		$this->load->view('template',$data);
	}

	function save_tag($tags,$id){
		$this->db->delete('gs_berita_kategori', array('berita_id' => $id)); 
		$d = array();
		foreach ($tags as $key) {
			$d[] = array(
				'berita_id'	=> $id,
				'kategori'	=> $key
			);
		}
		$this->db->insert_batch('gs_berita_kategori',$d);
	}

	function get_tag(){
		$ds = $this->db->get('v_b_kat');
		$d = array();
		foreach ($ds->result() as $key) {
			$d[] = $key->kategori;
		}
		return json_encode($d);
	}

	function upload(){
		$_FILES['userfile']['name']	= strtolower($_FILES['userfile']['name']);
		$config['upload_path']		= 'media/blog';
		$config['allowed_types']	= 'jpg';
		$config['max_size']			= '10000';
		$config['max_width']		= '5000';
		$config['max_height']		= '5000';
		$config['encrypt_name']		= true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload()){
			return array(false,$this->upload->display_errors());
		}else{
			$cnfg = $this->config->item('blog_img');
			$bcnfg = $this->config->item('blog_thumb_img');

			$a = $this->upload->data();
			$this->load->library('image_moo');
			$this->image_moo
				->load('media/blog/'.$a['file_name'])
				->resize_crop($cnfg[0],$cnfg[1])
				->save('media/blog/'.$a['file_name'],true)
				->load('media/blog/'.$a['file_name'])
				->resize_crop($bcnfg[0],$bcnfg[1])
				->save('media/blog/thumbs/'.$a['file_name'],true);
			return array(true,$a);
		}
	}

	function delete($id){
		$this->db->delete('gs_berita', array('berita_id' => $id)); 
		$this->session->set_flashdata('message','berita berhasil dihapus');
		redirect(base_url('admin/berita'));

	}
}