<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Testimonial<small> mengatur testi dari pengunjung</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="<?=base_url('admin/testi')?>">Testimonial</a> <span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Detail</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>Detail Testimonial</h4>
				</div>
				<div class="widget-body">
					<div class="control-group">
						<label class="control-label">Nama</label>
						<div class="controls">
							<input type="text" class="input-xlarge" name="art_judul" id="art_judul" value="<?=$ds->t_name;?>" readonly="readonly">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal</label>
						<div class="controls">
							<input type="text" class="input-large" name="art_keyword" id="art_keyword" value="<?=pretty_date($ds->t_date);?>" readonly="readonly">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Company</label>
						<div class="controls">
							<input type="text" class="input-large" name="art_keyword" id="art_keyword" value="<?=$ds->t_company;?>" readonly="readonly">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Testimonial</label>
						<div class="controls">
							<textarea class="input-xxlarge" rows="5"  name="art_content" readonly="readonly"><?=$ds->t_msg;?></textarea>
						</div>
					</div>
					<div class="form-actions">
						<a href="<?=base_url('admin/testi')?>" class="btn"><i class=" icon-arrow-left"></i> index testi</a>
						<a href="<?=base_url()?>admin/testi/delete/<?=$ds->t_id;?>" class="btn btn-danger delete"><i class=" icon-trash"></i> delete testi</a>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>