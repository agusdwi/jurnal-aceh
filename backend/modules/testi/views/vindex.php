<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Testimonial<small> mengatur testi dari pengunjung</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Testimonial</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>List Testi</h4>
				</div>
				<div class="widget-body">
					<table class="table table-striped table-bordered" id="tb_artikel">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Company</th>
								<th>Testi</th>
								<th>Tanggal</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?$i=0;foreach ($ds->result() as $key): $i++;?>
								<tr class="<?=($key->t_status == 0)?'unread': '' ;?>">
									<td><?=$i;?></td>
									<td><?=$key->t_name;?></td>
									<td><?=$key->t_company;?></td>
									<td><?=smart_trim($key->t_msg,100)?></td>
									<td><?=pretty_date($key->t_date);?></td>
									<td class="tcenter" style="width:120px">
										<a href="<?=base_url()?>admin/testi/detail/<?=$key->t_id;?>" class="edit btn btn-mini purple tooltips" title="detail pesan"><i class="icon-arrow-right"></i> Detail</a>
										<a href="<?=base_url()?>admin/testi/delete/<?=$key->t_id;?>" class="edit btn btn-danger btn-mini purple tooltips delete" title="hapus data"><i class="icon-trash"></i> Delete</a>
									</td>
								</tr>
							<?endforeach;?>
						</tbody>
					</table>
					<div class="clearfix"></div>
				</div>
			</div>	
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		// begin first table
		$('#tb_artikel').dataTable({
			"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"sPaginationType": "bootstrap",
			"oLanguage": {
				"sLengthMenu": "_MENU_ records per page",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next"
				}
			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': [1]
			}]
		});
	})
</script>
<style type="text/css">
	tr.unread td{
		background: #fdffa8 !important;
	}
</style>