<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testi extends CI_Controller {	
	
	var $perpage = 10;

	function index(){
		$data['main']	= "vindex";
		$data['cur']	= "mn_9";
		$data['ds']		= $this->db->order_by('t_id','desc')->get('app_testi');
		$this->load->view('template',$data);
	}

	function detail($id){
		$data = array('t_status' => 1 );
		$this->db->where('t_id', $id);
		$this->db->update('app_testi', $data); 
		

		$data['main']	= "vdetail";
		$data['cur']	= "mn_9";
		$data['ds']		= $this->db->get_where('app_testi',array('t_id'=>$id))->row();
		$this->load->view('template',$data);
	}

	function delete($id){
		$this->db->delete('app_testi', array('t_id' => $id)); 
		$this->session->set_flashdata('message','Message berhasil di hapus');
		redirect(base_url('admin/testi'));
	}
}