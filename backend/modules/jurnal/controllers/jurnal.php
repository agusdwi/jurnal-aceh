<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jurnal extends CI_Controller {	

	function __construct(){
		parent::__construct();
		$this->table = "jr_jurnal";
		$this->tview = "v_jurnal";
	}

	function redirect(){
		redirect(base_url('admin/jurnal'));
	}

	function index($state=''){
		$criteria = array('status'=>1);
		if (isset($_GET['kategori'])) {
			$criteria = array('status'=>1,'kategori_id'=>$_GET['kategori']);
		}else $_GET['kategori'] = '';

		$data['cur']			= "mn_4";
		$data['main']			= "jurnal_index";
		$data['ds']				= $this->db->order_by('tanggal','desc')->get_where($this->tview,$criteria);
		$data['ktg']			= $this->db->order_by('sort')->get_where('jr_kategori',array('status'=>1));
		$this->load->view('template',$data);	
	}

	function detail($id){
		$data['cur']			= "mn_4";
		$data['main']			= "jurnal_detail";
		$data['ds']				= $this->db->get_where($this->tview,array('id'=>$id))->row();
		$data['user']			= $this->db->get_where('jr_users',array('id'=>$data['ds']->user_id))->row();
		$this->load->view('template',$data);	
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		$this->redirect();
	}
}