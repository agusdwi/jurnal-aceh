<style type="text/css">
	.control-label{
		width: 80px !important;
	}
	.controls{
		margin-left: 80px !important;
	}
</style>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Pengaturan Jurnal<small> mengatur jurnal mahasiswa / dosen</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Jurnal</a> <span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Detail</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>Detail Jurnal</h4>
				</div>
				<div class="widget-body" style="position:relative">
					<div class="row-fluid">
						<div class="span7">
							<h4>Preview Jurnal</h4>
							<a style="float: right;margin-top: -30px;" target="_blank" href="http://localhost/file/Modul%20web%20jurnal%20gizi_Spesifikasi%20System%20(1).pdf" class="edit btn btn-mini purple tooltips" title="download jurnal"><i class="icon-download"></i> Download</a>
							<iframe width="100%" height="600" src="http://localhost/file/Modul%20web%20jurnal%20gizi_Spesifikasi%20System%20(1).pdf"></iframe>
						</div>
						<div class="span5">
							<h4>Data Jurnal</h4>
							<hr>
							<form action="#" class="form-horizontal">
								<div class="control-group">
									<label class="control-label">Judul</label>
									<div class="controls"><?=$ds->judul;?></div>
								</div>
								<div class="control-group">
									<label class="control-label">Kategori</label>
									<div class="controls"><?=$ds->kategori;?></div>
								</div>
								<div class="control-group">
									<label class="control-label">Terbitan</label>
									<div class="controls"><?=$ds->terbitan;?></div>
								</div>
								<div class="control-group">
									<label class="control-label">Tgl Upload</label>
									<div class="controls"><?=format_date_time($ds->tanggal);?></div>
								</div>
								<br>
								<h4>Data Penulis</h4>
								<hr>
								<div style="position:relative">
									<img width="100" src="<?=base_url()?>media/avatar/<?=(($user->avatar == "")?'blank.jpg':$ds->avatar);?>" style="position:absolute;right:0px;top:0px">
									<div class="control-group">
										<label class="control-label">Nama</label>
										<div style="margin-right:100px;" class="controls"><?=$user->nama;?></div>
									</div>
									<div class="control-group">
										<label class="control-label">Email</label>
										<div style="margin-right:100px;" class="controls"><?=$user->email;?></div>
									</div>
									<div class="control-group">
										<label class="control-label">Tipe</label>
										<div style="margin-right:100px;" class="controls"><?=$user->type;?></div>
									</div>
								</div>
							</form>
							
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>