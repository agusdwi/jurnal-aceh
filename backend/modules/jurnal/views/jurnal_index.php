<style type="text/css">
	#kategori_select{
		height: auto;
		width: 200px;
		float: left;
	}
	#kategori{
		position: absolute;
		margin-left: 230px;
	}
	#kategori label{
		float: left;
	}
</style>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Pengaturan Jurnal<small> mengatur jurnal mahasiswa / dosen</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Jurnal</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>Daftar Jurnal</h4>
				</div>
				<div class="widget-body" style="position:relative">
					<div id="kategori">
						<label>kategori:</label>
						<select id="kategori_select">
							<option value="0">semua kategori</option>
							<?foreach ($ktg->result() as $key): ?>
								<option <?=(($key->id == $_GET['kategori'])? 'selected="selected"' : '' );?> value="<?=$key->id;?>"><?=$key->nama;?></option>
							<?endforeach;?>
						</select>
					</div>
					<table class="table table-striped table-bordered def_table">
						<thead>
							<tr>
								<th style="width:8px;">No</th>
								<th>Judul</th>
								<th>Kategori</th>
								<th>Penulis</th>
								<th>Terbitan</th>
								<th>Tgl Upload</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?$i=0;foreach ($ds->result() as $key):$i++; ?>
							<tr>
								<td><?=$i;?></td>
								<td><?=$key->judul;?></td>
								<td><?=$key->kategori;?></td>
								<td><?=$key->penulis;?></td>
								<td><?=$key->terbitan;?></td>
								<td><?=format_date_time($key->tanggal);?></td>
								<td class="tcenter" style="width:120px">
									<a href="<?=base_url()?>admin/jurnal/detail/<?=$key->id;?>" class="edit btn btn-mini purple tooltips" title="detail jurnal"><i class="icon-edit"></i> Detail</a>
									<a href="<?=base_url('admin/jurnal/delete')?>/<?=$key->id;?>" class="edit btn btn-danger btn-mini purple tooltips delete" title="hapus jurnal"><i class="icon-trash"></i> Delete</a>
								</td>
							</tr>
							<?endforeach;?>
						</tbody>
					</table>				
					
				</div>
			</div>	
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('#kategori_select').change(function (e) {
			e.preventDefault();
			var url = "";
			if ($(this).val() != 0) {
				url = "<?=base_url();?>admin/jurnal?kategori="+$(this).val();
			}else{
				url = "<?=base_url();?>admin/jurnal";
			}
			window.location = url;
		})
	})
</script>