<html lang="en">
<head>
	<title>elFinder</title>
	<link rel="stylesheet" href="<?=base_url()?>assets/editor/finder/css/smoothness/jquery-ui-1.8.13.custom.css" type="text/css" media="screen" title="no title" charset="utf-8">
	<link rel="stylesheet" href="<?=base_url()?>assets/editor/finder/css/elfinder.css" type="text/css" media="screen" title="no title" charset="utf-8">

	<script src="<?=base_url()?>assets/editor/finder/js/jquery-1.6.1.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url()?>assets/editor/finder/js/jquery-ui-1.8.13.custom.min.js" type="text/javascript" charset="utf-8"></script>

	<script src="<?=base_url()?>assets/editor/finder/js/elfinder.min.js" type="text/javascript" charset="utf-8"></script>

	<script type="text/javascript" charset="utf-8">
		$().ready(function() {
			
			var f = $('#finder').elfinder({
				url : '<?=base_url()?>assets/editor/finder/connectors/php/connector.php?url=<?=base_url()?>',
				lang : 'en',
				docked : true,
				editorCallback : function(url) {
					window.tinymceFileWin.document.forms[0].elements[window.tinymceFileField].value = url;
					window.tinymceFileWin.focus();
					window.close();
				}

				// dialog : {
				// 	title : 'File manager',
				// 	height : 500
				// }

				// Callback example
				//editorCallback : function(url) {
				//	if (window.console && window.console.log) {
				//		window.console.log(url);
				//	} else {
				//		alert(url);
				//	}
				//},
				//closeOnEditorCallback : true
			})
			// window.console.log(f)
		})
	</script>

</head>
<body>
	<div id="finder">finder</div>
</body>
</html>