<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori extends CI_Controller {	
	
	var $sort = 0;	

	function __construct(){
		parent::__construct();
		$this->load->library('Kategori_nest');
		$this->load->library('Prodi_nest');
	}

	public function index(){
		$data['main']	= "kategori_index";
		$data['cur']	= "mn_3";
		$data['menu']	= $this->kategori_nest->get();
		$data['prodi']	= $this->prodi_nest->get();
		$this->load->view('template',$data);
	}

	function add(){
		$data['nama'] = $this->input->post('nama');
		$this->db->insert('jr_kategori',$data);
		$data = array('sort' =>  $this->db->insert_id());
		$this->db->where('id', $this->db->insert_id());
		$this->db->update('jr_kategori', $data); 
		
		$this->session->set_flashdata('message','menu berhasil di simpan');
		redirect(base_url('admin/kategori'));
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('id', $id);
		$this->db->update('jr_kategori', $data);
		$this->session->set_flashdata('message','kategori berhasil dihapus');
		redirect(base_url('admin/kategori'));
	}

	function save_sort(){
		$menu = json_decode($this->input->post('sort'));
		$this->save_tree($menu);
	}

	function save_tree($menu,$parent=0) {
		foreach ($menu as $key) {
			$this->sort++;
			
			$data = array(
			               'parent_id' 	=>  $parent,
			               'sort' 	=>  $this->sort
			            );
			$this->db->where('id', $key->id);
			$this->db->update('jr_kategori', $data); 

			if (isset($key->children)) {
				$this->save_tree($key->children,$key->id);
			}
		}
	}

	function update($id,$nama){
		$data = array(
		               'nama' =>  $nama
		            );
		$this->db->where('id', $id);
		$this->db->update('jr_kategori', $data); 
		echo 'true';
	}

	function add_prodi(){
		$data['nama'] = $this->input->post('nama');
		$this->db->insert('jr_prodi',$data);
		$data = array('sort' =>  $this->db->insert_id());
		$this->db->where('id', $this->db->insert_id());
		$this->db->update('jr_prodi', $data); 
		
		$this->session->set_flashdata('message','menu berhasil di simpan');
		redirect(base_url('admin/kategori'));
	}

	function delete_prodi($id){
		$data = array('status' => 0);
		$this->db->where('id', $id);
		$this->db->update('jr_prodi', $data);
		$this->session->set_flashdata('message','kategori berhasil dihapus');
		redirect(base_url('admin/kategori'));
	}

	function save_sort_prodi(){
		$menu = json_decode($this->input->post('sort'));
		$this->save_tree_prodi($menu);
	}

	function save_tree_prodi($menu,$parent=0) {
		foreach ($menu as $key) {
			$this->sort++;
			
			$data = array(
			               'parent_id' 	=>  $parent,
			               'sort' 	=>  $this->sort
			            );
			$this->db->where('id', $key->id);
			$this->db->update('jr_prodi', $data); 

			if (isset($key->children)) {
				$this->save_tree_prodi($key->children,$key->id);
			}
		}
	}

	function update_prodi($id,$nama){
		$data = array(
		               'nama' =>  $nama
		            );
		$this->db->where('id', $id);
		$this->db->update('jr_prodi', $data); 
		echo 'true';
	}
}