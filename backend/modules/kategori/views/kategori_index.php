<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Kategori & Prodi<small> mengatur kategori & prodi</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Kategori & Prodi</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4">
			<div class="widget">
				<div class="widget-title">
					<h4>Daftar Kategori</h4>
				</div>
				<div class="widget-body">
					<div class="dd" id="nestable_list_1">
						<?=form_open('kategori/save_sort',array('id'=>'frm_sort','style="display:none"'))?>
							<textarea name="sort" id="nestable_list_1_output" class="m-wrap span12" style="display:none"></textarea>
						</form>
						<?=form_open('kategori/add',array('id'=>'frm_add'))?>
							<input type="text" name="nama"><button style="margin-top:-9px" type="submit" class="btn blue"><i class="icon-plus"></i> add</button>
						</form>
						<?=$menu;?>
						<br><i>Geser kategori untuk mengurutkan & membuat sub kategori</i>
					</div>
				</div>
			</div>
		</div>

		<div class="span4">
			<div class="widget">
				<div class="widget-title">
					<h4>Daftar Prodi</h4>
				</div>
				<div class="widget-body">
					<div class="dd" id="nestable_list_11">
						<?=form_open('kategori/save_sort_prodi',array('id'=>'frm_sort_prodi','style="display:none"'))?>
							<textarea name="sort" id="nestable_list_11_output" class="m-wrap span12" style="display:none"></textarea>
						</form>
						<?=form_open('kategori/add_prodi',array('id'=>'frm_add_prodi'))?>
							<input type="text" name="nama"><button style="margin-top:-9px" type="submit" class="btn blue"><i class="icon-plus"></i> add</button>
						</form>
						<?=$prodi;?>
						<br><i>Geser kategori untuk mengurutkan & membuat sub kategori</i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?=base_url()?>assets/be/assets/nestable/jquery.nestable.js"></script>
<script src="<?=base_url()?>assets/be/js/ui-nestable.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/be/assets/nestable/jquery.nestable.css">	
<script type="text/javascript">
	$(function(){
		UINestable.init();
		$(".hh").die('click').live('click',function(){
			$('.mactive').removeClass('mactive')
			$(this).addClass('mactive');
			var id = $(this).parent().attr('data-id');
			$("#def_body").fadeOut(function(){
				$("#pil_body").fadeIn();
			})
			return false;
		})
		$("#frm_add").validate({
			rules: {nama	: "required"},
			messages: {nama	: ""}
		});
		$("#frm_add_prodi").validate({
			rules: {nama	: "required"},
			messages: {nama	: ""}
		});
		$(".deletemenu").die('click').live('click',function(){
			if(confirm('apakah anda yakin menghapus menu ini ?')){
				window.location="<?=base_url()?>admin/kategori/delete/"+$(this).attr('uid');
				return false;
			}else return false;
		})

		$(".editmenu").die('click').live('click',function(){
			var id = $(this).attr('uid');
			var nm = $(this).attr('uname');
			var ktg=prompt("Edit Kategori",nm);

			if (ktg!=null){
				$.get("<?=base_url('admin/kategori/update')?>/"+id+"/"+ktg, function(data) {
					if (data == "true") {
						window.location = "";
					};
				});
			}
		})

		$(".deletemenuprodi").die('click').live('click',function(){
			if(confirm('apakah anda yakin menghapus menu ini ?')){
				window.location="<?=base_url()?>admin/kategori/delete_prodi/"+$(this).attr('uid');
				return false;
			}else return false;
		})

		$(".editmenuprodi").die('click').live('click',function(){
			var id = $(this).attr('uid');
			var nm = $(this).attr('uname');
			var ktg=prompt("Edit Kategori",nm);

			if (ktg!=null){
				$.get("<?=base_url('admin/kategori/update_prodi')?>/"+id+"/"+ktg, function(data) {
					if (data == "true") {
						window.location = "";
					};
				});
			}
		})
	})
	function save_sort(){
		var url  = $("#frm_sort").attr('action');
		var data = $("#frm_sort").serialize();
		$.post(url,data, function(data) {});

		var url  = $("#frm_sort_prodi").attr('action');
		var data = $("#frm_sort_prodi").serialize();
		$.post(url,data, function(data) {});
	}
</script>
<style type="text/css">
	label.error{
		display: none !important;
	}
	.hh:hover .deletemenu,.hh:hover .deletemenuprodi{
		opacity: 1;
	}
	.hh:hover .editmenu,.hh:hover .editmenuprodi{
		opacity: 1;
	}
	.deletemenu,.deletemenuprodi{
		opacity: 0;
		position: absolute;
		z-index: 999;
		right: 5px;
		top: 6px;
		font-weight: bold;
		color: #e74955;
	}
	.editmenu,.editmenuprodi{
		opacity: 0;
		position: absolute;
		z-index: 999;
		right: 15px;
		top: 10px;
		font-weight: bold;
		color: green;	
	}
</style>