<div style="padding:5px 20px">
	<?=form_open('',array("class"=>"form-horizontal"))?>
		<div class="control-group">
			<label class="control-label">Meta Keywod <br><small>Separate by comma</small></label>
			<div class="controls">
				<input type="text" class="input-xxlarge" name="cog_value5" value="<?=$key;?>">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Meta Description <br></label>
			<div class="controls">
				<textarea name="cog_value6" class="input-xxlarge" rows="5"><?=$des;?></textarea>
			</div>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
		</div>
	</form>
</div>