<?=tiny_mce('mce');?>
<div style="padding:5px 20px">
	<?=form_open('',array("class"=>"form-horizontal"))?>
		<div class="control-group">
			<label class="control-label">Quote</label>
			<div class="controls">
				<input type="text" class="input-xxlarge" name="cog_value1" value="<?=$ds1;?>">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">About Us Footer</label>
			<div class="controls">
				<textarea class="mce" name="cog_value2"><?=$ds2;?></textarea>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Address Footer</label>
			<div class="controls">
				<textarea class="mce" name="cog_value3"><?=$ds3;?></textarea>
			</div>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
		</div>
	</form>
</div>