<div style="padding:5px 20px">
	<?=form_open('',array("class"=>"form-horizontal"))?>
		<div class="control-group">
			<label class="control-label">Top Tagline</label>
			<div class="controls">
				<input type="text" class="input-xxlarge" name="cog_value1" value="<?=$cog1;?>">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Welcome Words<br></label>
			<div class="controls">
				<input type="text" class="input-xxlarge" name="cog_value2" value="<?=$cog2;?>">
			</div>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
		</div>
	</form>
</div>