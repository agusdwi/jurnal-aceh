<div style="padding:5px 20px">
	<?=form_open('',array("class"=>"form-horizontal"))?>
		<div class="control-group">
			<label class="control-label">Facebook</label>
			<div class="controls">
				<input type="text" class="input-xxlarge" name="1" value="<?=$fb;?>">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Twitter</label>
			<div class="controls">
				<input type="text" class="input-xxlarge" name="2" value="<?=$tw;?>">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Google Plus</label>
			<div class="controls">
				<input type="text" class="input-xxlarge" name="3" value="<?=$gp;?>">
			</div>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
		</div>
	</form>
</div>