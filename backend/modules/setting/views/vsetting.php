<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Web Setting<small> konfigurasi website</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="#"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Setting</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4><i class="icon-globe"></i>Web Setting</h4>
				</div>
				<div class="widget-body">
					<ul class="nav nav-tabs">
						<li class="<?=($this->uri->segment(2) == 'intro')? 'active' : '' ?>"><a href="<?=base_url('admin/setting/intro')?>" >Website Introduction</a></li>
						<li class="<?=($this->uri->segment(2) == 'social')? 'active' : '' ?>"><a href="<?=base_url('admin/setting/social')?>" >Social Media</a></li>
						<li class="<?=($this->uri->segment(2) == 'meta')? 'active' : '' ?>"><a href="<?=base_url('admin/setting/meta')?>" >Meta Tag & SEO</a></li>
					</ul>
					<?=$this->load->view($this->uri->segment(2));?>
				</div>
			</div>
		</div>
	</div>
</div>