<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {	
	
	function __construct(){
		parent::__construct();
	}

	public function index(){
		redirect(base_url('admin/setting/intro'));
	}

	function intro(){
		if (is_post()) {
			$this->update(1,$this->input->post('cog_value1'));
			$this->update(2,$this->input->post('cog_value2'));
			$this->update(3,$this->input->post('cog_value3'));
			$this->redirect();
		}
		$data['main']	= "vsetting";
		$data['cur']	= "mn_10";
		$data['ds1']		= $this->get(1);
		$data['ds2']		= $this->get(2);
		$data['ds3']		= $this->get(3);
		$this->load->view('template',$data);
	}

	function social(){
		if (is_post()) {
			foreach ($this->input->post() as $key => $value) {
				$data = array('soc_link' => $value);
				$this->db->where('soc_id', $key);
				$this->db->update('gs_social', $data); 
			}
			$this->redirect();
		}

		$ds = $this->db->get_where('gs_social',array('soc_st'=>1))->result();

		$data['main']	= "vsetting";
		$data['cur']	= "mn_9";
		$data['fb']		= $ds[0]->soc_link;
		$data['tw']		= $ds[1]->soc_link;
		$data['gp']		= $ds[2]->soc_link;
		$this->load->view('template',$data);
	}

	function meta(){
		if (is_post()) {
			$this->update(6,$this->input->post('cog_value6'));
			$this->update(7,$this->input->post('cog_value7'));
			$this->redirect();
		}
		$data['main']	= "vsetting";
		$data['cur']	= "mn_9";
		$data['key']	= $this->get(6);
		$data['des']	= $this->get(7);
		$this->load->view('template',$data);
	}

	function get($id){
		return $this->db->get_where('gs_config',array('cog_id'=>$id))->row()->cog_value;
	}

	function update($id,$value){
		$data = array('cog_value' =>  $value);
		$this->db->where('cog_id', $id);
		$this->db->update('gs_config', $data); 
	}

	function redirect(){
		$this->session->set_flashdata('message','Setting saved');
		redirect(base_url().'admin/'. uri_string());
	}
}