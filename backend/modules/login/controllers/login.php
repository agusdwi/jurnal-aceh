<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {	

	function __construct(){
		parent::__construct();
	}
	
	public function index(){
		if (is_login()) {redirect('dashboard');}
		$data['captcha'] = $this->get_captcha();
		$this->load->view('vlogin',$data);
	}

	function check(){
		if (is_login()) {redirect('dashboard');}
		if (!$this->check_captcha()) {
			$this->session->set_flashdata('message','maaf kode captcha salah');
			redirect(base_url('admin'));
		}else{
			if(login_cek($this->input->post('username'),$this->input->post('password'))){
				$this->session->set_flashdata('message','selamat datang :D');
				redirect(base_url('admin/dashboard'));
			}else{
				$this->session->set_flashdata('message','maaf username / password salah');
				redirect(base_url('admin'));
			}
		}
	}

	function logout(){
		logout();redirect();
	}

	private function check_captcha(){
		$expiration = time()-7200; 
		$this->db->query("DELETE FROM gs_captcha WHERE captcha_time < ".$expiration);	

		$sql = "SELECT COUNT(*) AS count FROM gs_captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();

		return (($row->count == 0)? false:true);
	}

	private function get_captcha(){
		$this->load->helper(array('captcha','string'));
		$vals = array(
			'word'			=> random_string('numeric', 4), 
			'img_path'	 	=> 'media/captcha/',
			'img_width'	 	=> '150',
			'img_url'	 	=> base_url().'media/captcha/'
		);
		
		$cap = create_captcha($vals);
		
		$data = array(
			'captcha_time'	=> $cap['time'],
			'ip_address'	=> $this->input->ip_address(),
			'word'	 => $cap['word']
		);
		
		$query = $this->db->insert_string('gs_captcha', $data);
		$this->db->query($query);
		return $cap['image'];
	}

	function backup_db(){

		$this->load->dbutil();
		$prefs = array(
                'ignore'      => array(),           // List of tables to omit from the backup
                'format'      => 'txt',             // gzip, zip, txt
                'filename'    => 'mybackup.sql',    // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
                'newline'     => "\n"               // Newline character used in backup file
                );
		
		$backup =& $this->dbutil->backup($prefs);
		$this->load->helper('file');

		$this->load->helper('download');
		force_download('mybackup.sql', $backup);
		echo write_file('/mybackup.sql', $backup); 
	}

}