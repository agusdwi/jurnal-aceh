<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Login page | GSadmin</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	
	<link href="<?=base_url()?>assets/be/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/be/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/be/css/style.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/be/css/style_responsive.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/be/css/style_default.css" rel="stylesheet" id="style_color" />

	<script src="<?=base_url()?>assets/be/js/jquery-1.8.3.min.js"></script>
	<script src="<?=base_url()?>assets/be/assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/be/js/jquery.blockui.js"></script>
	<script src="<?=base_url()?>assets/be/js/scripts.js"></script>
	<script src="<?=base_url()?>assets/be/assets/valid/jquery.valid.js"></script>
	<script>
	$(function(){
		App.initLogin();
		$("form").validate({
			rules: {
				username	: "required",
				password	: "required",
				captcha		: "required"
			},messages:{
				username	: "",
				password	: "",
				captcha		: ""
			}
		});
	})
	</script>
	<style type="text/css">
		label.error{display: none !important}
	</style>
	<?=$this->load->view('include/msg');?>
</head>
<body id="login-body">
	<div class="login-header">
		<div id="logo" class="center">
			<img src="<?=base_url()?>assets/be/img/logo.png" alt="logo" class="center" />
		</div>
	</div>
	<div id="login">
		<?=form_open('login/check',array('id'=>'loginform','class'=>'form-vertical no-padding no-margin'))?>
			<div class="lock">
				<i class="icon-lock"></i>
			</div>
			<div class="control-wrap">
				<h4>User Login</h4>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-user"></i></span>
							<input id="input-username" type="text" placeholder="Username" autofocus="true" name="username" autocomplete="off"/>
						</div>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-key"></i></span>
							<input id="input-password" type="password" placeholder="Password" name="password" autocomplete="off"/>
						</div>
						<div class="clearfix space5"></div>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<div class="input-prepend captcha">
							<?=$captcha;?>
							<input id="input-password" type="text" placeholder="Code" name="captcha" autocomplete="off"/>
						</div>
						<div class="clearfix space5"></div>
					</div>
				</div>
			</div>
			<input type="submit" id="login-btn" class="btn btn-block login-btn" value="Login"/>
		</form>
	</div>
	<div id="login-copyright">
		2013 &copy; Gudeg Studio
	</div>
</body>
</html>