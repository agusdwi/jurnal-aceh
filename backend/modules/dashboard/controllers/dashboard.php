<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {	
	
	function __construct(){
		parent::__construct();
	}

	public function index(){
		$data['main']	= "vdashboard";
		$data['cur']	= "mn_1";
		$this->load->view('template',$data);
	}
}