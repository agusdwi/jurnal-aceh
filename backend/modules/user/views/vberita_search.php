<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Berita<small> mengatur kabar berita terbaru website</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Berita</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>List Berita</h4>
				</div>
				<div class="widget-body">
					<div class="pull-left">
						<?=form_open(base_url('admin/berita/search'))?>
							<label>Search: <input type="text" class="input-medium" name="q" value="<?=$this->q;?>"></label>
						</form>
					</div>
					<div class="pull-right">
						<a class="btn btn-primary" href="<?=base_url('admin/berita/add')?>"><i class="icon-plus"></i> Tambah Berita</a>
					</div>
					<div class="clearfix"></div>
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>No</th>
								<th>Judul</th>
								<th>Tanggal</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?$i=$this->page;foreach ($ds->result() as $key): $i++;?>
								<tr>
									<td><?=$i;?></td>
									<td><?=$key->berita_judul;?></td>
									<td><?=$key->berita_judul;?></td>
									<td><?=$key->berita_judul;?></td>
								</tr>
							<?endforeach;?>
						</tbody>
					</table>
					<div class="pull-right pagin">
						<?=$pagin;?>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>	
		</div>
	</div>
</div>