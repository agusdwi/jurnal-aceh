<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Keanggotaan<small> mengatur keanggotaan website</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Keanggotaan</a> <span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Detail</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>Form Detail Pengguna</h4>
				</div>
				<div class="widget-body">
					<div class="row-fluid">
						<div class="span5">
							<img style="display:inline-block;margin-right:10px" width="120" src="<?=base_url()?>media/avatar/<?=(($ds->avatar == "")?'blank.jpg':$ds->avatar);?>">
							<div style="display:inline-block">
								<b><?=$ds->nama;?></b><br>
								<?=$ds->email;?><br>
								Last login : <?=format_date_time($ds->last_login);?> <br>
								Reg Date : <?=format_date_time($ds->reg_date);?>
							</div>
						</div>
						<div class="span7">
							<b>Jurnal</b>
							<hr>
							<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
									<tr>
										<th>No</th>
										<th>Judul</th>
										<th>Tgl Upload</th>
										<th>View</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?for ($i=0;$i<10;$i++): ?>
										<tr>
											<td><?=$i;?></td>
											<td>Judul ini judul</td>
											<td>2-2-2012</td>
											<td>10</td>
											<td style="width:180px">
												<a href="" class="edit btn btn-mini purple tooltips" title="detail"><i class="icon-edit"></i>view</a>
												<a href="" class="edit btn btn-mini purple tooltips" title="detail"><i class="icon-edit"></i>download</a>
												<a href="" class="edit btn btn-danger btn-mini purple tooltips delete" title="hapus data"><i class="icon-trash"></i>delete</a>
											</td>
										</tr>
									<?endfor;?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="form-actions">
						<a href="<?=base_url('admin/user')?>" class="btn"><i class=" icon-arrow-left"></i> kembali</a>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>