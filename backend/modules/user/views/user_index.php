<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Keanggotaan<small> mengatur keanggotaan website</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Keanggotaan</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>Daftar Anggota</h4>
				</div>
				<div class="widget-body">
					<ul class="nav nav-tabs">
						<li class="<?=($state == 'aktif')? 'active' : '' ?>"><a href="<?=base_url('admin/user/index/aktif')?>" >Pengguna Aktif</a></li>
						<li class="<?=($state == 'pending')? 'active' : '' ?>">
							<a href="<?=base_url('admin/user/index/pending')?>" >
								Pending Aproval
								<?if ($pending_count > 0): ?>
									<span class="badge badge-important"><?=$pending_count;?></span>
								<?endif;?>
							</a>
						</li>
					</ul>
					<div class="clearfix"></div>
					<div class="btn-group" id="usr_switch">
						<button class="btn"><?=str_repeat("&nbsp;", 3);?>Dosen<?=str_repeat("&nbsp;", 3);?></button>
						<button class="btn">Mahasiswa</button>
					</div>
					<div class="clearfix"></div><br>
					<div id="tab-contain">
						<div id="child-dosen" class="hide">
							<table class="table table-striped table-bordered def_table">
								<thead>
									<tr>
										<th style="width:8px;">No</th>
										<th>Anggota</th>
										<th>Register Date</th>
										<th>Last Login</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?$i=0;foreach ($dsn->result() as $key):$i++; ?>
										<tr>
											<td><?=$i;?></td>
											<td>
												<img style="display:inline-block" width="40" src="<?=base_url()?>media/avatar/<?=(($key->avatar == "")?'blank.jpg':$key->avatar);?>">
												<div style="display:inline-block">
													<b><?=$key->nama;?></b><br>
													<?=$key->email;?>
												</div>
											</td>
											<td><?=format_date_time($key->reg_date);?></td>
											<td><?=format_date_time($key->last_login);?></td>
											<td class="tcenter" style="width:120px">
												<a href="<?=base_url()?>admin/user/detail/<?=$key->id;?>" class="edit btn btn-mini purple tooltips" title="detail"><i class="icon-edit"></i> Detail</a>
												<a href="<?=base_url('admin/user/delete')?>/<?=$key->id;?>" class="edit btn btn-danger btn-mini purple tooltips delete" title="hapus data"><i class="icon-trash"></i> Delete</a>
											</td>
										</tr>
									<?endforeach;?>
								</tbody>
							</table>
						</div>
						<div id="child-mahasiswa" class="hide">
							<table class="table table-striped table-bordered def_table">
								<thead>
									<tr>
										<th style="width:8px;">No</th>
										<th>Anggota</th>
										<th>Register Date</th>
										<th>Last Login</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?$i=0;foreach ($mhs->result() as $key):$i++; ?>
										<tr>
											<td><?=$i;?></td>
											<td>
												<img style="display:inline-block" width="40" src="<?=base_url()?>media/avatar/<?=(($key->avatar == "")?'blank.jpg':$key->avatar);?>">
												<div style="display:inline-block">
													<b><?=$key->nama;?></b><br>
													<?=$key->email;?>
												</div>
											</td>
											<td><?=format_date_time($key->reg_date);?></td>
											<td><?=format_date_time($key->last_login);?></td>
											<td class="tcenter" style="width:120px">
												<a href="<?=base_url()?>admin/user/detail/<?=$key->id;?>" class="edit btn btn-mini purple tooltips" title="detail"><i class="icon-edit"></i> Detail</a>
												<a href="<?=base_url('admin/user/delete')?>/<?=$key->id;?>" class="edit btn btn-danger btn-mini purple tooltips delete" title="hapus data"><i class="icon-trash"></i> Delete</a>
											</td>
										</tr>
									<?endforeach;?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('#usr_switch').on('click','button',function (e) {
			e.preventDefault();
			$("#usr_switch .btn").removeClass('btn-primary');
			$(this).addClass('btn-primary');
			if (e.currentTarget.innerText == "Mahasiswa") {
				$("#child-dosen").addClass('hide');
				$("#child-mahasiswa").removeClass('hide');
			}else{
				$("#child-mahasiswa").addClass('hide');
				$("#child-dosen").removeClass('hide');
			}
		});
		$("#usr_switch .btn:eq(0)").trigger('click');
	})
</script>