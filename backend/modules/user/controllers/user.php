<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {	

	function __construct(){
		parent::__construct();
		$this->table = "jr_users";
	}

	function redirect(){
		redirect(base_url('admin/user/index/aktif'));
	}

	function index($state=''){
		if ($state=='') {
			$this->redirect();
		}
		$data['state']			= $state;
		$data['cur']			= "mn_2";
		$data['pending_count']	= $this->db->get_where($this->table,array('status'=>-1))->num_rows();
		if ($state=="aktif") {
			$data['main']			= "user_index";
			$data['mhs']			= $this->db->get_where($this->table,array('type'=>'mahasiswa','status'=>1));
			$data['dsn']			= $this->db->get_where($this->table,array('type'=>'dosen','status'=>1));
			$this->load->view('template',$data);	
		}else{
			$data['main']			= "user_index_pending";
			$data['mhs']			= $this->db->get_where($this->table,array('type'=>'mahasiswa','status'=>-1));
			$data['dsn']			= $this->db->get_where($this->table,array('type'=>'dosen','status'=>-1));
			$this->load->view('template',$data);
		}
	}

	function delete($id){
		$data = array('status' => 0);
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		$this->redirect();
	}

	function aktif($id){
		$data = array('status' => 1);
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		$this->session->set_flashdata('message', 'pengguna berhasil diaktifkan');
		redirect(base_url('admin/user/index/pending'));
	}

	function detail($id){
		$data['cur']			= "mn_2";
		$data['main']			= "user_detail";
		$data['jurnal']			= array();
		$data['ds']				= $this->db->get_where($this->table,array('id'=>$id))->row();
		$this->load->view('template',$data);	
	}
}