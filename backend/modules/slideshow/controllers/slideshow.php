<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slideshow extends CI_Controller {	
	
	var $path	= "media/slideshow";

	function __construct(){
		parent::__construct();
		$cnfg = $this->config->item('slide_img');
		$this->width 	= $cnfg[0];
		$this->height 	= $cnfg[1];
	}

	public function index(){
		$data['main']	= "vslideshow";
		$data['cur']	= "mn_8";
		$data['ds']		= $this->db->get('app_slideshow');
		$this->load->view('template',$data);
	}

	function add(){
		if (is_post()) {
			$upload = $this->upload();
			$data = $this->input->post();
			if (!$upload[0]) {
				$this->session->set_flashdata('message','Maaf file gagal di upload, silahkan ulangi');
				redirect(base_url('admin/slideshow/add'));
			}
			$data['ss_img'] = $upload[1]['file_name'];
			$this->db->insert('app_slideshow',$data);
			
			$this->session->set_flashdata('message','Slideshow berhasil disimpan');
			redirect(base_url('admin/slideshow'));
		}
		$data['main']	= "vadd";
		$data['cur']	= "mn_8";
		$this->load->view('template',$data);
	}

	function delete($id){
		$file = $this->db->get_where('app_slideshow',array('ss_id'=>$id))->row();
		unlink($this->path."/".$file->ss_url);
		$this->db->delete('app_slideshow', array('ss_id' => $id)); 
		$this->session->set_flashdata('message','Gambar berhasil di hapus');
		redirect(base_url('admin/slideshow'));	
	}

	function upload(){
		$_FILES['userfile']['name']	= strtolower($_FILES['userfile']['name']);
		$config['upload_path']		= 'media/slideshow';
		$config['allowed_types']	= '*';
		$config['max_size']			= '10000';
		$config['max_width']		= '4000';
		$config['max_height']		= '4000';
		$config['encrypt_name']		= true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload()){
			return array(false,$this->upload->display_errors());
		}else{
			$a = $this->upload->data();
			$this->load->library('image_moo');
			$this->image_moo
				->load('media/slideshow/'.$a['file_name'])
				->resize_crop($this->width,$this->height)
				->save('media/slideshow/'.$a['file_name'],true);	
			return array(true,$a);
		}
	}
}