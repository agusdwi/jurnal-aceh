<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Front Page Slideshow<small> mengatur slideshow halaman depan</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Front Page Slideshow</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>List Slideshow</h4>
				</div>
				<div class="widget-body">
					<div class="pull-right">
						<a class="btn btn-success btn-mini" href="<?=base_url('admin/slideshow/add')?>"><i class="icon-plus"></i> Tambah Slideshow</a>
					</div>
					<div class="clearfix"></div><br>
					<table class="table table-striped table-bordered" id="tb_artikel">
						<thead>
							<tr>
								<th>No</th>
								<th>Img</th>
								<th>Title</th>
								<th>Description</th>
								<th>Link</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?$i=0;foreach ($ds->result() as $key): $i++;?>
							<tr>
								<td><?=$i;?></td>
								<td><img width="75" src="<?=base_url()?>media/slideshow/<?=$key->ss_img;?>"></td>
								<td><?=$key->ss_title;?></td>
								<td><?=$key->ss_desc;?></td>
								<td><a href="<?=$key->ss_link;?>"><?=$key->ss_link;?></a></td>
								<td class="tcenter" style="width:110px">
									<a href="<?=base_url()?>admin/slideshow/delete/<?=$key->ss_id;?>" class="edit btn btn-danger btn-mini purple tooltips delete" title="hapus data"><i class="icon-trash"></i> Delete</a>
								</td>
							</tr>
							<?endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>