<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Front Page Slideshow<small> mengatur slideshow halaman depan</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Front Page Slideshow</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>Template Contact Us</h4>
				</div>
				<div class="widget-body">
					<?=form_open_multipart('',array("class"=>"form-horizontal"))?>
						<div class="control-group">
							<label class="control-label">Title</label>
							<div class="controls">
								<input type="text" class="input-xlarge" name="ss_title" id="ss_title">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Description</label>
							<div class="controls">
								<input type="text" class="input-xxlarge" name="ss_desc" id="ss_desc">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Image</label>
							<div class="controls">
								<div class="fileupload fileupload-new" data-provides="fileupload" style="float:left;margin-right:5px">
									<input type="hidden" value="" name="">
									<div class="input-append">
										<div class="uneditable-input">
											<i class="icon-file fileupload-exists"></i>
											<span class="fileupload-preview"></span>
										</div>
										<span class="btn btn-file">
											<span class="fileupload-new">Select file</span>
											<span class="fileupload-exists">Change</span>
											<input type="file" class="default" name="userfile">
										</span>
										<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
									</div>
								</div><br><br><i>Gambar sebaiknya portait (berdiri) dengan ukuran <?=$this->width;?> x <?=$this->height;?> piksel</i>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Link</label>
							<div class="controls">
								<input type="text" class="input-xxlarge" name="ss_link" id="ss_link">
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
							<a href="<?=base_url('admin/contact')?>" class="btn"><i class=" icon-remove"></i> Cancel</a>
						</div>
					</form>
				</div>
			</div>	
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("form").validate({
			rules: {
				ss_title	: "required",
				ss_desc		: "required",
				ss_link		: "required",
				userfile	: "required"
			}
		});
	})
</script>