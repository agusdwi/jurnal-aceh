<?=tiny_mce('mce');?>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Contact Us<small> mengatur pesan dari pengunjung</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="<?=base_url('admin/contact')?>">Contact</a> <span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Template</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>Template Contact Us</h4>
				</div>
				<div class="widget-body">
					<?=form_open('',array("class"=>"form-horizontal"))?>
						<div class="control-group">
							<label class="control-label">Template Contact Us</label>
							<div class="controls">
								<textarea class="mce" name="cog_value"><?=$ds;?></textarea>
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
							<a href="<?=base_url('admin/contact')?>" class="btn"><i class=" icon-remove"></i> Cancel</a>
						</div>
					</form>
				</div>
			</div>	
		</div>
	</div>
</div>