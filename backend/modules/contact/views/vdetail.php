<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Contact Us<small> mengatur pesan dari pengunjung</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="<?=base_url('admin/contact')?>">Contact</a> <span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Detail</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>Detail Pesan</h4>
				</div>
				<div class="widget-body">
					<div class="control-group">
						<label class="control-label">Nama</label>
						<div class="controls">
							<input type="text" class="input-xlarge" name="art_judul" id="art_judul" value="<?=$ds->name;?>" readonly="readonly">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Tanggal</label>
						<div class="controls">
							<input type="text" class="input-large" name="art_keyword" id="art_keyword" value="<?=pretty_date($ds->date);?>" readonly="readonly">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Email</label>
						<div class="controls">
							<input type="text" class="input-large" name="art_keyword" id="art_keyword" value="<?=$ds->email;?>" readonly="readonly">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Subject</label>
						<div class="controls">
							<input type="text" class="input-xxlarge" name="art_description" id="art_description" value="<?=$ds->subject;?>" readonly="readonly">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Pesan</label>
						<div class="controls">
							<textarea class="input-xxlarge" rows="5"  name="art_content" readonly="readonly"><?=$ds->msg;?></textarea>
						</div>
					</div>
					<div class="form-actions">
						<a href="<?=base_url('admin/contact')?>" class="btn"><i class=" icon-arrow-left"></i> index pesan</a>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>