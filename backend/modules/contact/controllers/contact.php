<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {	
	
	var $perpage = 10;

	function index(){
		$data['main']	= "vindex";
		$data['cur']	= "mn_8";
		$data['ds']		= $this->db->order_by('id','desc')->get('app_contact');
		$this->load->view('template',$data);
	}

	function detail($id){
		$data = array('read' => 1 );
		$this->db->where('id', $id);
		$this->db->update('app_contact', $data); 
		

		$data['main']	= "vdetail";
		$data['cur']	= "mn_8";
		$data['ds']		= $this->db->get_where('app_contact',array('id'=>$id))->row();
		$this->load->view('template',$data);
	}

	function delete($id){
		$this->db->delete('app_contact', array('id' => $id)); 
		$this->session->set_flashdata('message','Message berhasil di hapus');
		redirect(base_url('admin/contact'));
	}

	function template(){
		if (is_post()) {
			$this->update(4,$this->input->post('cog_value'));
			$this->redirect();
		}
		$data['main']	= "vtemplate";
		$data['cur']	= "mn_8";
		$data['ds']		= $this->get(4);
		$this->load->view('template',$data);
	}

	function get($id){
		return $this->db->get_where('gs_config',array('cog_id'=>$id))->row()->cog_value;
	}

	function update($id,$value){
		$data = array('cog_value' =>  $value);
		$this->db->where('cog_id', $id);
		$this->db->update('gs_config', $data); 
	}

	function redirect(){
		$this->session->set_flashdata('message','Setting saved');
		redirect(base_url().'admin'. uri_string());
	}
}