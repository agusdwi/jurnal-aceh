<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends CI_Controller {	
	
	var $sort = 0;	

	function __construct(){
		parent::__construct();
		$this->load->library('Menu_nest');
	}

	public function index(){
		$data['main']		= "vmenu";
		$data['cur']		= "mn_5";
		$data['menu']		= $this->menu_nest->get();
		$data['menu_json']	= $this->get_menu_json();
		
		// master
		$data['modul']	= $this->db->get('gs_modul');
		$data['artikel']= $this->db->get('gs_artikel');
		$data['widget']	= $this->db->get('gs_widget');
		$this->load->view('template',$data);
	}

	function update(){
		$all = $this->input->post();
		$wgt = (isset($all['wgt'])) ? $all['wgt'] : array();
		$data = $all['ds'];
		$data['menu_content'] = $all[$data['menu_type']];
		$this->db->where('menu_id', $all['menu_id']);
		$this->db->update('gs_menu', $data);

		$this->db->delete('gs_mn_wgt', array('mn_id' => $all['menu_id'])); 
		foreach ($wgt as $key) {
			$swgt[] = array(
					'mn_id' => $all['menu_id'],
					'wgt_id' => $key,
				);
		}
		if(isset($swgt))
			$this->db->insert_batch('gs_mn_wgt',$swgt);

		$this->update_link($all['menu_id'],$data['menu_type'],$data['menu_content']);

		$this->session->set_flashdata('message',"menu ".$all['ds']['menu_nama']." berhasil di update");
		redirect(base_url('admin/menu'));
	}

	function update_link($mn_id,$mn_tp,$ctn){
		switch ($mn_tp) {
			case 'modul' :
				$data = array('menu_link' => $ctn);
				break;
			case 'artikel' :
				$art = $this->db->get_where('gs_artikel',array('art_id'=>$ctn))->row()->art_link;
				$data = array('menu_link' => "page/$art");
			break;
			case 'eksternal' :
				$parsed = parse_url($ctn);
				if (empty($parsed['scheme'])) {
					$ctn = 'http://' . ltrim($ctn, '/');
				}
				$data = array('menu_link' => $ctn);
			break;
		}
		$this->db->where('menu_id', $mn_id);
		$this->db->update('gs_menu', $data);
	}

	function add(){
		$data['menu_nama'] = $this->input->post('menu');
		$this->db->insert('gs_menu',$data);
		$data = array('menu_sort' =>  $this->db->insert_id());
		$this->db->where('menu_id', $this->db->insert_id());
		$this->db->update('gs_menu', $data); 
		
		$this->session->set_flashdata('message','menu berhasil di simpan');
		redirect(base_url('admin/menu'));
	}

	function delete($id){
		$this->db->delete('gs_menu', array('menu_id' => $id)); 
		$this->session->set_flashdata('message','menu berhasil dihapus');
		redirect(base_url('admin/menu'));
	}

	function save_sort(){
		$menu = json_decode($this->input->post('sort'));
		$this->save_tree($menu);
	}

	function save_tree($menu,$parent=0) {
		foreach ($menu as $key) {
			$this->sort++;
			
			$data = array(
			               'parent_id' 	=>  $parent,
			               'menu_sort' 	=>  $this->sort
			            );
			$this->db->where('menu_id', $key->id);
			$this->db->update('gs_menu', $data); 

			if (isset($key->children)) {
				$this->save_tree($key->children,$key->id);
			}
		}
	}

	private function get_menu_json(){
		$ds_menu = $this->db->get('gs_menu');
		$data = array();
		foreach ($ds_menu->result() as $key) {
			$data[$key->menu_id] = array(
				'info' 	=> $key,
				'wgt' 	=> $this->db->get_where('v_gs_mn_wgt',array('mn_id'=>$key->menu_id))->result()
			);
		}
		return json_encode($data);
	}
}