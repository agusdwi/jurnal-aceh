<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Menu Website<small> mengatur menu website</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Menu Website</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4">
			<div class="widget">
				<div class="widget-title">
					<h4>Menu Items</h4>
				</div>
				<div class="widget-body">
					<div class="dd" id="nestable_list_1">
						<?=form_open('menu/save_sort',array('id'=>'frm_sort','style="display:none"'))?>
							<textarea name="sort" id="nestable_list_1_output" class="m-wrap span12" style="display:none"></textarea>
						</form>
						<?=form_open('menu/add',array('id'=>'frm_add'))?>
							<input type="text" name="menu"><button style="margin-top:-9px" type="submit" class="btn blue"><i class="icon-plus"></i> add</button>
						</form>
						<?=$menu;?>
					</div>
				</div>
			</div>
		</div>
		<div class="span8">
			<div class="widget">
				<div class="widget-title">
					<h4>Menu Content</h4>
				</div>
				<div class="widget-body" id="def_body">
					<center><h4>Pilih Menu di Sebelah Kiri</h4></center>
				</div>
				<div class="widget-body hide" id="pil_body" style="position:relative">
					<?=form_open('menu/update',array('class'=>'form-vertical','id'=>'frm_update'))?>
					<input type="hidden" name="menu_id" id="menu_id">
					<div class="rowspan">
						<div class="span7">
							<div class="control-group">
								<label class="control-label">Nama Menu</label>
								<div class="controls">
									<input type="text" class="input-large" name="ds[menu_nama]" id="menu_nama">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label">Tipe Menu</label>
								<div class="controls">
									<select id="mn_tipe" name="ds[menu_type]">
										<option value=""> -- pilih tipe -- </option>
										<option>artikel</option>
										<option>modul</option>
										<option>eksternal</option>
									</select>
								</div>
							</div>
							<div class="control-group hide" id="st_modul">
								<label class="control-label">Modul</label>
								<div class="controls">
									<select name="modul" id="mn_modul">
										<?foreach ($modul->result() as $key): ?>
										<option value="<?=$key->modul_url;?>"><?=$key->modul_nama;?></option>
										<?endforeach;?>
									</select>
								</div>
							</div>
							<div class="control-group hide" id="st_link">
								<label class="control-label">Link</label>
								<div class="controls">
									<input type="text" class="input-large" name="eksternal" id="eksternal">
								</div>
							</div>

							<div class="control-group hide" id="st_artikel">
								<label class="control-label">Artikel</label>
								<div id="artikel_container">
									<span id="title_artikel">Judul Artikel</span>
									<div id="scroller">
										<ul>
											<?foreach ($artikel->result() as $key): ?>
												<li id="art_<?=$key->art_id;?>">
													<input style="display:none" type="checkbox" class="artikel_cx" name="artikel" value="<?=$key->art_id;?>">
													<?=$key->art_judul;?>	<button class="pil_artikel pull-right btn btn-mini btn-primary" type="button">pilih</button>
												</li>	
											<?endforeach;?>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="span5">
							<!-- <div id="banner" style="margin-right: 15px !important;">
								<div style="background:#DDD;color:#666;padding:4px 10px;text-align:center;font-weight">Widget Banner</div>
								<div style="border:1px solid #DDD;height:200px;padding:10px">
									<select style="width:150px" id="opt_widget">
										<?foreach ($widget->result() as $key ): ?>
											<option value="<?=$key->wgt_id;?>"><?=$key->wgt_nama;?></option>
										<?endforeach;?>
									</select>
									<button id="add_widget" style="margin-top:-10px">+</button>
									<ul id="list_widget"></ul>	
								</div>
							</div> -->
						</div>
						<div class="clearfix"></div>
					</div>	
					<div class="clearfix"></div>
					<div class="form-actions hide" id="bt_simpan">
						<button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
						<button type="button" class="btn"><i class=" icon-remove"></i> Cancel</button>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?=base_url()?>assets/be/assets/nestable/jquery.nestable.js"></script>
<script src="<?=base_url()?>assets/be/js/ui-nestable.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/be/assets/nestable/jquery.nestable.css">	
<script type="text/javascript">
	var menu_json = <?=$menu_json;?>;
	$(function(){
		UINestable.init();
		$(".hh").die('click').live('click',function(){
			$('.mactive').removeClass('mactive')
			$(this).addClass('mactive');
			var id = $(this).parent().attr('data-id');
			$("#def_body").fadeOut(function(){
				$("#pil_body").fadeIn();
			})
			init_form(menu_json[id]);
			return false;
		})
		$("#frm_add").validate({
			rules: {menu	: "required"},
			messages: {menu	: ""}
		});
		$(".deletemenu").die('click').live('click',function(){
			if(confirm('apakah anda yakin menghapus menu ini ?')){
				window.location="<?=base_url()?>admin/menu/delete/"+$(this).attr('uid');
				return false;
			}else return false;
		})
		$("#mn_tipe").change(function(){
			var sw = $(this).val();
			if(sw != ''){
				$("#st_modul,#st_artikel,#st_link,#bt_simpan").hide();
				if (sw == 'artikel') 
					$("#st_artikel,#bt_simpan").show()
				else if (sw == 'modul') 
					$("#st_modul,#bt_simpan").show()
				else if (sw == 'eksternal') 
					$("#st_link,#bt_simpan").show()
			}else{
				$("#st_modul,#st_artikel,#st_link,#bt_simpan").hide();
			}
		})
		$("#list_widget").sortable();
		$("#add_widget").click(function(){
			$("#list_widget").append("<li><input type='hidden' name='wgt[]' value='"+ $("#opt_widget").val() +"'>" + $("#opt_widget option:selected").text() + "<a href='' class='del_widget'>x</a></li>");
			return false;
		})
		$(".del_widget").die('click').live('click',function(){
			$(this).parent().fadeOut(function(){
				$(this).remove();
			});
			return false;
		})
		$(".pil_artikel").die('click').live('click',function(){
			$('li.selected').removeClass('selected');
			$(this).parent().addClass('selected');
			$(".artikel_cx").removeAttr('checked');
			$(this).parent().find('input').attr('checked','checked');
			$.uniform.update();
			return false;
		})
	})
	function save_sort(){
		var url  = $("#frm_sort").attr('action');
		var data = $("#frm_sort").serialize();
		$.post(url,data, function(data) {});
	}

	function init_form(json){
		console.log(json);
		$("#menu_id").val(json.info.menu_id);
		$("#menu_nama").val(json.info.menu_nama);
		$("#mn_tipe").val(json.info.menu_type);
		$("#mn_tipe").trigger('change');
		var sw = json.info.menu_type;
		if (sw == 'artikel') {
			$('li.selected').removeClass('selected');
			$("#art_"+json.info.menu_content).addClass('selected');
			$(".artikel_cx").removeAttr('checked');
			$("#art_"+json.info.menu_content).find('input').attr('checked','checked');
			$.uniform.update();
		}else if (sw == 'modul') 
		$("#mn_modul").val(json.info.menu_content);
		else if (sw == 'eksternal') 
			$("#eksternal").val(json.info.menu_content);
		// wgt
		$("#list_widget").html('');
		$(json.wgt).each(function(index,val){
			$("#list_widget").append("<li><input type='hidden' name='wgt[]' value='"+ val.wgt_id +"'>" + val.wgt_nama + "<a href='' class='del_widget'>x</a></li>");
		})
	}
</script>
<style type="text/css">
	label.error{
		display: none !important;
	}
	.hh:hover .deletemenu{
		opacity: 1;
	}
	.deletemenu{
		opacity: 0;
		position: absolute;
		z-index: 999;
		right: 5px;
		top: 6px;
		font-weight: bold;
		color: #e74955;
	}
	#list_widget{
		list-style: none;
		margin: 0px;
		height: 150px;
		overflow: auto;
		padding-right: 10px;
	}
	#list_widget li{
		padding:2px 4px;
		background: #dddddd;
		margin: 5px 0px;
		cursor: move;
	}
	#list_widget li a{
		color: #e74955;
		font-weight: bold;
		float: right;
		opacity: 0;
	}
	#list_widget li:hover a{
		opacity: 1;
	}
	/*artikel*/
	#artikel_container{
		background: #fbfbfb;
		border-radius: 5px;
		border:1px solid #E5E5E5;
	}
	#artikel_container ul{
		list-style: none;
		margin: 0px;
		padding: 0px;
	}
	#artikel_container ul li {
		padding: 5px 10px;
	}
	#artikel_container ul li button{
		opacity: 0;
	}
	#artikel_container ul li:hover button{
		opacity: 1;
	}
	#artikel_container ul li:nth-child(odd) {
		background: #f2f2f2;
	}
	#artikel_container #title_artikel {
		background: #E5E5E5;
		font-weight: bold;
		text-align: center;
		display: block;
		padding: 5px 10px;
	}
	#scroller{
		height: 300px;
		overflow: auto;
	}
	li.selected{
		background: #aae4ff !important;
	}
	li .checker{
		display: none;
	}
</style>