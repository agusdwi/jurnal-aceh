<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artikel extends CI_Controller {	
	
	var $perpage = 10;

	function index($page="0"){
		$data['main']	= "vindex";
		$data['cur']	= "mn_7";
		$data['ds']		= $this->db->order_by('art_id','desc')->get('gs_artikel');
		$this->load->view('template',$data);
	}

	function add(){
		if (is_post()) {
			$data = $this->input->post();
			$data['art_link'] = get_unique($data['art_judul'],'gs_artikel','art_link');
			$this->db->insert('gs_artikel',$data);
			$this->session->set_flashdata('message','Artikel berhasil di simpan');
			redirect(base_url('admin/artikel'));
		}
		$data['main']	= "vadd";
		$data['cur']	= "mn_7";
		$this->load->view('template',$data);
	}

	function edit($id){
		if (is_post()) {
			$data = $this->input->post();
			$data['art_link'] = get_unique_edit($data['art_judul'],'gs_artikel','art_link','art_id',$id);
			$this->db->where('art_id', $id);
			$this->db->update('gs_artikel', $data); 
			$this->session->set_flashdata('message','Artikel berhasil di perbarui');
			redirect(base_url('admin/artikel'));
		}
		$data['main']	= "vedit";
		$data['cur']	= "mn_7";
		$data['ds']		= $this->db->get_where('gs_artikel',array('art_id'=>$id))->row();
		$this->load->view('template',$data);
	}

	function delete($id){
		$this->db->delete('gs_artikel', array('art_id' => $id)); 
		$this->session->set_flashdata('message','Artikel berhasil di hapus');
		redirect(base_url('admin/artikel'));
	}
}