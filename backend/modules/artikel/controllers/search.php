<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {	

	var $perpage = 10;

	function index(){
		$q = rawurlencode($this->input->post('q'));
		redirect(base_url('admin/berita/search/result/'.$q));

	}

	function result($q,$page=0){
		$this->load->library('pagination');

		$config['base_url'] 	= base_url().'admin/berita/search/result/'.$q;
		$config['total_rows'] 	= $this->db->get('gs_berita')->num_rows();
		$config['per_page'] 	= $this->perpage; 
		$config['uri_segment'] 	= 5; 

		$this->pagination->initialize($config); 

		$this->page = $page;
		$this->q = $q;

		$data['main']	= "vberita_search";
		$data['cur']	= "mn_4";
		$data['pagin']	= $this->pagination->create_links();
		$data['ds']		= $this->db->limit($this->perpage,$page)->get('gs_berita');
		$this->load->view('template',$data);
	}
}