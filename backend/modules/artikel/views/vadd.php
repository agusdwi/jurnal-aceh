<?=tiny_mce('mce');?>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Artikel<small> mengatur artikel website</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="<?=base_url('admin/artikel')?>">Artikel</a> <span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Tambah</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>Form Tambah Artikel</h4>
				</div>
				<div class="widget-body">
					<?=form_open('',array("class"=>"form-horizontal"))?>
						<div class="control-group">
							<label class="control-label">Judul Artikel</label>
							<div class="controls">
								<input type="text" class="input-xlarge" name="art_judul" id="art_judul">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Meta Keyword</label>
							<div class="controls">
								<input type="text" class="input-large" name="art_keyword" id="art_keyword">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Meta Description</label>
							<div class="controls">
								<input type="text" class="input-xxlarge" name="art_description" id="art_description">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Isi Berita</label>
							<div class="controls">
								<textarea class="mce" name="art_content"></textarea>
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
							<a href="<?=base_url('admin/artikel')?>" class="btn"><i class=" icon-remove"></i> Cancel</a>
						</div>
					</form>
				</div>
			</div>	
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("form").validate({
			rules: {
				art_judul			: "required",
				art_keyword			: "required",
				art_description		: "required"
			}
		});
	})
</script>