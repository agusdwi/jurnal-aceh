<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="page-title">Artikel<small> mengatur artikel website</small></h3>
			<ul class="breadcrumb">
				<li>
					<a href="<?=base_url('admin')?>"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
				</li>
				<li>
					<a href="#">Artikel</a> <span class="divider-last">&nbsp;</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="widget">
				<div class="widget-title">
					<h4>List Artikel</h4>
				</div>
				<div class="widget-body">
					<a class="btn btn-mini btn-success pull-right" href="<?=base_url('admin/artikel/add')?>"><i class="icon-plus"></i> Tambah Artikel</a>
					<div class="clearfix"></div><br>
					<table class="table table-striped table-bordered" id="tb_artikel">
						<thead>
							<tr>
								<th>No</th>
								<th>Judul</th>
								<th>Deskripsi</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?$i=0;foreach ($ds->result() as $key): $i++;?>
								<tr>
									<td><?=$i;?></td>
									<td><?=$key->art_judul;?></td>
									<td><?=smart_trim($key->art_content,50)?></td>
									<td class="tcenter" style="width:110px">
										<a href="<?=base_url()?>admin/artikel/edit/<?=$key->art_id;?>" class="edit btn btn-mini purple tooltips" title="ubah data"><i class="icon-edit"></i> Edit</a>
										<a href="<?=base_url()?>admin/artikel/delete/<?=$key->art_id;?>" class="edit btn btn-danger btn-mini purple tooltips delete" title="hapus data"><i class="icon-trash"></i> Delete</a>
									</td>
								</tr>
							<?endforeach;?>
						</tbody>
					</table>
					<div class="clearfix"></div>
				</div>
			</div>	
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		// begin first table
		$('#tb_artikel').dataTable({
			"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"sPaginationType": "bootstrap",
			"oLanguage": {
				"sLengthMenu": "_MENU_ records per page",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next"
				}
			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': [1]
			}]
		});
	})
</script>