<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['app_name'] 		= 'GS Admin';

$config['blog_img'] 		= array('750','390');
$config['blog_thumb_img'] 	= array('170','170');

$config['brand_img'] 		= array('280','187');
$config['slide_img'] 	= array('543','250');
$config['web_title'] 		= "Aceh Nutrition Jurnal";