<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <?=$this->load->view('include/script');?>
</head>
<body class="fixed-top">
    <?=$this->load->view('include/header');?>
    <?=$this->load->view('include/msg');?>
    <div id="container" class="row-fluid">
        <?=$this->load->view('include/sidebar');?>
        <div id="main-content">
            <?=(isset($main)) ? $this->load->view($main) : '<center>anda belum men set main view</center>';?> 
        </div>
    </div>
    <div id="footer">
        2013 &copy; Admin Lab Dashboard.
        <div class="span pull-right">
            <span class="go-top"><i class="icon-arrow-up"></i></span>
        </div>
    </div>
</body>
</html>