<div id="header" class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="brand" href="index.html">
                <img src="<?=base_url()?>assets/be/img/logo.png" alt="Admin Lab" />
            </a>
            <a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="arrow"></span>
            </a>
            <div id="top_menu" class="nav notify-row">
             <ul class="nav top-menu">

             </ul>
         </div>
         <div class="top-nav ">
             <ul class="nav pull-right top-menu" >
                 <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                         <img src="<?=base_url()?>assets/be/img/avatar1_small.jpg" alt="">
                         <span class="username"><?=get_user('username');?></span>
                         <b class="caret"></b>
                     </a>
                     <ul class="dropdown-menu">
                         <li><a href="<?=base_url('admin/dashboard/profile')?>"><i class="icon-user"></i> My Profile</a></li>
                         <li><a href="<?=base_url('admin/logout')?>"><i class="icon-key"></i> Log Out</a></li>
                     </ul>
                 </li>
             </ul>
         </div>
     </div>
 </div>
</div>