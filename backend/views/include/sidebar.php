<?
	$menu = array(
		array('icon-dashboard','dashboard','Dashboard'),
		array('icon-group','user','Keanggotaan'),
		array('icon-inbox','kategori','Kategori & Prodi'),
		array('icon-book','jurnal','Pengaturan Jurnal'),
		array('icon-sitemap','menu','Menu Website'),
		array('icon-rss','berita','Pengaturan Berita'),
		array('icon-copy','artikel','Pengaturan Artikel'),
		array('icon-copy','slideshow','Slide Show'),
		array('icon-envelope','contact','Contact Us'),
		array('icon-cogs','setting','Pengaturan Website'),
		array('icon-remove-sign','logout','Logout'),
	);

	function is_actip($ij){
		if(isset($cur)) {
			if ($cur == $ij) {
				return 'active';
			}else return 'a';
		}else return 'ab';
	}
?>

<div id="sidebar" class="nav-collapse collapse">
	<div class="sidebar-toggler hidden-phone"></div>   
	<div class="navbar-inverse"></div>
	<ul class="sidebar-menu">
		<?$i=0;foreach ($menu as $key):$i++; ?>
			<li style="position:relative" class="<?=(isset($cur)? (($cur == 'mn_'.$i)? 'active' : '' ) : '')?>">
				<a href="<?=base_url('admin/'.$key[1])?>">
					<span class="icon-box">
						<i class="<?=$key[0];?>"></i>
					</span> 
					<?=$key[2];?>
				</a>
			</li>
		<?endforeach;?>
	</ul>
</div>

<?
	$unread = $this->db->get_where('app_contact',array('read'=>0))->num_rows();
	$testi 	= $this->db->get_where('app_testi',array('t_status'=>0))->num_rows();
?>
<?if ($unread > 0): ?>
	<script type="text/javascript">
		$(function(){
			$("ul.sidebar-menu li:eq(7)").append("<span id='notif'>"+<?=$unread;?>+"</span>");
		})
	</script>	
<?endif;?>
<?if ($testi > 0): ?>
	<script type="text/javascript">
		$(function(){
			$("ul.sidebar-menu li:eq(8)").append("<span id='notif'>"+<?=$testi;?>+"</span>");
		})
	</script>	
<?endif;?>