<meta charset="utf-8" />
<title><?=(isset($title)) ? $title.' | ' : 'GS admin panel';?></title>

<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />

<link href="<?=base_url()?>assets/be/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?=base_url()?>assets/be/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="<?=base_url()?>assets/be/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
<link href="<?=base_url()?>assets/be/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="<?=base_url()?>assets/be/css/style.css" rel="stylesheet" />
<link href="<?=base_url()?>assets/be/css/style_responsive.css" rel="stylesheet" />
<link href="<?=base_url()?>assets/be/css/style_default.css" rel="stylesheet" id="style_color" />
<link href="<?=base_url()?>assets/be/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
<link href="<?=base_url()?>assets/be/assets/jquery-tags-input/jquery.tagsinput.css" rel="stylesheet" />
<link href="<?=base_url()?>assets/be/assets/jqui/css/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />

<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/be/assets/uniform/css/uniform.default.css" />

<script src="<?=base_url()?>assets/be/js/jquery-1.8.3.min.js"></script>
<script src="<?=base_url()?>assets/be/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/be/js/jquery.blockui.js"></script>
<!-- ie8 fixes -->
<!--[if lt IE 9]>
	<script src="js/excanvas.js"></script>
	<script src="js/respond.js"></script>
<![endif]-->
<script type="text/javascript" src="<?=base_url()?>assets/be/assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/be/assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/be/assets/bootstrap/js/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/be/assets/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/be/assets/jqui/js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/be/assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="<?=base_url()?>assets/be/assets/valid/jquery.valid.js"></script>
<script src="<?=base_url()?>assets/be/js/scripts.js"></script>
<script>
	var BASE = "<?=base_url()?>";
	jQuery(document).ready(function() {       
         App.init();
     });
</script>

<script type="text/javascript" src="<?=base_url()?>assets/be/assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/be/assets/data-tables/DT_bootstrap.js"></script>
<link href="<?=base_url()?>assets/be/assets/data-tables/DT_bootstrap.css" rel="stylesheet" />