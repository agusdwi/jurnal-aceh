/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50531
 Source Host           : localhost
 Source Database       : jurnal-aceh

 Target Server Type    : MySQL
 Target Server Version : 50531
 File Encoding         : utf-8

 Date: 03/12/2014 09:19:27 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `app_brand`
-- ----------------------------
DROP TABLE IF EXISTS `app_brand`;
CREATE TABLE `app_brand` (
  `b_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_nama` varchar(100) NOT NULL,
  `b_url` varchar(100) NOT NULL,
  `b_img` varchar(255) NOT NULL,
  PRIMARY KEY (`b_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `app_brand`
-- ----------------------------
BEGIN;
INSERT INTO `app_brand` VALUES ('2', 'quick chicken', 'http://quickchicken.co.id', 'e0c1ba4760f1960d8cc42cb0ef276e34.jpg'), ('3', 'HUMA', 'http://huma.co.id', '343963fb5a018824c6dfb7e8b6859347.jpg'), ('4', 'quick coffe', 'http://quickcoffee.co.id', '5776b60b8e7d65420e8b9ee3ff7a4729.jpg'), ('5', 'zubento', 'http://zubento.co.id', '912504408827306dc647de67cf3de48e.jpg');
COMMIT;

-- ----------------------------
--  Table structure for `app_contact`
-- ----------------------------
DROP TABLE IF EXISTS `app_contact`;
CREATE TABLE `app_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `msg` text NOT NULL,
  `read` int(11) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `app_slideshow`
-- ----------------------------
DROP TABLE IF EXISTS `app_slideshow`;
CREATE TABLE `app_slideshow` (
  `ss_id` int(11) NOT NULL AUTO_INCREMENT,
  `ss_title` varchar(100) NOT NULL,
  `ss_desc` varchar(255) NOT NULL,
  `ss_img` varchar(255) NOT NULL,
  `ss_direction` varchar(50) NOT NULL,
  `ss_bg` varchar(100) NOT NULL,
  PRIMARY KEY (`ss_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `app_slideshow`
-- ----------------------------
BEGIN;
INSERT INTO `app_slideshow` VALUES ('3', 'Penghargaan Waralaba Paling Sukses', 'Waralaba Paling Sukses', '1c661c6fa2bfe886d65ab38bb8cd3867.png', 'rtl', 'white'), ('5', 'Franchise Makanan', 'Investasi terjangkau dan dapatkan kepastian keuntungan dari masing-masing Brands', 'ced87d37cba8fbe4b2a8ce0e6161aaa2.png', 'ltr', 'white');
COMMIT;

-- ----------------------------
--  Table structure for `app_testi`
-- ----------------------------
DROP TABLE IF EXISTS `app_testi`;
CREATE TABLE `app_testi` (
  `t_id` int(11) NOT NULL AUTO_INCREMENT,
  `t_name` varchar(50) NOT NULL,
  `t_company` varchar(255) NOT NULL,
  `t_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `t_msg` text NOT NULL,
  `t_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`t_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `gs_artikel`
-- ----------------------------
DROP TABLE IF EXISTS `gs_artikel`;
CREATE TABLE `gs_artikel` (
  `art_id` int(11) NOT NULL AUTO_INCREMENT,
  `art_link` varchar(100) NOT NULL,
  `art_judul` varchar(100) NOT NULL,
  `art_content` text NOT NULL,
  `art_keyword` varchar(255) NOT NULL,
  `art_description` text NOT NULL,
  PRIMARY KEY (`art_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_artikel`
-- ----------------------------
BEGIN;
INSERT INTO `gs_artikel` VALUES ('1', 'About-Us', 'About Us', '<p style=\"text-align: justify;\">&nbsp;</p>\n<p style=\"text-align: justify;\"><strong>Holding company</strong> menjadi isu strategis bagi kelompok perusahaan. Dalam kemasan <strong>holding company</strong> penyelarasan berbagai aspek bisnis, optimalisasi pengelolaan sumber daya dan portfolio bisnis yang berujung pada peningkatan nilai tambah suatu perusahaan, serta institusionalisasi sistem dapat ditampung. Kenyataannya memang masih banyak dijumpai <strong>Holding Company</strong> yang belum dikelola dengan baik sehingga justru menjadi beban baik bagi perusahaan induk maupun anak perusahaan serta afiliasinya, dan nilai tambah yang diharapkan meleset.</p>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" title=\"Bedi Zubaedi Food Specialist \" alt=\"Bedi Zubaedi Food Specialist \" src=\"http://www.bedicorporation.com/assets/editor/finder/files/food%20specialist.jpg\" /></p>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p style=\"text-align: justify;\"><strong>Bedi Zubaedi</strong> sebagai founder dari <strong>Quick Chicken, Zu Bento, HUMA Ribs Steak &amp; Shake, Quick Coffee, </strong>dan<strong> Bebek Nusantara</strong> mengukuhkan diri membentuk sebuah <strong>Holding Company</strong> yang memungkinkan perusahaan membangun, mengendalikan, mengola, mengkonsolidasikan serta mengkordinasikan aktivitas dalam sebuah lingkungan multibisnis. Bedi Corporation hadir melalui serangkaian tahapan yang dirumuskan secara jelas yaitu pemahaman seputar definisi pasar, karakteristik, serta faktor kunci penunjang kesuksesan sebuah <strong>Holding Company</strong>. Demikian pula aspek-aspek strategis seperti aspek finansial, struktur organisasi, dan sumber daya manusia harus dirumuskan secara jelas agar dapat mempermudah pengembangan Holding Company, maka hadirlah <strong>Holding Company Bedi Corporation</strong> dengan sistem <strong>franchise</strong> / <strong>waralaba</strong>.</p>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p style=\"text-align: justify;\"><strong>Bedi Corporation</strong> memiliki anak perusahaan dengan konsep yang berbeda setiap brandnya. <strong>Quick Chicken</strong> yang berkonsep restaurant fast food <strong>fried chicken</strong>, <strong>Zu Bento</strong> dengan konsep restaurant <strong>fast food bento</strong>, <strong>HUMA Ribs Steak &amp; Shake</strong> menyajikan aneka macam steak, <strong>Quick Coffee</strong> yang ikut meramaikan pasar penikmat coffee, dan <strong>Bebek Nusantara</strong> yang menghadirkan sajian <strong>makanan tradisional</strong>.</p>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p style=\"text-align: justify;\">Dewasa ini banyak kita lihat betapa meluasnya <strong>perkembangan bisnis</strong> di Indonesia ini. Salah satu diantaranya adalah perkembangan <strong>bisnis franchise</strong> khususnya <strong>bisnis franchise makanan minuman</strong> dimana bisnis tersebut sangat meloncat naik dibanding dengan tahun-tahun sebelumnya. <strong>Bedi Corporation </strong>sebagai salah satu perusahaan dengan sistem <strong>franchise makanan minuman</strong> telah membuktikannya melalui banyak prestasi yang diraih seperti Asia Pasific Entreprenuer Award 2011, Asia Pasific Business &amp; Company Award 2011, Market Leader dan Franchise Fastest Growing 2011 dan 2012, Waralaba Paling Sukses 2012, dan masih banyak yang lainnya.</p>', 'Holding Company, Bedi Corporation, Franchise Makanan Minuman', 'Dengan Investasi hanya Rp. 300jutaan sudah dapat memiliki Brand dari Bedi Corporation');
COMMIT;

-- ----------------------------
--  Table structure for `gs_berita`
-- ----------------------------
DROP TABLE IF EXISTS `gs_berita`;
CREATE TABLE `gs_berita` (
  `berita_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `berita_judul` varchar(255) NOT NULL,
  `berita_link` varchar(100) NOT NULL,
  `berita_img` varchar(255) NOT NULL,
  `berita_isi` text NOT NULL,
  `berita_keyword` varchar(255) NOT NULL,
  `berita_description` text NOT NULL,
  `berita_date` date NOT NULL,
  PRIMARY KEY (`berita_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_berita`
-- ----------------------------
BEGIN;
INSERT INTO `gs_berita` VALUES ('1', 'Memilih Investasi Franchise ', 'Memilih-Investasi-Franchise', '524a6fa4b443a610c8c9436c6ac37087.jpg', '<div style=\"text-align: justify;\" align=\"center\"></div>\n<p style=\"text-align: justify;\"><b>Waralaba</b> atau <strong>franchise</strong> menjadi pilihan yang banyak dipilih oleh banyak orang supaya bisa memiliki usaha sendiri. Dengan keunggulan yang dimiliki dengan penggunaan sistem <b>Waralaba</b> / <b>Franchise</b>, maka banyak <b>investor</b> memutuskan untuk berbisnis dengan usaha bisnis <b>Waralaba / Franchise</b>. Oleh karena itulah usaha <b>waralaba</b> semakin banyak dan menjamur di tanah air.</p>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p style=\"text-align: justify;\">Kesuksesan usaha dengan penggunaan sistem <b>Bisnis Waralaba / Franchise</b> untuk meraih profitabilitas dan brand image yang makin dikenal di masyarakat luas telah membuat banyak orang tergiur untuk membuka usaha sejenis. Keuntungan yang tentunya tidak sedikit dalam jangka waktu yang relatif singkat telah membuat banyak orang membuka usaha <b>waralaba</b> untuk kesuksesan bisnisnya.</p>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p style=\"text-align: justify;\">Sebenarnya sah saja untuk membuka suatu usaha apalagi sejenis usaha dengan sistem bisnis <b>waralaba</b> ini, tapi tetap saja Anda sebagai <b>investor</b>&nbsp; harus teliti sebelum membuka usaha sejenis <b>waralaba </b>yang anda inginkan. Jangan sampai usaha ini berhenti di tengah jalan ataupun membuat anda menyesal pada akhirnya. Untuk itu Anda memerlukan kiat khusus sebelum benar-benar menjalankan <b>bisnis waralaba</b> / <b>franchise</b>. Anda yang tertarik untuk membuka usaha <b>bisnis waralaba</b> dapat menyimak kiat khusus sebagai berikut sebagi tips untuk Anda memilih <b>bisnis waralaba</b> yang tepat untuk <b>berinvestasi</b>.</p>\n<ul style=\"text-align: justify;\">\n</ul>\n<ol>\n<li>Ketahuilah portofolio bisnis /      background perusahaan yang akan Anda pilih sebagai pilihan untuk      berinvestasi.</li>\n<li>Pastikan sistem manajemen      bisnis perusahaan profesional.</li>\n<li>Carilah informasi mengenai      bisnis yang akan Anda masuki.</li>\n<li>Apabila ada Franchise yang      tergolong baru dalam usaha bisnis <b>waralaba</b> / <b>franchise</b>, yakinkan      bahwa <b>Franchise&nbsp;</b> tersebut memiliki perencanaan bisnis kedepan yang      bagus. Tidak ada salahnya Anda memilih usaha bisnis <b>waralaba</b> dari      Pendatang Baru sebagai pilihan Anda, karena keuntungan dari Pendatang Baru      adalah Investasinya yang Rendah, jika Anda Jeli menaksir prospek usaha      tersebut kedepan, Anda tentu akan meraih banyak keuntungan.</li>\n<li>Cari tahu terlebih dahulu      reputasi bisnis perusahaan franchise. Banyaknya perusahaan franchise yang      bergabung bukan jaminan perusahaan tersebut memiliki kinerja yang bagus      pula. Pilihlha yang telah memiliki suatu Holding Company.</li>\n<li>Mampu menjadi inovator dan      creator. Jangan hanya sekedar menjalankan SOP (Standard Operating      Procedure) tapi anda juga dituntut jeli melihat dan mencari peluang.</li>\n</ol>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p>Selamat memilih bisnis <b>Waralaba / Franchise</b>untuk kesempatan Anda berinvestasi. Jika Anda ingin membuka Usaha Bisnis Restaurant <b>Fried Chicken</b> , <strong>Fast Food Bento</strong>, <strong>Restaurant Franchise Steak</strong>, <strong>Franchise Minuman Coffee</strong>, <strong>Franchise Bebek Nusantara</strong> yang rasanya sudah pasti menyaingi Brand Internasional namun harga jual dapat menjangkau segmentasi menengah ke bawah dengan <b>investasi</b> yang terjangkau.&nbsp;</p>\n<p>&nbsp;</p>\n<p style=\"text-align: justify;\">Tertarik mengembangkan usaha <strong>waralaba franchise</strong>? Wujudkan mimpi Anda segera dengan cara bisnis yang satu ini bersama Bedi Corporation ; <strong>Quick Chicken</strong>, <strong>Zu Bento</strong>, <strong>HUMA Ribs Steak &amp; Shake</strong>, <strong>Quick Coffee</strong>, dan <strong>Bebek Nusantara</strong>.</p>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p style=\"text-align: justify;\">Informasi <strong>Bisnis Franchise Makanan</strong> dan <strong>Franchise Minuman</strong> : <b>081335005331 / 081910113875<br /></b></p>', 'Holding Company, Bedi Corporation, Franchise Makanan Minuman', 'Dengan Investasi hanya Rp. 300jutaan sudah dapat memiliki Brand dari Bedi Corporation', '2013-07-15'), ('2', 'Penghargaan Franchise I Bedi Corporation', 'Penghargaan-Franchise-I-Bedi-Corporation', 'ec0ca0d25c6268a0af3dcfbe3c2288d0.jpg', '<div style=\"text-align: justify;\">Bedi Corporation sebagai perusahaan <strong>holding company</strong> yang bergerak di bidang food &amp; beverage dan memiliki sistem kerjasama <b>franchise / waralaba</b> dengan investasi yang terjangkau. Dalam kurun waktu beberapa tahun ini  banyak sekali prestasi yang ditorehkan oleh <strong>Bedi Corporation</strong> sebagai <b>holding company. </b>Salah  satunya adalah&nbsp; dengan menerima 3 penghargaan sekaligus yaitu <strong>Asia  Pasific Enterpreneur Award</strong> sebagai Most Promising Enterpreneurship  Award, <strong>Franchise &amp; Bussiness Opportunity</strong> sebagai The Market Leader  Franchise &amp; Business Opportunity Resto Fried Chicken, dan <strong>Asia  Pasific Business &amp; Company Award</strong> sebagai The Best Quality &amp;  Service Excellence of the Year 2011, sedangkan pada tahun 2012, Bedi  Corporation kembali menerima penghargaan sebagai Indonesia Inspiring  Food &amp; Beverage, <strong>Waralaba Paling Sukses 2012</strong>, dan masuk dalam <strong>Top 8  Franchise Start Up</strong> 2012, <strong>Market Leader 2012</strong> dan <strong>Fastest Growing 2012</strong>.</div>\n<div style=\"text-align: justify;\"></div>\n<div style=\"text-align: justify;\">Dengan beberapa prestasi yang diraih oleh perusahaan yang memiliki sistem kerjasama <b>bisnis franchise makanan </b>dan <strong>franchise minuman</strong>, diharapkan hal tersebut dapat menjadi motivasi untuk menjadi lebih baik lagi. <strong>Bedi Corporation</strong> yang telah memiliki 5 brands saat ini yaitu <strong>Quick  Chicken, Zu Bento, HUMA Ribs Steak &amp; Shake, Quick Coffee</strong>, dan yang  baru lahir kembali menyemarakan pasar kuliner di nusantara yaitu <strong>Bebek  Nusantara</strong>. Bedi Corporation akan selalu memberikan pelayanan yang baik,  kualitas produk yang sempurna guna memuaskan keinginan customer  masing-masing brands.</div>\n<div style=\"text-align: justify;\"></div>\n<div style=\"text-align: justify;\">Tertarik mengembangkan <b>usaha franchise</b> <strong>makanan </strong>dan <strong>franchise minuman</strong> bersama  brand-brand yang dimiliki oleh <strong>Bedi Corporation</strong> ? Wujudkan impian anda  memiliki bisnis dengan keuntungan yang tak terduga.</div>\n<div style=\"text-align: justify;\"></div>\n<div style=\"text-align: justify;\">Informasi<b> franchise / waralaba</b> : <i><b>081335005331 / 0812 237 0214</b></i></div>', 'Holding Company, Bedi Corporation, Franchise Makanan Minuman', 'Dengan Investasi hanya Rp. 300jutaan sudah dapat memiliki Brand dari Bedi Corporation', '2013-07-15'), ('3', 'Peresmian Kantor Baru Bedi Corporation', 'Peresmian-Kantor-Baru-Bedi-Corporation', '6ea707ab3b5396bf16487d15aa586b0f.jpg', '<p style=\"text-align: justify;\">Pada 15 Juni 2013, <b>Bedi Corporation</b> selaku holding company dari perusahaan <b>franchise makanan</b> dan <strong>franchise minuman</strong> ; <strong>Quick Chicken</strong>, <strong>Zu Bento</strong>, <strong>HUMA Ribs Steak &amp; Shake</strong>, <strong>Quick Coffee</strong> dan <strong>Bebek Nusantara</strong> melakukan acara ceremonial <b>peresmian kantor baru</b> Bedi Corporation.</p>\n<p style=\"text-align: justify;\">Kantor baru <strong>Bedi Corporation</strong> yang terletak di Jl. Imogiri Timur KM 2.5 Bantul - Yogyakarta ini memiliki bangunan yang cukup luas dan nantinya akan mempermudah untuk mengendalikan, mengelolah, mengkonsolidasikan serta mengkordinasikan aktivitas dalam sebuah lingkungan multibisnis dengan penggunaan sistem bisnis <b>franchise</b> atau <strong>waralaba makanan</strong> dan <strong>waralaba minuman</strong>.</p>\n<p style=\"text-align: justify;\">Peresmian kantor baru <strong>Bedi Corporation</strong> dibuka dengan acara yang hikmat dengan menghadirkan anak-anak berbakat dari Panti Asuhan, Team Mawaris, Profile dari <strong>Bedi Corporation</strong>, Penghargaan Karyawan, dan ditutup dengan Musik Tradisional Jogja.</p>\n<p style=\"text-align: justify;\">Dengan adanya kantor baru Bedi Corporation sebagai perusahaan yang memiliki banyak brand dengan menggunakan sistem <strong>bisnis franchise</strong> ini diharapkan akan menambah kinerja team dari berbagai aspek strategis yaitu seperti aspek finansial, struktur organisasi, dan sumber daya manusia yang harus dirumuskan secara jelas agar dapat mempermudah pengembangan Holding Company <b>Bedi Corporation</b>.</p>\n<p style=\"text-align: justify;\">&nbsp;</p>', 'Peresmian Kantor, Bedi Corporation, Franchise Makanan, Franchise Minuman, Trend Franchise Makanan 2013', 'Dengan Investasi hanya Rp. 300jutaan sudah dapat memiliki Brand dari Bedi Corporation', '2013-07-15'), ('4', 'Bedi Corporation I Franchise Makanan I Franchise Minuman', 'Bedi-Corporation-I-Franchise-Makanan-I-Franchise-Minuman', '9676f572cbdf080205251dbf589a7219.jpg', '<p><strong>Bedi Corporation </strong>merupakan <strong>holding company</strong> yang membawahi 5 Brand yang bergerak dibidang food and beverage sebagai perusahaan yang memiliki sistem kerjasama <b>Franchise</b> atau <b>Waralaba</b>, Adapun anak perusahaan dari holding company&nbsp;<strong>Bedi Corporation</strong> yaitu Quick Chicken, Zu Bento, HUMA Ribs, Steak &amp; Shake, Quick Coffee, dan Bebek Nusantara.</p>\n<p style=\"text-align: justify;\">Produk yang diolah oleh tangan-tangan terlatih dan professional ini akan menjadi jaminan ke<b><i>puas</i></b>an citarasa untuk pelanggan. Pelayanan yang mengutamakan keramahtamahan dan harga yang terjangkau oleh pelanggan menjadikan <b>Bedi Corporation </b>hadir menyajikan pilihan baru yang <b><i>pas</i></b> bagi penikmat kuliner di Indonesia.</p>\n<p style=\"text-align: justify;\">Berawal  pada tahun 2000 quick chicken hadir di yogyakarta dan terus bergerak di  hampir seluruh wilayah Indonesia, dan pada awal 2008 saat store  berjumlah 34 <strong>Quick chicken</strong> untuk pertama kalinya di <strong>franchise</strong> kan, dan sampai saat ini Quick Chicken telah mencapai angka 240 store di seluruh Indonesia dan akan terus berkembang.</p>\n<p style=\"text-align: justify;\">Pada  bulan juni 2011 Quick Bento muncul untuk pertama kalinya di Bojonegoro  dan sampai saat ini sudah ada 4 store di pulau jawa. Karna adanya  kesamaan dengan Quick Chicken sehingga banyak customer yang memesan menu  quick chicken di Quick Bento, untuk itu ditahun 2012 Quick Bento resmi  berganti nama dengan <strong>Zu Bento</strong>. <strong>Zu Bento</strong> saat ini telah memiliki total 9 store dan akan terus berkembang meramaikan pasar makanan cepat saji jepang.</p>\n<p style=\"text-align: justify;\">Tidak berhenti sampai disitu pada awal tahun 2012 lahirlah <strong>HUMA Ribs Steak &amp; Shake</strong>,  restaurant dengan menu utama steak ini menjadi incaran para customer  karena sesuai dengan selera customer saat ini khususnya anak muda. <strong>HUMA Ribs Steak &amp; Shake</strong> saat ini telah memiliki total 9 store dan akan terus berkembang.</p>\n<p style=\"text-align: justify;\"><strong>Quick Coffee</strong> sebagai bagian dari <strong>Quick Chicken</strong> dihadirkan untuk memenuhi keinginan pelanggan Quick Chicken. <strong>Quick Coffee</strong> yang hadir di akhir tahun 2012 memberikan suasana yang baru di Quick  Chicken dan disambut baik oleh masyarakat. Quick Coffee saat ini telah  memiliki total 4 store dan akan berkembang serta berdampingan bersama <strong>Quick Chicken</strong>.</p>\n<p style=\"text-align: justify;\"><strong>Bebek Nusantara</strong> hadir dan meramaikan dunia kulinari khas makanan tradisional Indonesia. <strong>Bebek Nusantara</strong> hadir di pertengan Tahun 2013 guna melestarikan kulinari khas Indonesia. <strong>Bebek Nusantara</strong> pertama kali hadir di <strong>Yogyakarta</strong> yang terkenal dengan kota wisata.</p>\n<p style=\"text-align: justify;\">Setelah 5 brand itu lahir, maka terciptalah sebuah <strong>holding company</strong> untuk membawahi 5 brand tersebut yaitu <strong>Bedi Corporation</strong>.&nbsp;Adapun pionirnya adalah Bapak H. Bedi Zubaedi yang telah <b>berpengalaman</b> di dunia Hospitality Industri sejak tahun 1987.</p>\n<p style=\"text-align: justify;\">Sang <b>FOOD SPECIALIST </b>hadir  dengan memberikan banyak varian menu makanan yang dapat dinikmati  seluruh kalangan, dengan pelayanan yang ramah, jaminan citarasa untuk  pelanggan serta dengan harga yang terjangkau. <b>BEDI CORPORATION </b>hadir dengan membawa banyak pilihan yang pas bagi penikmat <strong>kuliner Indonesia</strong> dan Anda yang ingin bekerjasama dan berinvestasi bersama kami di <strong>Bisnis Franchise Makanan</strong> dan <strong>Franchise Minuman</strong> bersama <strong>Bedi Corporation</strong> \"<strong>Food Specialist</strong>\"</p>', 'Holding Company, Bedi Corporation, Franchise Makanan Minuman', 'Dengan Investasi hanya Rp. 300jutaan sudah dapat memiliki Brand dari Bedi Corporation', '2013-07-15');
COMMIT;

-- ----------------------------
--  Table structure for `gs_berita_kategori`
-- ----------------------------
DROP TABLE IF EXISTS `gs_berita_kategori`;
CREATE TABLE `gs_berita_kategori` (
  `berita_id` bigint(20) NOT NULL,
  `kategori` varchar(11) NOT NULL,
  PRIMARY KEY (`berita_id`,`kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_berita_kategori`
-- ----------------------------
BEGIN;
INSERT INTO `gs_berita_kategori` VALUES ('1', 'Franchise M'), ('2', 'Franchise M'), ('3', 'Franchise M'), ('4', 'Franchise M');
COMMIT;

-- ----------------------------
--  Table structure for `gs_captcha`
-- ----------------------------
DROP TABLE IF EXISTS `gs_captcha`;
CREATE TABLE `gs_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `captcha_time` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=MyISAM AUTO_INCREMENT=197 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_captcha`
-- ----------------------------
BEGIN;
INSERT INTO `gs_captcha` VALUES ('196', '1394582339', '::1', '5744');
COMMIT;

-- ----------------------------
--  Table structure for `gs_config`
-- ----------------------------
DROP TABLE IF EXISTS `gs_config`;
CREATE TABLE `gs_config` (
  `cog_id` int(11) NOT NULL AUTO_INCREMENT,
  `cog_value` text NOT NULL,
  PRIMARY KEY (`cog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_config`
-- ----------------------------
BEGIN;
INSERT INTO `gs_config` VALUES ('1', 'Bedi Corporation sebagai perusahaan yang bergerak di Bidang Franchise Makanan dan Franchise Minuman sejak tahun 2000 dan telah banyak meraih prestasi yang membanggakan seperti Asia Pasific Enterpreneur Award sebagai Most Promising Enterpreneurship Award, Franchise & Bussiness Opportunity sebagai The Market Leader Franchise & Business Opportunity Resto Fried Chicken, dan Asia Pasific Business & Company Award sebagai The Best Quality & Service Excellence of the Year 2011, sedangkan pada tahun 2012, Bedi Corporation kembali menerima penghargaan sebagai Indonesia Inspiring Food & Beverage, Waralaba Paling Sukses 2012, dan masuk dalam Top 8 Franchise Start Up 2012, Market Leader 2012 dan Fastest Growing 2012.'), ('2', '<p>Bedi corporation merupakan holding company yang memungkinkan perusahaan membangun, mengendalikan, mengola,  mengkonsolidasikan serta mengkordinasikan aktivitas dalam sebuah  lingkungan multibisnis.</p>'), ('3', '<ul class=\"getintouch\">\n<li><i class=\"icon-map-marker\"></i><strong>Adress: </strong>Jl. Imogiri Timur KM 6.5 Nglebeng RT 7 Desa Tamanan</li>\n<li><i class=\"icon-phone\"></i><strong>Phone: </strong>(+48) 439 6636</li>\n<li><strong>Mail: </strong>bedicorporation@yahoo.com<strong><br /></strong>\n<table height=\"112\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"335\">\n<colgroup><col width=\"145\"></col></colgroup> \n<tbody>\n<tr height=\"17\">\n<td class=\"xl66\" style=\"height: 12.75pt; width: 109pt;\" height=\"17\" width=\"145\"><br /></td>\n</tr>\n</tbody>\n</table>\n</li>\n</ul>'), ('4', '<div class=\"widget\">\n	<h3>Information</h3>\n	<p>Phasellus ultricies id suscipit mauris monte lobortis. \n	Cum sociis natoque penatibus magnis parturient.</p>\n</div>\n\n<div class=\"widget\">\n<h3>General Inquiries</h3>\n	<ul>\n		<li><i class=\"icon-building\"></i>954 Yellow Av.</li>\n		<li><i class=\"icon-map-marker\"></i>Los Angeles, CA 90185, USA</li>\n	</ul>\n	<ul>\n		<li><i class=\"icon-phone\"></i>(123) 880 440 110</li>\n		<li><i class=\"icon-envelope\"></i>support@example.com</li>\n		<li><i class=\"icon-globe\"></i>www.example.com</li>\n	</ul>\n</div>\n\n<div class=\"widget\">\n	<h3>Business Hours</h3>\n	<ul>\n		<li><i class=\"icon-time \"></i>Monday - Friday 8 am to 4 pm</li>\n		<li><i class=\"icon-time \"></i>Saturday 9 am to 2 pm</li>\n		<li><i class=\"icon-ban-circle\"></i>Sunday Closed</li>\n	</ul>\n</div>'), ('5', '<iframe width=\"425\" height=\"350\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.com/maps/ms?msa=0&amp;msid=202433578992789968751.0004e1a9535b6ce8af440&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=-6.161548,106.810455&amp;spn=0,0&amp;output=embed\"></iframe><br /><small>View <a href=\"https://maps.google.com/maps/ms?msa=0&amp;msid=202433578992789968751.0004e1a9535b6ce8af440&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=-6.161548,106.810455&amp;spn=0,0&amp;source=embed\" style=\"color:#0000FF;text-align:left\">Bedi Corporation HQ</a> in a larger map</small>'), ('6', 'keyword'), ('7', 'description');
COMMIT;

-- ----------------------------
--  Table structure for `gs_gallery`
-- ----------------------------
DROP TABLE IF EXISTS `gs_gallery`;
CREATE TABLE `gs_gallery` (
  `gallery_id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `gallery_description` varchar(255) NOT NULL,
  `gallery_url` varchar(255) NOT NULL,
  PRIMARY KEY (`gallery_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_gallery`
-- ----------------------------
BEGIN;
INSERT INTO `gs_gallery` VALUES ('4', '4', 'asdsad', '357e0ef5ca9493d471c4e6e0966dfc74.jpg'), ('5', '4', 'sdas asdsadad', 'fb10cd39268b88264239713b6457f783.jpg'), ('8', '6', 'Franchise Fried Chicken Chicken', 'aaad4de5c38c08653bf2a04ea1a79aec.jpg'), ('9', '6', 'Franchise Fried Chicken', '3061186c75cef2a2d4b772bc0cd6f9f0.jpg'), ('10', '6', 'Franchise Fried Chicken', 'eace47103851299f5d78494eee320009.jpg'), ('11', '6', 'Franchise Fried Chicken', 'f1cc06374ef7947b57e338b6b00bfb2f.jpg'), ('12', '6', 'Franchise Fried Chicken', '7188a763d1993456efd0e36c4cf52dfc.jpg'), ('13', '6', 'Franchise Fried Chicken', '6ddf00d6baf783778e3f6cdc9b4c51f1.jpg');
COMMIT;

-- ----------------------------
--  Table structure for `gs_gallery_album`
-- ----------------------------
DROP TABLE IF EXISTS `gs_gallery_album`;
CREATE TABLE `gs_gallery_album` (
  `album_id` int(11) NOT NULL AUTO_INCREMENT,
  `album_nama` varchar(255) NOT NULL,
  `album_sort` int(11) NOT NULL,
  PRIMARY KEY (`album_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_gallery_album`
-- ----------------------------
BEGIN;
INSERT INTO `gs_gallery_album` VALUES ('6', 'Quick Chicken', '6'), ('7', 'Zu Bento', '7'), ('8', 'HUMA Ribs Steak & Shake', '8'), ('9', 'Quick Coffee', '9'), ('10', 'Bebek Nusantara', '10');
COMMIT;

-- ----------------------------
--  Table structure for `gs_kategori`
-- ----------------------------
DROP TABLE IF EXISTS `gs_kategori`;
CREATE TABLE `gs_kategori` (
  `kategori_id` int(11) NOT NULL,
  `kategori_nama` varchar(255) NOT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `gs_menu`
-- ----------------------------
DROP TABLE IF EXISTS `gs_menu`;
CREATE TABLE `gs_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_nama` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `menu_sort` int(11) NOT NULL,
  `menu_link` varchar(255) NOT NULL,
  `menu_type` enum('modul','artikel','eksternal') DEFAULT NULL,
  `menu_content` text NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_menu`
-- ----------------------------
BEGIN;
INSERT INTO `gs_menu` VALUES ('3', 'Gallery', '0', '6', 'gallery', 'modul', 'gallery'), ('4', 'About Us', '0', '7', 'page/About-Us', 'artikel', '1'), ('6', 'News Room', '0', '4', 'berita', 'modul', 'berita'), ('7', 'Home', '0', '1', '', 'modul', ''), ('9', 'Contact Us', '0', '2', 'contact', 'modul', 'contact'), ('14', 'Testimonial', '0', '5', 'testimonial', 'modul', 'testimonial'), ('15', 'asdsasadsa asdasd', '0', '3', '', null, ''), ('16', 'asdasd', '0', '8', '', null, '');
COMMIT;

-- ----------------------------
--  Table structure for `gs_mn_wgt`
-- ----------------------------
DROP TABLE IF EXISTS `gs_mn_wgt`;
CREATE TABLE `gs_mn_wgt` (
  `mn_wgt_id` int(11) NOT NULL AUTO_INCREMENT,
  `mn_Id` int(11) NOT NULL,
  `wgt_id` int(11) NOT NULL,
  PRIMARY KEY (`mn_wgt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_mn_wgt`
-- ----------------------------
BEGIN;
INSERT INTO `gs_mn_wgt` VALUES ('21', '7', '1'), ('22', '7', '3'), ('25', '9', '2'), ('26', '9', '3'), ('29', '3', '1'), ('30', '3', '2'), ('32', '13', '3'), ('33', '13', '1'), ('34', '13', '1'), ('35', '13', '2'), ('38', '12', '1'), ('39', '12', '3'), ('40', '6', '1'), ('53', '4', '2'), ('54', '4', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gs_modul`
-- ----------------------------
DROP TABLE IF EXISTS `gs_modul`;
CREATE TABLE `gs_modul` (
  `modul_id` int(11) NOT NULL AUTO_INCREMENT,
  `modul_nama` varchar(255) NOT NULL,
  `modul_url` varchar(100) NOT NULL,
  PRIMARY KEY (`modul_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_modul`
-- ----------------------------
BEGIN;
INSERT INTO `gs_modul` VALUES ('1', 'Home', ''), ('2', 'Gallery', 'gallery'), ('3', 'Contact Us', 'contact'), ('4', 'Berita', 'berita'), ('5', 'Testimonial', 'testimonial');
COMMIT;

-- ----------------------------
--  Table structure for `gs_page`
-- ----------------------------
DROP TABLE IF EXISTS `gs_page`;
CREATE TABLE `gs_page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_judul` varchar(250) NOT NULL,
  `page_content` text NOT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `gs_social`
-- ----------------------------
DROP TABLE IF EXISTS `gs_social`;
CREATE TABLE `gs_social` (
  `soc_id` int(11) NOT NULL AUTO_INCREMENT,
  `soc_name` varchar(100) NOT NULL,
  `soc_link` varchar(255) NOT NULL,
  `soc_st` int(11) NOT NULL,
  PRIMARY KEY (`soc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_social`
-- ----------------------------
BEGIN;
INSERT INTO `gs_social` VALUES ('1', 'Facebook', 'fb', '1'), ('2', 'Twitter', 'tw', '1'), ('3', 'Google + ', 'gp', '1'), ('4', 'Pinterest', '', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gs_users`
-- ----------------------------
DROP TABLE IF EXISTS `gs_users`;
CREATE TABLE `gs_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `jenis` enum('superadmin','admin') NOT NULL DEFAULT 'admin',
  `status` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_users`
-- ----------------------------
BEGIN;
INSERT INTO `gs_users` VALUES ('1', 'agusdwi89@gmail.com', 'admin', 'ef6726658bb32242cae18a7ce6c0bc74', 'superadmin', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gs_widget`
-- ----------------------------
DROP TABLE IF EXISTS `gs_widget`;
CREATE TABLE `gs_widget` (
  `wgt_id` int(11) NOT NULL AUTO_INCREMENT,
  `wgt_nama` varchar(100) NOT NULL,
  `wgt_tipe` enum('img','script') NOT NULL,
  `wgt_isi` text NOT NULL,
  PRIMARY KEY (`wgt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_widget`
-- ----------------------------
BEGIN;
INSERT INTO `gs_widget` VALUES ('1', 'widget 1', 'img', 'asldkj'), ('2', 'widget 2', 'img', 'a;sdjk sad'), ('3', 'widget 3', 'img', 'asldk sakd');
COMMIT;

-- ----------------------------
--  Table structure for `jr_jurnal`
-- ----------------------------
DROP TABLE IF EXISTS `jr_jurnal`;
CREATE TABLE `jr_jurnal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `judul` varchar(200) DEFAULT NULL,
  `terbitan` year(4) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `jr_jurnal`
-- ----------------------------
BEGIN;
INSERT INTO `jr_jurnal` VALUES ('1', '1', '1', 'judul pertama', '2013', 'alkdjsakljdlsakdlasjdasd', '2014-03-12 07:24:33', '1'), ('2', '1', '1', 'judul kedua', '2013', 'akjdaskdasjdasasd', '2014-03-12 07:24:33', '1'), ('3', '2', '2', 'adklsjahd asl;djasd', '2013', null, '2014-03-12 07:24:33', '1'), ('4', '2', '3', 'asdjlsaj daslkdjasd', '2014', ';alskdla;skdasd', '2014-03-12 07:24:33', '1'), ('5', '3', '1', 'akjdas dlaksjdas dlkasjdas', '2014', 'akljdaskdjaskjdasljd', '2014-03-04 07:35:39', '0'), ('6', '3', '2', '.ajksdlaskjdsad', '2014', 'laskjdlksajdklasjd', '2014-03-12 07:25:18', '1');
COMMIT;

-- ----------------------------
--  Table structure for `jr_kategori`
-- ----------------------------
DROP TABLE IF EXISTS `jr_kategori`;
CREATE TABLE `jr_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `jr_kategori`
-- ----------------------------
BEGIN;
INSERT INTO `jr_kategori` VALUES ('1', '0', 'Gizi Masyarakat', '1', '1'), ('2', '0', 'Gizi Klinik', '2', '1'), ('3', '0', 'Teknologi Pangan', '3', '1'), ('4', '0', 'Manajemen Gizi Institusi', '4', '1'), ('5', '0', 'Kesehatan Umum', '5', '1'), ('6', '0', 'Umum', '6', '1');
COMMIT;

-- ----------------------------
--  Table structure for `jr_prodi`
-- ----------------------------
DROP TABLE IF EXISTS `jr_prodi`;
CREATE TABLE `jr_prodi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `jr_prodi`
-- ----------------------------
BEGIN;
INSERT INTO `jr_prodi` VALUES ('1', '0', 'dIII Gizi', '1', '1'), ('2', '0', 'dIV Gizi', '2', '1');
COMMIT;

-- ----------------------------
--  Table structure for `jr_users`
-- ----------------------------
DROP TABLE IF EXISTS `jr_users`;
CREATE TABLE `jr_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `type` enum('dosen','mahasiswa') DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `reg_date` timestamp NULL DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `status` int(1) DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1 COMMENT='status\n	1 : active\n	-1 : pending approve\n	0 : deleted';

-- ----------------------------
--  Records of `jr_users`
-- ----------------------------
BEGIN;
INSERT INTO `jr_users` VALUES ('1', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '0'), ('2', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('3', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '0'), ('4', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '-1'), ('5', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '0'), ('6', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('7', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('8', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('9', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('10', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '-1'), ('11', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('12', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('13', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('14', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('15', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '0'), ('16', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '-1'), ('17', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('18', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('19', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('20', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('21', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('22', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '-1'), ('23', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('24', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('25', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('26', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('27', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('28', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '-1'), ('29', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('30', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('31', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('32', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('33', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('34', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '-1'), ('35', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('36', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '-1'), ('37', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('38', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('39', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('40', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '-1'), ('41', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '-1'), ('42', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '-1'), ('43', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('44', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('45', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '1'), ('46', 'askjdhas@adsdas.com', '1', 'mahasiswa', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '-1'), ('47', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '-1'), ('48', 'askjdhas@adsdas.com', '1', 'dosen', 'asdsad asd asd asd ', null, '2014-03-04 04:01:20', '2004-01-23 00:00:00', '-1');
COMMIT;

-- ----------------------------
--  View structure for `v_b_kat`
-- ----------------------------
DROP VIEW IF EXISTS `v_b_kat`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_b_kat` AS select `gs_berita_kategori`.`kategori` AS `kategori`,`gs_berita_kategori`.`berita_id` AS `berita_id` from `gs_berita_kategori` group by `gs_berita_kategori`.`kategori`;

-- ----------------------------
--  View structure for `v_berita_kategori`
-- ----------------------------
DROP VIEW IF EXISTS `v_berita_kategori`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_berita_kategori` AS select `gb`.`berita_id` AS `berita_id`,`gb`.`berita_judul` AS `berita_judul`,`gb`.`berita_link` AS `berita_link`,`gb`.`berita_img` AS `berita_img`,`gb`.`berita_isi` AS `berita_isi`,`gb`.`berita_keyword` AS `berita_keyword`,`gb`.`berita_description` AS `berita_description`,`gb`.`berita_date` AS `berita_date`,`gbk`.`kategori` AS `kategori` from (`gs_berita_kategori` `gbk` join `gs_berita` `gb` on((`gb`.`berita_id` = `gbk`.`berita_id`)));

-- ----------------------------
--  View structure for `v_gs_mn_wgt`
-- ----------------------------
DROP VIEW IF EXISTS `v_gs_mn_wgt`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_gs_mn_wgt` AS select `m`.`wgt_id` AS `wgt_id`,`w`.`wgt_nama` AS `wgt_nama`,`m`.`mn_Id` AS `mn_Id` from (`gs_mn_wgt` `m` join `gs_widget` `w` on((`w`.`wgt_id` = `m`.`wgt_id`)));

-- ----------------------------
--  View structure for `v_jurnal`
-- ----------------------------
DROP VIEW IF EXISTS `v_jurnal`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_jurnal` AS select `j`.`id` AS `id`,`j`.`kategori_id` AS `kategori_id`,`j`.`user_id` AS `user_id`,`j`.`judul` AS `judul`,`j`.`terbitan` AS `terbitan`,`j`.`url` AS `url`,`j`.`tanggal` AS `tanggal`,`j`.`status` AS `status`,`u`.`nama` AS `penulis`,`k`.`nama` AS `kategori` from ((`jr_jurnal` `j` join `jr_users` `u` on((`j`.`user_id` = `u`.`id`))) join `jr_kategori` `k` on((`j`.`kategori_id` = `k`.`id`)));

SET FOREIGN_KEY_CHECKS = 1;
