/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50531
 Source Host           : localhost
 Source Database       : jurnal-aceh

 Target Server Type    : MySQL
 Target Server Version : 50531
 File Encoding         : utf-8

 Date: 03/13/2014 09:19:31 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `app_brand`
-- ----------------------------
DROP TABLE IF EXISTS `app_brand`;
CREATE TABLE `app_brand` (
  `b_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_nama` varchar(100) NOT NULL,
  `b_url` varchar(100) NOT NULL,
  `b_img` varchar(255) NOT NULL,
  PRIMARY KEY (`b_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `app_brand`
-- ----------------------------
BEGIN;
INSERT INTO `app_brand` VALUES ('2', 'quick chicken', 'http://quickchicken.co.id', 'e0c1ba4760f1960d8cc42cb0ef276e34.jpg'), ('3', 'HUMA', 'http://huma.co.id', '343963fb5a018824c6dfb7e8b6859347.jpg'), ('4', 'quick coffe', 'http://quickcoffee.co.id', '5776b60b8e7d65420e8b9ee3ff7a4729.jpg'), ('5', 'zubento', 'http://zubento.co.id', '912504408827306dc647de67cf3de48e.jpg');
COMMIT;

-- ----------------------------
--  Table structure for `app_contact`
-- ----------------------------
DROP TABLE IF EXISTS `app_contact`;
CREATE TABLE `app_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `msg` text NOT NULL,
  `read` int(11) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `app_slideshow`
-- ----------------------------
DROP TABLE IF EXISTS `app_slideshow`;
CREATE TABLE `app_slideshow` (
  `ss_id` int(11) NOT NULL AUTO_INCREMENT,
  `ss_title` varchar(100) NOT NULL,
  `ss_desc` varchar(255) NOT NULL,
  `ss_img` varchar(255) NOT NULL,
  `ss_link` varchar(200) NOT NULL,
  PRIMARY KEY (`ss_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `app_slideshow`
-- ----------------------------
BEGIN;
INSERT INTO `app_slideshow` VALUES ('6', 'coba deh', 'as;ljdsa as;dlkaslkasd', '481b488e1637a917a633fffe6bda9d7f.jpg', 'asdasdsadasdsa'), ('7', 'asdsad', 'asdsadasd', 'fc1ffdbc23c0d77527e0c33a234d9d22.jpg', 'asdasd'), ('8', 'asdasd', 'asdsadasd', '1ba642d7c335a7bda96d2d57ed0612ec.jpg', 'asdsadas dsamdasd asdksadasd');
COMMIT;

-- ----------------------------
--  Table structure for `app_testi`
-- ----------------------------
DROP TABLE IF EXISTS `app_testi`;
CREATE TABLE `app_testi` (
  `t_id` int(11) NOT NULL AUTO_INCREMENT,
  `t_name` varchar(50) NOT NULL,
  `t_company` varchar(255) NOT NULL,
  `t_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `t_msg` text NOT NULL,
  `t_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`t_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `gs_artikel`
-- ----------------------------
DROP TABLE IF EXISTS `gs_artikel`;
CREATE TABLE `gs_artikel` (
  `art_id` int(11) NOT NULL AUTO_INCREMENT,
  `art_link` varchar(100) NOT NULL,
  `art_judul` varchar(100) NOT NULL,
  `art_content` text NOT NULL,
  `art_keyword` varchar(255) NOT NULL,
  `art_description` text NOT NULL,
  PRIMARY KEY (`art_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_artikel`
-- ----------------------------
BEGIN;
INSERT INTO `gs_artikel` VALUES ('1', 'About-Us', 'About Us', '<p style=\"text-align: justify;\">&nbsp;</p>\n<p style=\"text-align: justify;\"><strong>Holding company</strong> menjadi isu strategis bagi kelompok perusahaan. Dalam kemasan <strong>holding company</strong> penyelarasan berbagai aspek bisnis, optimalisasi pengelolaan sumber daya dan portfolio bisnis yang berujung pada peningkatan nilai tambah suatu perusahaan, serta institusionalisasi sistem dapat ditampung. Kenyataannya memang masih banyak dijumpai <strong>Holding Company</strong> yang belum dikelola dengan baik sehingga justru menjadi beban baik bagi perusahaan induk maupun anak perusahaan serta afiliasinya, dan nilai tambah yang diharapkan meleset.</p>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" title=\"Bedi Zubaedi Food Specialist \" alt=\"Bedi Zubaedi Food Specialist \" src=\"http://www.bedicorporation.com/assets/editor/finder/files/food%20specialist.jpg\" /></p>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p style=\"text-align: justify;\"><strong>Bedi Zubaedi</strong> sebagai founder dari <strong>Quick Chicken, Zu Bento, HUMA Ribs Steak &amp; Shake, Quick Coffee, </strong>dan<strong> Bebek Nusantara</strong> mengukuhkan diri membentuk sebuah <strong>Holding Company</strong> yang memungkinkan perusahaan membangun, mengendalikan, mengola, mengkonsolidasikan serta mengkordinasikan aktivitas dalam sebuah lingkungan multibisnis. Bedi Corporation hadir melalui serangkaian tahapan yang dirumuskan secara jelas yaitu pemahaman seputar definisi pasar, karakteristik, serta faktor kunci penunjang kesuksesan sebuah <strong>Holding Company</strong>. Demikian pula aspek-aspek strategis seperti aspek finansial, struktur organisasi, dan sumber daya manusia harus dirumuskan secara jelas agar dapat mempermudah pengembangan Holding Company, maka hadirlah <strong>Holding Company Bedi Corporation</strong> dengan sistem <strong>franchise</strong> / <strong>waralaba</strong>.</p>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p style=\"text-align: justify;\"><strong>Bedi Corporation</strong> memiliki anak perusahaan dengan konsep yang berbeda setiap brandnya. <strong>Quick Chicken</strong> yang berkonsep restaurant fast food <strong>fried chicken</strong>, <strong>Zu Bento</strong> dengan konsep restaurant <strong>fast food bento</strong>, <strong>HUMA Ribs Steak &amp; Shake</strong> menyajikan aneka macam steak, <strong>Quick Coffee</strong> yang ikut meramaikan pasar penikmat coffee, dan <strong>Bebek Nusantara</strong> yang menghadirkan sajian <strong>makanan tradisional</strong>.</p>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p style=\"text-align: justify;\">Dewasa ini banyak kita lihat betapa meluasnya <strong>perkembangan bisnis</strong> di Indonesia ini. Salah satu diantaranya adalah perkembangan <strong>bisnis franchise</strong> khususnya <strong>bisnis franchise makanan minuman</strong> dimana bisnis tersebut sangat meloncat naik dibanding dengan tahun-tahun sebelumnya. <strong>Bedi Corporation </strong>sebagai salah satu perusahaan dengan sistem <strong>franchise makanan minuman</strong> telah membuktikannya melalui banyak prestasi yang diraih seperti Asia Pasific Entreprenuer Award 2011, Asia Pasific Business &amp; Company Award 2011, Market Leader dan Franchise Fastest Growing 2011 dan 2012, Waralaba Paling Sukses 2012, dan masih banyak yang lainnya.</p>', 'Holding Company, Bedi Corporation, Franchise Makanan Minuman', 'Dengan Investasi hanya Rp. 300jutaan sudah dapat memiliki Brand dari Bedi Corporation'), ('2', 'asdasd-asd-asd-as', 'asdasd asd asd as', '<p>sadsad asdas d asd as</p>', 'asdasd', 'asdasd sad as'), ('3', 'Dari-Redaksi', 'Dari Redaksi', '<p>dari redaksi</p>', 'dari redaksi', 'dari redaksi');
COMMIT;

-- ----------------------------
--  Table structure for `gs_berita`
-- ----------------------------
DROP TABLE IF EXISTS `gs_berita`;
CREATE TABLE `gs_berita` (
  `berita_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `berita_judul` varchar(255) NOT NULL,
  `berita_link` varchar(100) NOT NULL,
  `berita_img` varchar(255) NOT NULL,
  `berita_isi` text NOT NULL,
  `berita_keyword` varchar(255) NOT NULL,
  `berita_description` text NOT NULL,
  `berita_date` date NOT NULL,
  PRIMARY KEY (`berita_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_berita`
-- ----------------------------
BEGIN;
INSERT INTO `gs_berita` VALUES ('5', 'asdasdasd asdasd ', 'asdasdasd-asdasd', 'ca363cb2821b26414ee7c9131a438398.jpg', '<p>asdsad asd asd asd asd</p>', 'asdasdd', 'asdsadsad', '2014-03-12');
COMMIT;

-- ----------------------------
--  Table structure for `gs_berita_kategori`
-- ----------------------------
DROP TABLE IF EXISTS `gs_berita_kategori`;
CREATE TABLE `gs_berita_kategori` (
  `berita_id` bigint(20) NOT NULL,
  `kategori` varchar(11) NOT NULL,
  PRIMARY KEY (`berita_id`,`kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_berita_kategori`
-- ----------------------------
BEGIN;
INSERT INTO `gs_berita_kategori` VALUES ('1', 'Franchise M'), ('2', 'Franchise M'), ('3', 'Franchise M'), ('4', 'Franchise M'), ('5', 'adsasd'), ('5', 'asdasd'), ('5', 'asdasda'), ('5', 'asdasdasd'), ('5', 'asdasdasdas');
COMMIT;

-- ----------------------------
--  Table structure for `gs_captcha`
-- ----------------------------
DROP TABLE IF EXISTS `gs_captcha`;
CREATE TABLE `gs_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `captcha_time` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_captcha`
-- ----------------------------
BEGIN;
INSERT INTO `gs_captcha` VALUES ('199', '1394674939', '::1', '0037'), ('200', '1394676380', '::1', '7267');
COMMIT;

-- ----------------------------
--  Table structure for `gs_config`
-- ----------------------------
DROP TABLE IF EXISTS `gs_config`;
CREATE TABLE `gs_config` (
  `cog_id` int(11) NOT NULL AUTO_INCREMENT,
  `cog_value` text NOT NULL,
  PRIMARY KEY (`cog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_config`
-- ----------------------------
BEGIN;
INSERT INTO `gs_config` VALUES ('1', '<h2 class=\"welcome-text\">Welcome to Universe Premium Template</h2>\n<p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, adipisci, quibusdam, ad ab quisquam esse aspernatur exercitationem aliquam at fugit omnis vitae recusandae eveniet.</strong><br><br>Inventore, aliquam sequi nisi velit magnam accusamus reprehenderit nemo necessitatibus doloribus molestiae fugit repellat repudiandae dolor. Incidunt, nulla quidem illo suscipit nihil!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, dolorem, fugiat, commodi totam accusantium illo incidunt quis eius eum iure et fugit voluptas atque ratione nobis sed omnis quod ipsa.<br><br>Vivamus mattis nibh vitae dui egestas posuere. Maecenas a est at enim blandit interdum. Cras eget ipsum ac nunc tristique tincidunt sit amet nec quam. Vivamus sed suscipit enim, et dignissim tellus.</p>'), ('2', '<p>Bedi corporation merupakan holding company yang memungkinkan perusahaan membangun, mengendalikan, mengola,  mengkonsolidasikan serta mengkordinasikan aktivitas dalam sebuah  lingkungan multibisnis.</p>'), ('3', '<ul class=\"getintouch\">\n<li><i class=\"icon-map-marker\"></i><strong>Adress: </strong>Jl. Imogiri Timur KM 6.5 Nglebeng RT 7 Desa Tamanan</li>\n<li><i class=\"icon-phone\"></i><strong>Phone: </strong>(+48) 439 6636</li>\n<li><strong>Mail: </strong>bedicorporation@yahoo.com<strong><br /></strong>\n<table height=\"112\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"335\">\n<colgroup><col width=\"145\"></col></colgroup> \n<tbody>\n<tr height=\"17\">\n<td class=\"xl66\" style=\"height: 12.75pt; width: 109pt;\" height=\"17\" width=\"145\"><br /></td>\n</tr>\n</tbody>\n</table>\n</li>\n</ul>'), ('4', '<div class=\"widget\">\n <h3>Information</h3>\n  <p>Phasellus ultricies id suscipit mauris monte lobortis. \n  Cum sociis natoque penatibus magnis parturient.</p>\n</div>\n\n<div class=\"widget\">\n<h3>General Inquiries</h3>\n <ul>\n    <li><i class=\"icon-building\"></i>954 Yellow Av.</li>\n    <li><i class=\"icon-map-marker\"></i>Los Angeles, CA 90185, USA</li>\n  </ul>\n <ul>\n    <li><i class=\"icon-phone\"></i>(123) 880 440 110</li>\n    <li><i class=\"icon-envelope\"></i>support@example.com</li>\n   <li><i class=\"icon-globe\"></i>www.example.com</li>\n  </ul>\n</div>\n\n<div class=\"widget\">\n <h3>Business Hours</h3>\n <ul>\n    <li><i class=\"icon-time \"></i>Monday - Friday 8 am to 4 pm</li>\n   <li><i class=\"icon-time \"></i>Saturday 9 am to 2 pm</li>\n    <li><i class=\"icon-ban-circle\"></i>Sunday Closed</li>\n </ul>\n</div>'), ('5', '<iframe width=\"425\" height=\"350\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"https://maps.google.com/maps/ms?msa=0&amp;msid=202433578992789968751.0004e1a9535b6ce8af440&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=-6.161548,106.810455&amp;spn=0,0&amp;output=embed\"></iframe><br /><small>View <a href=\"https://maps.google.com/maps/ms?msa=0&amp;msid=202433578992789968751.0004e1a9535b6ce8af440&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=-6.161548,106.810455&amp;spn=0,0&amp;source=embed\" style=\"color:#0000FF;text-align:left\">Bedi Corporation HQ</a> in a larger map</small>'), ('6', 'keyword'), ('7', 'description');
COMMIT;

-- ----------------------------
--  Table structure for `gs_gallery`
-- ----------------------------
DROP TABLE IF EXISTS `gs_gallery`;
CREATE TABLE `gs_gallery` (
  `gallery_id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `gallery_description` varchar(255) NOT NULL,
  `gallery_url` varchar(255) NOT NULL,
  PRIMARY KEY (`gallery_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_gallery`
-- ----------------------------
BEGIN;
INSERT INTO `gs_gallery` VALUES ('4', '4', 'asdsad', '357e0ef5ca9493d471c4e6e0966dfc74.jpg'), ('5', '4', 'sdas asdsadad', 'fb10cd39268b88264239713b6457f783.jpg'), ('8', '6', 'Franchise Fried Chicken Chicken', 'aaad4de5c38c08653bf2a04ea1a79aec.jpg'), ('9', '6', 'Franchise Fried Chicken', '3061186c75cef2a2d4b772bc0cd6f9f0.jpg'), ('10', '6', 'Franchise Fried Chicken', 'eace47103851299f5d78494eee320009.jpg'), ('11', '6', 'Franchise Fried Chicken', 'f1cc06374ef7947b57e338b6b00bfb2f.jpg'), ('12', '6', 'Franchise Fried Chicken', '7188a763d1993456efd0e36c4cf52dfc.jpg'), ('13', '6', 'Franchise Fried Chicken', '6ddf00d6baf783778e3f6cdc9b4c51f1.jpg');
COMMIT;

-- ----------------------------
--  Table structure for `gs_gallery_album`
-- ----------------------------
DROP TABLE IF EXISTS `gs_gallery_album`;
CREATE TABLE `gs_gallery_album` (
  `album_id` int(11) NOT NULL AUTO_INCREMENT,
  `album_nama` varchar(255) NOT NULL,
  `album_sort` int(11) NOT NULL,
  PRIMARY KEY (`album_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_gallery_album`
-- ----------------------------
BEGIN;
INSERT INTO `gs_gallery_album` VALUES ('6', 'Quick Chicken', '6'), ('7', 'Zu Bento', '7'), ('8', 'HUMA Ribs Steak & Shake', '8'), ('9', 'Quick Coffee', '9'), ('10', 'Bebek Nusantara', '10');
COMMIT;

-- ----------------------------
--  Table structure for `gs_kategori`
-- ----------------------------
DROP TABLE IF EXISTS `gs_kategori`;
CREATE TABLE `gs_kategori` (
  `kategori_id` int(11) NOT NULL,
  `kategori_nama` varchar(255) NOT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `gs_menu`
-- ----------------------------
DROP TABLE IF EXISTS `gs_menu`;
CREATE TABLE `gs_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_nama` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `menu_sort` int(11) NOT NULL,
  `menu_link` varchar(255) NOT NULL,
  `menu_type` enum('modul','artikel','eksternal') DEFAULT NULL,
  `menu_content` text NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_menu`
-- ----------------------------
BEGIN;
INSERT INTO `gs_menu` VALUES ('7', 'Home', '0', '1', '', 'modul', ''), ('17', 'Dari Redaksi', '0', '17', '', null, '');
COMMIT;

-- ----------------------------
--  Table structure for `gs_mn_wgt`
-- ----------------------------
DROP TABLE IF EXISTS `gs_mn_wgt`;
CREATE TABLE `gs_mn_wgt` (
  `mn_wgt_id` int(11) NOT NULL AUTO_INCREMENT,
  `mn_Id` int(11) NOT NULL,
  `wgt_id` int(11) NOT NULL,
  PRIMARY KEY (`mn_wgt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_mn_wgt`
-- ----------------------------
BEGIN;
INSERT INTO `gs_mn_wgt` VALUES ('25', '9', '2'), ('26', '9', '3'), ('29', '3', '1'), ('30', '3', '2'), ('32', '13', '3'), ('33', '13', '1'), ('34', '13', '1'), ('35', '13', '2'), ('38', '12', '1'), ('39', '12', '3'), ('40', '6', '1'), ('53', '4', '2'), ('54', '4', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gs_modul`
-- ----------------------------
DROP TABLE IF EXISTS `gs_modul`;
CREATE TABLE `gs_modul` (
  `modul_id` int(11) NOT NULL AUTO_INCREMENT,
  `modul_nama` varchar(255) NOT NULL,
  `modul_url` varchar(100) NOT NULL,
  PRIMARY KEY (`modul_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_modul`
-- ----------------------------
BEGIN;
INSERT INTO `gs_modul` VALUES ('1', 'Home', ''), ('2', 'Gallery', 'gallery'), ('3', 'Contact Us', 'contact'), ('4', 'Berita', 'berita'), ('5', 'Jurnal', 'jurnal');
COMMIT;

-- ----------------------------
--  Table structure for `gs_page`
-- ----------------------------
DROP TABLE IF EXISTS `gs_page`;
CREATE TABLE `gs_page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_judul` varchar(250) NOT NULL,
  `page_content` text NOT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `gs_social`
-- ----------------------------
DROP TABLE IF EXISTS `gs_social`;
CREATE TABLE `gs_social` (
  `soc_id` int(11) NOT NULL AUTO_INCREMENT,
  `soc_name` varchar(100) NOT NULL,
  `soc_link` varchar(255) NOT NULL,
  `soc_st` int(11) NOT NULL,
  PRIMARY KEY (`soc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_social`
-- ----------------------------
BEGIN;
INSERT INTO `gs_social` VALUES ('1', 'Facebook', 'fb', '1'), ('2', 'Twitter', 'tw', '1'), ('3', 'Google + ', 'gp', '1'), ('4', 'Pinterest', '', '0');
COMMIT;

-- ----------------------------
--  Table structure for `gs_users`
-- ----------------------------
DROP TABLE IF EXISTS `gs_users`;
CREATE TABLE `gs_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `jenis` enum('superadmin','admin') NOT NULL DEFAULT 'admin',
  `status` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_users`
-- ----------------------------
BEGIN;
INSERT INTO `gs_users` VALUES ('1', 'agusdwi89@gmail.com', 'admin', 'ef6726658bb32242cae18a7ce6c0bc74', 'superadmin', '1');
COMMIT;

-- ----------------------------
--  Table structure for `gs_widget`
-- ----------------------------
DROP TABLE IF EXISTS `gs_widget`;
CREATE TABLE `gs_widget` (
  `wgt_id` int(11) NOT NULL AUTO_INCREMENT,
  `wgt_nama` varchar(100) NOT NULL,
  `wgt_tipe` enum('img','script') NOT NULL,
  `wgt_isi` text NOT NULL,
  PRIMARY KEY (`wgt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `gs_widget`
-- ----------------------------
BEGIN;
INSERT INTO `gs_widget` VALUES ('1', 'widget 1', 'img', 'asldkj'), ('2', 'widget 2', 'img', 'a;sdjk sad'), ('3', 'widget 3', 'img', 'asldk sakd');
COMMIT;

-- ----------------------------
--  Table structure for `jr_jurnal`
-- ----------------------------
DROP TABLE IF EXISTS `jr_jurnal`;
CREATE TABLE `jr_jurnal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `judul` varchar(200) DEFAULT NULL,
  `terbitan` year(4) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `jr_jurnal`
-- ----------------------------
BEGIN;
INSERT INTO `jr_jurnal` VALUES ('1', '1', '1', 'judul pertama', '2013', 'alkdjsakljdlsakdlasjdasd', '2014-03-12 07:24:33', '1'), ('2', '1', '1', 'judul kedua', '2013', 'akjdaskdasjdasasd', '2014-03-12 07:24:33', '1'), ('3', '2', '2', 'adklsjahd asl;djasd', '2013', null, '2014-03-12 07:24:33', '1'), ('4', '2', '3', 'asdjlsaj daslkdjasd', '2014', ';alskdla;skdasd', '2014-03-12 07:24:33', '1'), ('5', '3', '1', 'akjdas dlaksjdas dlkasjdas', '2014', 'akljdaskdjaskjdasljd', '2014-03-04 07:35:39', '0'), ('6', '3', '2', '.ajksdlaskjdsad', '2014', 'laskjdlksajdklasjd', '2014-03-12 07:25:18', '1');
COMMIT;

-- ----------------------------
--  Table structure for `jr_kategori`
-- ----------------------------
DROP TABLE IF EXISTS `jr_kategori`;
CREATE TABLE `jr_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `jr_kategori`
-- ----------------------------
BEGIN;
INSERT INTO `jr_kategori` VALUES ('1', '0', 'Gizi Masyarakat', '1', '1'), ('2', '0', 'Gizi Klinik', '2', '1'), ('3', '0', 'Teknologi Pangan', '3', '1'), ('4', '0', 'Manajemen Gizi Institusi', '4', '1'), ('5', '0', 'Kesehatan Umum', '5', '1'), ('6', '0', 'Umum', '6', '0');
COMMIT;

-- ----------------------------
--  Table structure for `jr_prodi`
-- ----------------------------
DROP TABLE IF EXISTS `jr_prodi`;
CREATE TABLE `jr_prodi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `jr_prodi`
-- ----------------------------
BEGIN;
INSERT INTO `jr_prodi` VALUES ('3', '0', 'Gizi Masyarakat', '1', '1'), ('4', '0', 'Gizi Klinik', '2', '1'), ('5', '0', 'Teknologi Pangan', '3', '1'), ('6', '0', 'Gizi Institusi', '4', '1');
COMMIT;

-- ----------------------------
--  Table structure for `jr_users`
-- ----------------------------
DROP TABLE IF EXISTS `jr_users`;
CREATE TABLE `jr_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `type` enum('dosen','mahasiswa') DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `reg_date` timestamp NULL DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `status` int(1) DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='status\n 1 : active\n  -1 : pending approve\n  0 : deleted';

-- ----------------------------
--  Records of `jr_users`
-- ----------------------------
BEGIN;
INSERT INTO `jr_users` VALUES ('1', 'asdsadsadas@asdasd.cim', 'a92ac31602bd7ad11897db676882fb27', 'dosen', 'asl;kdjaslkdjasdas', null, null, null, '1'), ('2', 'agusdwi89@gmail.com', '0ad10338e39e386d80d18bd077af3dd2', 'dosen', 'agus dwi', null, null, null, '1');
COMMIT;

-- ----------------------------
--  View structure for `v_b_kat`
-- ----------------------------
DROP VIEW IF EXISTS `v_b_kat`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_b_kat` AS select `gs_berita_kategori`.`kategori` AS `kategori`,`gs_berita_kategori`.`berita_id` AS `berita_id` from `gs_berita_kategori` group by `gs_berita_kategori`.`kategori`;

-- ----------------------------
--  View structure for `v_berita_kategori`
-- ----------------------------
DROP VIEW IF EXISTS `v_berita_kategori`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_berita_kategori` AS select `gb`.`berita_id` AS `berita_id`,`gb`.`berita_judul` AS `berita_judul`,`gb`.`berita_link` AS `berita_link`,`gb`.`berita_img` AS `berita_img`,`gb`.`berita_isi` AS `berita_isi`,`gb`.`berita_keyword` AS `berita_keyword`,`gb`.`berita_description` AS `berita_description`,`gb`.`berita_date` AS `berita_date`,`gbk`.`kategori` AS `kategori` from (`gs_berita_kategori` `gbk` join `gs_berita` `gb` on((`gb`.`berita_id` = `gbk`.`berita_id`)));

-- ----------------------------
--  View structure for `v_gs_mn_wgt`
-- ----------------------------
DROP VIEW IF EXISTS `v_gs_mn_wgt`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_gs_mn_wgt` AS select `m`.`wgt_id` AS `wgt_id`,`w`.`wgt_nama` AS `wgt_nama`,`m`.`mn_Id` AS `mn_Id` from (`gs_mn_wgt` `m` join `gs_widget` `w` on((`w`.`wgt_id` = `m`.`wgt_id`)));

-- ----------------------------
--  View structure for `v_jurnal`
-- ----------------------------
DROP VIEW IF EXISTS `v_jurnal`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_jurnal` AS select `j`.`id` AS `id`,`j`.`kategori_id` AS `kategori_id`,`j`.`user_id` AS `user_id`,`j`.`judul` AS `judul`,`j`.`terbitan` AS `terbitan`,`j`.`url` AS `url`,`j`.`tanggal` AS `tanggal`,`j`.`status` AS `status`,`u`.`nama` AS `penulis`,`k`.`nama` AS `kategori` from ((`jr_jurnal` `j` join `jr_users` `u` on((`j`.`user_id` = `u`.`id`))) join `jr_kategori` `k` on((`j`.`kategori_id` = `k`.`id`)));

-- ----------------------------
--  View structure for `v_terbitan`
-- ----------------------------
DROP VIEW IF EXISTS `v_terbitan`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_terbitan` AS select `jr_jurnal`.`terbitan` AS `terbitan` from `jr_jurnal` group by `jr_jurnal`.`terbitan`;

SET FOREIGN_KEY_CHECKS = 1;
