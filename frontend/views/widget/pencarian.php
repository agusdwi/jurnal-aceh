<?
	$pcr_tahun = $this->db->get('v_terbitan');
?>
<div class="widget-item">
	<div class="request-information">
		<h4 class="widget-title">Pencarian Jurnal</h4>
		<form class="request-info clearfix" method="get" action="<?=base_url()?>jurnal/index/0/"> 

			<div class="full-row">
				<label for="q">Judul Jurnal:</label>
				<input type="text" id="q" name="q">
			</div>

			<div class="full-row">
				<label for="cat">Terbitan tahun:</label>                
				<div class="input-select">
					<select name="tahun" id="tahun" class="postform">
						<option value="">-- pilih tahun --</option>
						<?foreach ($pcr_tahun->result() as $key): ?>
							<option><?=$key->terbitan;?></option>	
						<?endforeach;?>
					</select>
				</div>
			</div>
			<div class="full-row">
				<div class="submit_field">
					<input class="mainBtn pull-right" type="submit" name="" value="Cari">
				</div>
			</div>
		</form>
	</div>
</div>
