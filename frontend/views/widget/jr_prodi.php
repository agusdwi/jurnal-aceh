<?
	$wdt_prodi = $this->db->order_by('sort')->get_where('jr_prodi',array('status'=>1));
?>
<div class="widget-item">
	<div class="request-information">
		<h4 class="widget-title">Program Studi</h4>
		<div class="widget-inner">
			<ul class="mixitup-controls">
				<?foreach ($wdt_prodi->result() as $key): ?>
					<li class="filter">
						<a href="<?=base_url()?>jurnal/index/0?q=&kategori=&prodi=<?=$key->id;?>&tahun="><?=$key->nama;?></a>
					</li>
				<?endforeach;?>
			</ul>
		</div>
	</div>
</div>