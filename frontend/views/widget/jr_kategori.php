<?
	$wdt_kategori = $this->db->order_by('sort')->get_where('jr_kategori',array('status'=>1));
?>
<div class="widget-item">
	<div class="request-information">
		<h4 class="widget-title">Kategori Jurnal</h4>
		<div class="widget-inner">
			<ul class="mixitup-controls">
				<?foreach ($wdt_kategori->result() as $key): ?>
					<li class="filter">
						<a href="<?=base_url()?>jurnal/index/0?q=&kategori=<?=$key->id;?>&prodi=&tahun="><?=$key->nama;?></a>
					</li>
				<?endforeach;?>
			</ul>
		</div>
	</div>
</div>