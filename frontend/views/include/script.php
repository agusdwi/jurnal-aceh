<title><?=(isset($title)) ? $title.' | ' : $this->config->item('web_title');?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="College Education Responsive Template">
<meta name="author" content="Esmet">
<meta charset="UTF-8">

<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800' rel='stylesheet' type='text/css'>

<link href="<?=base_url()?>assets/fe/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="<?=base_url()?>assets/fe/css/font-awesome.min.css" rel="stylesheet" media="screen">
<link href="<?=base_url()?>assets/fe/css/animate.css" rel="stylesheet" media="screen">

<link href="<?=base_url()?>assets/fe/css/style.css" rel="stylesheet" media="screen">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=base_url()?>assets/fe/images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=base_url()?>assets/fe/images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=base_url()?>assets/fe/images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?=base_url()?>assets/fe/images/ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="<?=base_url()?>assets/fe/images/ico/favicon.ico">

<!-- JavaScripts -->
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="<?=base_url()?>assets/fe/js/modernizr.js"></script>

<script src="<?=base_url()?>assets/fe/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/fe/js/plugins.js"></script>
<script src="<?=base_url()?>assets/fe/js/custom.js"></script>
<script src="<?=base_url()?>assets/fe/js/valid.js"></script>