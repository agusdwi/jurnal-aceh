<? $a = $this->session->flashdata('message');?>
<?if(!empty($a)):?>
	<style>
		#flashmsg{
			position: fixed;
			width: 600px;
			z-index: 99999;
			/* right: 10px; */
			border-radius: 3px;
			text-align: center;
			font-size: 100%;
			font-weight: bold;
			color: white;
			background: #fecd0b;
			filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fecd0b', endColorstr='#e7b800');
			background: -webkit-gradient(linear, left top, left bottom, from(#fecd0b), to(#e7b800));
			background: -moz-linear-gradient(top, #fecd0b, #e7b800);
			top: 10px;
			opacity: 0.95;
			left: 50%;
			margin-left: -300px;
		}
	</style>
	<script>
		jQuery.fn.slideFadeToggle = function(speed, easing, callback){
			return this.animate({opacity: 'toggle',height: 'toggle'},speed,easing,callback);
		};
	</script>
	<script>
		$(function(){
			$("#flashmsg").slideFadeToggle('slow').delay(3000).slideFadeToggle('slow');
		})
	</script>
	<div id="flashmsg" style="display:none">
		<div style="margin:20px">
			<?=$a;?>
		</div>
	</div>
<? endif;?>
<? $a = $this->session->flashdata('log');?>
<?if(!empty($a)):?>
	<script type="text/javascript">
		$(function(){
			console.log("<?=$a;?>")
		})
	</script>
<? endif;?>