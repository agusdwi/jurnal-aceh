<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Menu {
	
	private $menu = array();
    private $class = "sf-menu";
    private $id    = "topnav";
    private $current = "current";
	private $sub   = "";
    private $li_class = "";
    private $selected = '';

    function built_tree($data, $parent = 0) {
    	static $i = 1;
    	$c = '';
    	$tab = str_repeat("\t\t", $i);
    	if (isset($data[$parent])) {
    		$html 	= "\n$tab<ul class='".$this->class."' id='".$this->id."'>";
    		$i++;
    		foreach ($data[$parent] as $v) {
    			$child = $this->built_tree($data, $v->menu_id);
    			if ($child) {
    				$c = "hasul";
    			}else $c='';
                $link = ($v->menu_type != 'eksternal') ? base_url($v->menu_link) : $v->menu_link ;
    			$html .= "\n\t$tab<li class='".(($this->selected == $v->menu_id)? $this->current : '' )."'>";
    			$html .= '<a href="'.$link.'">' . $v->menu_nama . '</a>';
    			if ($child) {
    				$i--;
    				$html .= $child;
    				$html .= "\n\t$tab";
    			}
    			$html .= '</li>';
    		}
    		$html .= "\n$tab</ul>";
    		return $html;
    	} else {
    		return false;
    	}
    }
	
	function get($current)
	{
        $this->selected = $current;
		$CI =& get_instance();
		$CI->db->order_by('menu_sort,parent_id');
		$ds = $CI->db->get('gs_menu');
		$data = array();
		foreach ($ds->result() as $row) {
			$data[$row->parent_id][] = $row;
		}
		return $this->built_tree($data);
	}
}