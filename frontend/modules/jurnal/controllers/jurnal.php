<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jurnal extends CI_Controller {	

	var $modul_ctn = '';

	function __construct(){
		parent::__construct();
		$this->mn_active 	= 'action jurnal';
		if (isset($_GET['mhs'])) {
			$this->mn_active 	= 'Publikasi Mahasiswa';
		}
	}

	function index($page="0",$query=""){
		$this->get_criteria();
		$num = $this->db->get();
		$this->get_criteria();
		$this->db->limit(5,$page);
		$query = $this->db->get();


		$data['main']		= "jurnal_index";
		$data['kategori']	= $this->db->get_where('jr_kategori',array('status'=>1));
		$data['prodi']		= $this->db->get_where('jr_prodi',array('status'=>1));
		$data['tahun']		= $this->db->get('v_terbitan');
		$data['cur_page']	= $page;
		
		$data['ds']			= $query;

		$this->load->library('pagination');
		$config['suffix'] 	= "?".$_SERVER['QUERY_STRING'];
		$config['base_url'] = base_url().'jurnal/index/';
		$config['total_rows'] = $num->num_rows();
		$config['per_page'] = 5; 
		$config['first_url'] = $config['base_url'].$config['suffix']; 
		$this->pagination->suffix = "?".$_SERVER['QUERY_STRING'];
		$this->pagination->initialize($config); 

		$data['pagin']		= $this->pagination->create_links();

		$this->load->view('template',$data);
	}

	function get_criteria(){
		// query
		$this->db->select('*');
		$this->db->from('v_jurnal');

		if (isset($_GET['q']) && $_GET['q'] != '') 
			$this->db->like('judul',$_GET['q']);

		if (isset($_GET['kategori']) && $_GET['kategori'] != '') 
			$this->db->where('kategori_id',$_GET['kategori']);

		if (isset($_GET['prodi']) && $_GET['prodi'] != '') 
			$this->db->where('prodi_id',$_GET['prodi']);

		if (isset($_GET['tahun']) && $_GET['tahun'] != '') 
			$this->db->where('terbitan',$_GET['tahun']);
	}

	function detail($id){
		$data['main']		= "jurnal_detail";
		$data['ds']			= $this->db->get_where('v_jurnal',array('id'=>$id))->row();
		$data['user']		= $this->db->get_where('jr_users',array('id'=>$data['ds']->user_id))->row();
		$this->load->view('template',$data);
	}
}