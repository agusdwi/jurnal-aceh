<div class="container">
	<div class="row">
		<div class="col-md-8">
			<div class="widget-item">				
				<div class="widget-inner">
					<div class="course-search">
						<h3>Cari Jurnal</h3>

						<form action="<?=base_url('jurnal/index/0/')?>" method="get" class="course-search-form">
							<input name="q" id="q" class="searchbox" placeholder="Masukkan judul atau kata kunci " autocomplete="off" size="" title="Judul Jurnal" value="<?=isset($_GET['q'])?$_GET['q']:'';?>">
							<div id="search_advanced" class="clearfix">
								<p class="search-form-item">
									<label class="alabel" for="Campus">Kategori : </label>
									<select class="searchselect" id="kategori" name="kategori">
										<option value="">-- semua kategori --</option>
										<?foreach ($kategori->result() as $key): ?>
											<option <?=(isset($_GET['kategori']) && $_GET['kategori'] == $key->id)?'selected="selected"':'';?> value="<?=$key->id;?>"><?=$key->nama;?></option>	
										<?endforeach;?>
									</select>
								</p>
								<p class="search-form-item">
									<label class="alabel" for="Campus">Prodi : </label>
									<select class="searchselect" id="prodi" name="prodi">
										<option value="">-- semua prodi --</option>
										<?foreach ($prodi->result() as $key): ?>
											<option <?=(isset($_GET['prodi']) && $_GET['prodi'] == $key->id)?'selected="selected"':'';?> value="<?=$key->id;?>"><?=$key->nama;?></option>	
										<?endforeach;?>
									</select>
								</p>
								<p class="search-form-item">
									<label class="alabel" for="Campus">Tahun Terbit : </label>
									<select class="searchselect" id="tahun" name="tahun">
										<option value="">-- semua tahun --</option>
										<?foreach ($tahun->result() as $key): ?>
											<option <?=(isset($_GET['tahun']) && $_GET['tahun'] == $key->terbitan)?'selected="selected"':'';?> value="<?=$key->terbitan;?>"><?=$key->terbitan;?></option>	
										<?endforeach;?>
									</select>
								</p>
							</div>
							<input style="margin-top:10px;margin-bottom:-15px;" class="mainBtn pull-right" value="Cari Jural" type="submit">
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
			<div class="widget-item">
				<div class="widget-inner">
					<h3>Jurnal</h3><br>

					<?if ($ds->num_rows() == 0): ?>
						<div class="well">
							<center>Tidak ada jurnal pada kategori ini, silahkan mencari pada kategori lain</center>
						</div>
					<?endif;?>
					
					<table class="table table-striped" id="table-jurnal">
						<?$i=1*$cur_page;foreach ($ds->result() as $key): $i++;?>
							<tr>
								<td style="width:50px"><?=$i;?></td>
								<td style="width:100px">
									<a href="<?=base_url()?>jurnal/detail/<?=$key->id;?>/<?=url_title($key->judul);?>">
										<img src="<?=get_cover($key->cover);?>" width=50></td>
									</a>
								<td>
									<a style="font-size:16px" href="<?=base_url()?>jurnal/detail/<?=$key->id;?>/<?=url_title($key->judul);?>">
										<b><?=$key->judul;?> (<?=$key->terbitan;?>)</b>
									</a><br>
									<b><?=$key->kategori;?></b><br>
									<p><?=substr($key->deskripsi, 0,200);?></p>
								</td>
								<td style="width:50px !important">
									<a type="button" href="<?=base_url()?>jurnal/detail/<?=$key->id;?>/<?=url_title($key->judul);?>" class="btn btn-primary btn-sm" style="margin-top:20px">detail</a>
								</td>
							</tr>
						<?endforeach;?>
					</table>
					<div class="pagination">
						<?=$pagin;?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<?=$this->load->view('widget/jr_kategori');?>		
			<?=$this->load->view('widget/jr_prodi');?>		
			<?=$this->load->view('widget/jr_tahun');?>		
		</div>
	</div>
</div>