<div class="container">
	<div class="row">
		<div class="col-md-8">
			<div class="widget-item">
				
				<table class="table table-striped" id="data-jurnal">
					<tr>
						<td rowspan="5" style="width:130px">
							<img id="cover" src="<?=get_cover($ds->cover);?>">
						</td>
						<td style="width:140px">Judul</td>
						<td>:&nbsp;&nbsp;<b><?=$ds->judul;?></b></td>
					</tr>
					<tr>
						<td>Tahun Terbit</td>
						<td>:&nbsp;&nbsp;<?=$ds->terbitan;?></td>
					</tr>
					<tr>
						<td>Kategori</td>
						<td>:&nbsp;&nbsp;<?=$ds->kategori;?></td>
					</tr>
					<tr>
						<td>Penulis</td>
						<td>:&nbsp;&nbsp;<?=$user->nama;?></td>
					</tr>
					<tr>
						<td>Deskripsi</td>
						<td>:&nbsp;&nbsp;<?=$ds->deskripsi;?></td>
					</tr>
				</table>
				<?if (!is_login()): ?>
					<div class="well">
						<center>
							Maaf anda harus login untuk melihat jurnal ini <br>
							<a href="<?=base_url('user/login')?>">login</a> atau <a href="<?=base_url('user/register')?>">register</a>
						</center>
					</div>
				<?else: ?>	
					<a target="_blank" type="button" href="<?=base_url()?>media/jurnal/<?=$ds->url;?>" class=" btn btn-primary btn-sm pull-right">download</a>
					<div class="clearfix"></div>
					<iframe id="preview" src="<?=base_url()?>media/jurnal/<?=$ds->url;?>"></iframe>
				<?endif;?>
			</div>
		</div>
		<div class="col-md-4">
			<?=$this->load->view('widget/pencarian');?>		
			<?=$this->load->view('widget/jr_kategori');?>		
			<?=$this->load->view('widget/jr_tahun');?>				
		</div>
	</div>
</div>