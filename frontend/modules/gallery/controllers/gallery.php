<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {	

	var $modul_ctn = '';

	function __construct(){
		parent::__construct();
		$this->mn_active 	= 'gallery';
	}

	function index(){
		$data['main']	= "index";
		$data['ds']		= $this->db->get('gs_gallery');
		$this->load->view('template',$data);		
	}
}