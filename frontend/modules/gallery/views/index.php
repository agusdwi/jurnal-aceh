<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="widget-item">               
                <h3>Gallery</h3>
            </div>

            <div id="Grid">
                <?foreach ($ds->result() as $key): ?>
                    <div class="col-md-4 mix students mix_all" data-cat="3" style="display: inline-block; opacity: 1;">
                        <div class="gallery-item">
                            <a title="<?=$key->gallery_description;?>" class="fancybox" rel="gallery1" href="<?=base_url()?>media/gallery/<?=$key->gallery_url;?>">
                                <div class="gallery-thumb">
                                    <img src="<?=base_url()?>media/gallery/thumbs/<?=$key->gallery_url;?>" alt="">
                                </div>
                                <div class="gallery-content">
                                    <p class="small-text"><?=$key->gallery_description;?></p>
                                </div>
                            </a>
                        </div>
                    </div>
                <?endforeach;?>
            </div>
        </div>
        <div class="col-md-4">
            <?=$this->load->view('widget/jr_kategori');?>       
            <?=$this->load->view('widget/jr_prodi');?>      
            <?=$this->load->view('widget/jr_tahun');?>      
        </div>
    </div>
</div>