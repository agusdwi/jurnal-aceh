<style type="text/css">
	label.error{
		display: none !important;
	}
	input.error{
		float: none !important;
	}
</style>
<?
	$mn_samping1 = array(
		array('Tentang Kami','site/tentang-kami'),
		array('Berlangganan','site/berlangganan'),
		array('Kirim Naskah ke Action ','site/kirim-naskah-ke-action'),
		array('Download Full Text ','site/download-full-text'),
		array('Hubungi Kami ','site/hubungi-kami')
	);
	$mn_tentang = array(
		array('Profil','site/profil'),
		array('Tim Penyusun dan Mitra','site/tim-penyusun-dan-mitra'),
		array('Pedoman Penulisan','site/pedoman-penulisan'),
		array('Sekretariat','site/sekretariat')
	);
?>
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<div class="widget-main">
				<div class="widget-main-title">
					<h4 class="widget-title">Menu</h4>
				</div>
				<div class="widget-inner">
					<ul class="mixitup-controls">
						<?foreach ($mn_samping1 as $key): ?>
							<li class="filter">
								<a href="<?=base_url()?><?=$key[1];?>"><?=$key[0];?></a>
								<?if ($key[0]=='Tentang Kami'): ?>
									<ul class="sub-menu">
										<?foreach ($mn_tentang as $k): ?>
											<li>
												<a href="<?=base_url()?><?=$k[1];?>"><?=$k[0];?></a>
											</li>
										<?endforeach;?>
									</ul>
								<?endif;?>
							</li>
						<?endforeach;?>
					</ul>
				</div>
			</div>
			<div class="widget-main">
				<div class="widget-main-title">
					<h4 class="widget-title">Edisi</h4>
				</div>
				<div class="widget-inner">
					<ul class="mixitup-controls">
						<li class="filter">
							<a href="<?=base_url()?>jurnal">Tahun Terakhir</a>
						</li>
						<?foreach ($terbitan->result() as $key): ?>
							<li class="filter">
								<a href="<?=base_url()?>jurnal/index/0?q=&kategori=&prodi=&tahun=<?=$key->terbitan;?>">Edisi <?=$key->terbitan;?></a>
							</li>
						<?endforeach;?>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="main-slideshow">
				<div class="flexslider"  style="height:250px">
					<ul class="slides">
						<?foreach ($slide->result() as $key): ?>
							<li>
								<img style="height:245px" src="<?=base_url()?>media/slideshow/<?=$key->ss_img;?>" />
								<div class="slider-caption">
									<h2><a href="<?=$key->ss_link;?>"><?=$key->ss_title;?></a></h2>
									<p><?=$key->ss_desc;?></p>
								</div>
							</li>	
						<?endforeach;?>
					</ul>
				</div>
			</div>
			<div class="widget-item">
				<?=$welcome;?>
			</div>
		</div>
		<div class="col-md-3">
			
			<?=$this->load->view('widget/pencarian');?>
			
			<div class="widget-item">
				<div class="request-information">
					<h4 class="widget-title">Langganan</h4>
					<?if (is_login()): ?>
						<hr>
						<b>Hi, <?=get_user('nama');?></b><br>
						<div style="margin-top:10px">
							<b><a href="<?=base_url('member')?>">Halaman Member</a> | <a href="<?=base_url('user/logout')?>">Logout</a></b>
						</div>
					<?else:?>
						<?=form_open('user/login',array('class'=>'request-info clearfix login'))?>
							<div class="full-row">
								<label for="yourname">Email:</label>
								<input type="text" id="yourname" name="email">
							</div>
							<div class="full-row">
								<label for="yourname">Password:</label>
								<input type="password" id="yourname" name="password">
							</div>
							<div class="full-row">
								<div style="float:left">
									<i>tidak punya akun ?</i><br> <a href="<?=base_url('user/register')?>"><b>daftar</b></a>
								</div>
								<div class="submit_field">
									<input class="mainBtn pull-right" type="submit" name="" value="login">
								</div>
							</div>
						</form>
					<?endif;?>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("form.login").validate({
			rules: {
				email		: "required",
				password	: "required"
			},
			errorLabelContainer: $("div.error-container")
		});
	})
</script>