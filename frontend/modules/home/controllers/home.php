<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {	

	var $modul_ctn = '';

	function __construct(){
		parent::__construct();
		$this->mn_active = 'home';
	}

	function index($page="0"){
		$data['main']		= "index";
		$data['slide']		= $this->db->get('app_slideshow');
		$data['terbitan']	= $this->db->get('v_terbitan');
		$data['welcome']	= $this->db->get_where('gs_config',array('cog_id'=>1))->row()->cog_value;
		$this->load->view('template',$data);
	}

	function stat(){
		$this->load->view('include/stat');
	}
}