<div class="page-title">
    <div class="container">
        <div class="twelve columns">
            <h1>Contact Us</h1>
        </div>
        <nav class="four columns">
            <ul class="breadcrumbs">
                <li><a href="<?=base_url()?>">Home</a></li>
                <li><i class="icon-angle-right"></i></li>
                <li>Contact</li>
            </ul>
        </nav>        
    </div>
</div>
<section class="contact">
    <div class="container">
        <div class="four columns">
            <div class="contact-sidebar">
                <?=$tpl;?>  
            </div>
        </div>
        <div class="twelve columns">
            <div id="google-map">
                <?=$map;?>
            </div>
            <div class="form">
                <div class="alert success success-message">
                    <div class="close">×</div>
                    <p>Your message has been sent!</p>
                </div>
                <form class="clearfix" method="post" action="<?=base_url('contact')?>">
                    <div class="field">
                        <label>Name <span>*</span></label>
                        <input type="text" name="name" class="text" value="" required="required">
                    </div>
                    <div class="field">
                        <label>Email <span>*</span></label>
                        <input type="email" name="email" class="text" value="" required="required">
                    </div>                    
                    <div class="field field-last">
                        <label>Subject <span>*</span></label>
                        <input type="text" name="subject" class="text" value="" required="required">
                    </div>
                    <textarea required="required" name="msg" class="text"></textarea>
                    <button id="send" class="btn">Submit</button>
                    <div class="loading"></div>
                </form>
            </div>
        </div>
    </div>
</section>
<? $a = $this->session->flashdata('message');?>
<?if(!empty($a)):?>
    <script type="text/javascript">
        alert("<?=$a;?>")
    </script>
<?endif;?>