<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {	

	var $modul_ctn = '';

	function __construct(){
		parent::__construct();

		$menu = $this->db->get_where('gs_menu',array('menu_type'=>'modul','menu_link'=>'contact'));
		$this->cur_menu = ($menu->num_rows() == 1) ? $menu->row()->menu_id : 0 ;
	}

	function index(){
		if (is_post()) {
			$data = $this->input->post();
			$this->db->insert('app_contact',$data);
			$this->session->set_flashdata('message','Terimakasih , kami akan segera menghubungi anda');
			redirect(base_url('contact'));
		}
		$data['main']	= "index";
		$data['tpl']	= $this->db->get_where('gs_config',array('cog_id'=>4))->row()->cog_value;
		$data['map']	= $this->db->get_where('gs_config',array('cog_id'=>5))->row()->cog_value;
		$this->load->view('template',$data);
	}
}