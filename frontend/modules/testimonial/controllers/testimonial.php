<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonial extends CI_Controller {	

	var $modul_ctn = '';

	function __construct(){
		parent::__construct();

		$menu = $this->db->get_where('gs_menu',array('menu_type'=>'modul','menu_link'=>'berita'));
		$this->cur_menu = ($menu->num_rows() == 1) ? $menu->row()->menu_id : 0 ;
	}

	function index(){
		if (is_post()) {
			$data = $this->input->post();
			$this->db->insert('app_testi',$data);
			$this->session->set_flashdata('message','Terimakasih , telah memberikan testimonial');
			redirect(base_url('testimonial'));
		}

		$data['main']	= "index";
		$data['vtitle']	= "Testimonial";
		$data['ds']		= $this->db->order_by('t_id','desc')->limit(5,$i)->get_where('app_testi',array('t_status'=>1));
		$data['num']	= $this->db->get_where('app_testi',array('t_status'=>1))->num_rows();
		$this->load->view('template',$data);
	}

	function loadmore($i=0){
		$ds		= $this->db->order_by('t_id','desc')->limit(1,$i)->get_where('app_testi',array('t_status'=>1));
		if ($ds->num_rows() > 0) {
			$d = $ds->result();
			foreach ($ds->result() as $key => $val) {
				$d[$key]->t_date = pretty_date($val->t_date,false);
			}
			echo json_encode(array('st'=>1,'dt'=>$ds->result()));
		}else echo json_encode(array('st'=>0));
	}
}