<div class="page-title">
    <div class="container">
        <div class="twelve columns">
            <h1><?=$vtitle;?></h1>
        </div>
        <nav class="four columns">
            <ul class="breadcrumbs">
                <li><a href="<?=base_url()?>">Home</a></li>
                <li><i class="icon-angle-right"></i></li>
                <li><?=$vtitle;?></li>
            </ul>
        </nav>
    </div>
</div>
<section class="blog">
    <div class="container">    
        <div class="three_fourth column">
            <ul class="accordion" id="testi">
                <?foreach ($ds->result() as $key): ?>
                    <li>
                        <a class="active" href="#"><b style="font-size:18px"><?=$key->t_name;?></b> ,<small><?=$key->t_company;?></small> <span><?=pretty_date($key->t_date,false);?></span></a>
                        <p class="expand" style="display: block;"><?=$key->t_msg;?></p>
                    </li>
                <?endforeach ?>
            </ul>
            <?if ($ds->num_rows() < $num): ?>
                <center>
                    <a class="btn grey" id="loadmore" href="<?=base_url('testimonial/loadmore')?>">loadmore</a>
                </center>
            <?endif ?>
        </div>
        <div class="one_fourth column">
            <div id="sidebar">
                <div class="widget">
                    <h3>Submit Testimonial</h3>
                    <form class="<?=base_url('testimoni')?>" method="post" action="#">

                        <div class="field">
                            <label for="name"><small>Name <span>*</span></small></label>
                            <input type="text" name="t_name" class="text" value="" required="required">
                        </div>

                        <div class="field">
                            <label for="email"><small>Company <span>*</span></small></label>
                            <input type="text" name="t_company" class="text" value="" required="required">
                        </div>

                        <div class="field">
                            <label for="website"><small>Testimonial <span>*</span></small></label>
                            <textarea name="t_msg" class="text textarea" rows="5" style="height:100px" required="required"></textarea>
                        </div>
                        
                        <br>
                        <button id="send" class="btn">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var tot = <?=$num;?>;
    var cur = 0;
    $(function(){
        $("#loadmore").click(function(){
            cur+=5;
            url = $(this).attr('href')+'/'+cur;
            $.getJSON(url, function(data) {
                var items = data['dt'];
                $.each(items, function(key, val) {
                    $("<li></li>")
                        .html("<a class='active' href='#'><b style='font-size:18px'>"+val.t_name+"</b> ,<small>"+val.t_company+"</small> <span>"+val.t_date+"</span></a><p class='expand' style='display: block;'>"+val.t_msg+"</p>")
                        .appendTo($("#testi"));
                });
            });
            if ((cur + 1) >= tot) {
                $(this).remove();
            };
            return false;
        })
    })
</script>
<style type="text/css">
    .accordion li span{float:right;font-size: 12px;margin-top: 5px}
</style>

<? $a = $this->session->flashdata('message');?>
<?if(!empty($a)):?>
    <script type="text/javascript">
        alert("<?=$a;?>")
    </script>
<?endif;?>