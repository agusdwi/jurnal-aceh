<div class="page-title">
    <div class="container">

        <div class="twelve columns">
            <h1><?=$ds->art_judul;?></h1>
        </div>
    </div>
</div>

<section class="blog">
    <div class="container">
        <div class="three_fourth column">
            <!-- Post Start -->
            <article class="post">
                <?=$ds->art_content;?>        
            </article>    
        </div>
        <?=$this->load->view('include/sidebar');?>    
    </div>
</section>