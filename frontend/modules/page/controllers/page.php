<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {	

	var $modul_ctn = '';

	function __construct(){
		parent::__construct();

		$menu = $this->db->get_where('gs_menu',array('menu_type'=>'artikel','menu_link'=>'page/'.$this->uri->segment(2)));
		$this->cur_menu = ($menu->num_rows() == 1) ? $menu->row()->menu_id : 0 ;
	}

	function index($link){
		$data['main']	= "index";
		$data['ds']		= $this->db->get_where('gs_artikel',array('art_link'=>$link))->row();
		$this->load->view('template',$data);
	}
}