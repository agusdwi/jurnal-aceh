<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berita extends CI_Controller {	

	var $modul_ctn = '';

	function __construct(){
		parent::__construct();
		$this->mn_active 	= 'berita';
	}

	function index($i=0){
		$this->load->library('pagination');

		$config['base_url'] 	= base_url('berita');
		$config['total_rows'] 	= $this->db->get('gs_berita')->num_rows();
		$config['per_page'] 	= 5; 
		$config['uri_segment']	= 2; 

		$this->pagination->initialize($config); 

		$data['pagin']	= $this->pagination->create_links();

		$data['main']	= "index";
		$data['vtitle']	= "News Room";
		$data['ds']		= $this->db->order_by('berita_id','desc')->limit($config['per_page'],$i)->get('gs_berita');
		$this->load->view('template',$data);
	}

	function read($link){
		$data['main']	= "read";
		$data['ds']		= $this->db->get_where('gs_berita',array('berita_link'=>$link))->row();
		$data['vtitle']	= $data['ds']->berita_judul;
		$data['kategori']= $this->db->get_where('gs_berita_kategori',array('berita_id'=>$data['ds']->berita_id));
		$this->load->view('template',$data);	
	}

	function kategori($link,$i=0){
		$dlink = rawurldecode($link);	
		$this->load->library('pagination');

		$config['base_url'] 	= base_url("berita/kategori/$link");
		$config['total_rows'] 	= $this->db->get_where('v_berita_kategori',array('kategori'=>$link))->num_rows();
		$config['per_page'] 	= 5; 
		$config['uri_segment']	= 4; 

		$this->pagination->initialize($config); 

		$data['pagin']	= $this->pagination->create_links();

		$data['main']	= "index";
		$data['vtitle']	= "Kategori : $dlink";
		$data['ds']		= $this->db->order_by('berita_id','desc')->limit($config['per_page'],$i)->get_where('v_berita_kategori',array('kategori'=>$link));
		$this->load->view('template',$data);
	}
}