<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="widget-item">
                <h3 class="blog-post-title">Berita</h3>
            </div>    
            
            <?foreach ($ds->result() as $key): ?>
                <?
                    $link = base_url('berita/read/'.$key->berita_link);
                    $img  = base_url('media/blog/thumbs/'.$key->berita_img);
                    $title = $key->berita_judul;
                    $date = $key->berita_date;
                ?>
                <div class="list-event-item">
                    <div class="box-content-inner clearfix">
                        <div class="list-event-thumb">
                            <a href="event-single.html">
                                <img src="<?=$img;?>" alt="">
                            </a>
                        </div>
                        <div class="list-event-header">
                            <span class="event-place small-text"><i class="fa fa-user"></i><?=$key->berita_penulis;?></span>
                            <span class="event-date small-text"><i class="fa fa-calendar-o"></i><?=pretty_date($key->berita_date,false);?></span>
                            <div class="view-details"><a href="<?=$link;?>" class="lightBtn">View Details</a></div>
                        </div>
                        <h5 class="event-title"><a href="<?=$link;?>"><?=$title;?></a></h5>
                        <p><?=smart_trim($key->berita_isi,200)?></p>
                    </div>
                </div>

            <?endforeach;?>
            <br>
            <div class="pagination">
                <?=$pagin;?>
            </div>
                
        </div>
        <div class="col-md-4">
            <?=$this->load->view('widget/pencarian');?>     
            <?=$this->load->view('widget/jr_kategori');?>       
            <?=$this->load->view('widget/jr_tahun');?>              
        </div>
    </div>
</div>