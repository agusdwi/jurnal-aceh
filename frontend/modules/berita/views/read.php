<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="blog-post-container">
                <?if ($ds->berita_img != ''): ?>
                    <div class="blog-post-image">
                        <img src="<?=base_url()?>media/blog/<?=$ds->berita_img;?>" alt="">
                        <div class="blog-post-meta">
                            <ul>
                                <li><i class="fa fa-calendar-o"></i><?=pretty_date($ds->berita_date,false);?></li>
                                <li><a href="blog-single.html#blog-author"><i class="fa fa-user"></i><?=$ds->berita_penulis;?></a></li>
                            </ul>
                        </div>
                    </div>
                <?endif;?>

                <div class="blog-post-inner">
                    <h3 class="blog-post-title"><?=$vtitle;?></h3>
                    <?=$ds->berita_isi;?>
                    <br>
                    <?=$this->load->view('widget/share');?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <?=$this->load->view('widget/pencarian');?>     
            <?=$this->load->view('widget/jr_kategori');?>       
            <?=$this->load->view('widget/jr_tahun');?>              
        </div>
    </div>
</div>