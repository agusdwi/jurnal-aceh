<?
	$mn_member = array(
		array('Dashboard','member',1),
		array('Profil','member/profil',2),
		array('Upload Jurnal','member/jurnal_add',3),
		array('Jurnal Saya','member/jurnal',4),
		array('Logout','user/logout',5)
	);
?>

<div class="widget-main">
	<div class="widget-main-title">
		<h4 class="widget-title">Menu Utama</h4>
	</div>
	<div class="widget-inner">
		<div id="member-photo">
			<img src="<?=get_avatar(get_user('avatar'));?>">
		</div>
		<div id="member-profil">
			<ul id="data-diri">
				<li><b><?=get_user('nama');?></b></li>
				<li><b><?=get_user('email');?></b></li>
				<li><b>[<?=strtoupper(get_user('type'));?>]</b></li>
			</ul>
		</div>
		<div class="clearfix"></div>
		<hr>
		<ul class="mixitup-controls">
			<?foreach ($mn_member as $key): ?>
				<li class="filter">
					<a href="<?=base_url()?><?=$key[1];?>"><?=$key[0];?></a>
				</li>
			<?endforeach;?>
		</ul>
	</div>
</div>