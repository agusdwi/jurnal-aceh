<style type="text/css">
	label.error{
		display: none !important;
	}
	input.error{
		float: none !important;
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<?=$this->load->view('_member_sidebar');?>
		</div>
		<div class="col-md-8">
			<div class="widget-item">
				<h3>Upload Jurnal</h3><br>	
				<div class="request-information">
					<p>Silahkan upload jurnal, tipe yang diterima berformat <b>PDF</b> dengan ukuran maksimal 10mb</p>
					<hr>
					<?=form_open_multipart('',array('id'=>'frm_jurnal','class'=>'request-info clearfix no-error'))?>
						<div class="row">
							<div class="col-md-6">
								<label for="judul">Judul Jurnal:</label>
								<input type="text" id="judul" name="judul" value="">
							</div>
							<div class="col-md-6">
								<label for="terbitan">Terbitan Tahun:</label>
								<input type="text" id="terbitan" name="terbitan" value="">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label for="kategori_id">Katgori Jurnal:</label>
								<div class="input-select">
									<select name="kategori_id" id="kategori_id" class="postform">
										<option value="">-- select --</option>
										<?foreach ($kategori->result() as $key): ?>
											<option value="<?=$key->id;?>"><?=$key->nama;?></option>
										<?endforeach;?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<label for="terbitan">Penulis:</label>
								<input type="text" id="terbitan" value="<?=get_user('nama');?>" disabled>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label for="deskripsi">Deskripsi singkat:</label>
								<input type="text" name="deskripsi" id="deskripsi" value="">
							</div>
						</div>
						<div class="row">
							<b style="padding-left:20px">Berkas Dokumen</b><br><div class="clearfix"></div>
							<div class="col-md-6">
								<label for="pdf">Dokumen Jurnal (PDF):</label>
								<input type="file" name="pdf" id="pdf" value="">
							</div>
							<div class="col-md-6">
								<label for="deskripsi">Cover Jurnal (JPG) / optional: </label>
								<input type="file" name="cover" id="cover" value="">
							</div>
						</div>
						<div class="full-row">
							<div class="submit_field">
								<input id="submit" class="mainBtn pull-right" type="submit" name="" value="Submit Jurnal">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("form").validate({
			rules: {
				judul 		: "required",
				terbitan 	: {number:true,required:true},
				kategori_id : "required",
				deskripsi 	: "required",
				pdf 		: "required"
			},submitHandler: function(form) {
				$("<div id='loading_block'>loading</div>").appendTo("body");
				form.submit();
			}
		});	
	})
</script>