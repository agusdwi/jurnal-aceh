<div class="container">
	<div class="row">
		<div class="col-md-4">
			<?=$this->load->view('_member_sidebar');?>
		</div>
		<div class="col-md-8">
			<div class="widget-item">
				<h3>Jurnal Saya</h3><br>	
				
				<table class="table table-striped">
					<?$i=0;foreach ($ds->result() as $key): $i++;?>
						<tr>
							<td><?=$i;?></td>
							<td>
								<a href="<?=base_url()?>jurnal/detail/<?=$key->id;?>/<?=url_title($key->judul);?>">
									<img src="<?=get_cover($key->cover);?>" width=50></td>
								</a>
							<td>
								<a style="font-size:16px" href="<?=base_url()?>jurnal/detail/<?=$key->id;?>/<?=url_title($key->judul);?>">
									<b><?=$key->judul;?> (<?=$key->terbitan;?>)</b>
								</a><br>
								<b><?=$key->kategori;?></b><br>
								<p><?=substr($key->deskripsi, 0,200);?></p>
							</td>
							<td style="width:150px !important">
								<a type="button" href="<?=base_url()?>media/jurnal/<?=$key->url;?>" class="fancybox_iframe btn btn-primary btn-sm">preview</a>
								<a href="<?=base_url()?>member/jurnal_delete/<?=$key->id;?>" type="button" class="delete btn btn-danger btn-sm">hapus</a>
							</td>
						</tr>
					<?endforeach;?>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('body').on('click','.delete',function (e) {
			return confirm('apakah anda yakin menghapus jurnal ini ?');
		});
	})
</script>