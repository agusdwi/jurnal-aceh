<div class="container">
	<div class="row">
		<div class="col-md-4">
			<?=$this->load->view('_member_sidebar');?>
		</div>
		<div class="col-md-8">
			<div class="widget-item">
				<h3>Data Diri</h3><br>	
				<div class="request-information">
					<?=form_open_multipart('',array('id'=>'frm_profil','class'=>'request-info clearfix no-error'))?>
						<div class="row">
							<div class="col-md-5">
								<center><img src="<?=get_avatar(get_user('avatar'));?>" style="margin-top:-10px;width:75px;margin-bottom:10px"></center>
								<input type="file" name="userfile" style="float:left;width:100%">
							</div>
							<div class="col-md-7">
								<div class="full-row">
									<label for="yourname">Nama Lengkap:</label>
									<input type="text" id="nama" name="nama" value="<?=$ds->nama;?>">
								</div>
								<div class="full-row">
									<label for="yourname">Email:</label>
									<input type="text" id="email" name="email" value="<?=$ds->email;?>">
								</div>
							</div>
						</div>
						<div class="full-row">
							<div class="submit_field">
								<input class="mainBtn pull-right" type="submit" name="" value="Update Profil">
							</div>
						</div>
					</form>
					<hr>
					<h3>Ubah Password</h3><br>
					<?=form_open('member/password',array('id'=>'password','class'=>'request-info clearfix no-error'))?>
						<div class="row">
							<div class="col-md-6">
								<div class="full-row">
									<label for="yourname">Password Lama:</label>
									<input type="password" id="old" name="old">
								</div>
							</div>
							<div class="col-md-6">
								<div class="full-row">
									<label for="yourname">Email:</label>
									<input type="password" id="new" name="new">
								</div>
							</div>
						</div>
						<div class="full-row">
							<div class="submit_field">
								<input class="mainBtn pull-right" type="submit" name="" value="Kirim">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#password").validate({
			rules: {
				old	: "required",
				new	: "required"
			}
		});
		$("#frm_profil").validate({
			rules: {
				nama	: "required",
				email	: "required"
			}
		});

	})
</script>