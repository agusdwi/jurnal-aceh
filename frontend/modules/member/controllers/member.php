<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {	

	var $modul_ctn = '';

	function __construct(){
		parent::__construct();
	}

	function index($page="0"){
		$data['main']		= "member_index";
		$this->load->view('template',$data);
	}

	function profil(){
		if (is_post()) {
			$post = $this->input->post();
			if($_FILES['userfile']['name'] != ""){
				$post['avatar'] = $this->upload();
			}

			if (get_user('email') != $post['email']) {
				if ($this->db->get_where('jr_users',array('email'=>$post['email']))->num_rows()>0) {
					$this->session->set_flashdata('message','Email sudah dipakai, silahkan pakai yang lain');
					redirect('member/profil');
				}
			}

			$this->db->where('id', get_user('id'));
			$this->db->update('jr_users', $post); 
			$this->session->set_flashdata('message','Profil berhasil di perbarui');

			$new_session = $this->db->get_where('jr_users',array('id'=>get_user('id')));
			$new_session = $new_session->result_array();
			$new_session = $new_session[0];
			sess_register($new_session);

			redirect('member/profil');
		}
		$data['main']		= "member_profil";
		$data['ds']			= $this->db->get_where('jr_users',array('id'=>get_user('id')))->row();
		$this->load->view('template',$data);	
	}

	function password(){
		$post = $this->input->post();
		$ds = $this->db->get_where('jr_users',array('id'=>get_user('id')))->row();
		if (md5(encryptkey().$post['old']) == $ds->password) {
			$data = array('password' =>  md5(encryptkey().$post['new']));
			$this->db->where('id',get_user('id'));
			$this->db->update('jr_users', $data);
			$this->session->set_flashdata('message','password berhasil di perbarui');
		}else{
			$this->session->set_flashdata('message','password lama salah, silahkan ulangi');
		}
		redirect('member/profil');
	}

	function upload(){
		$_FILES['userfile']['name']	= strtolower($_FILES['userfile']['name']);
		$config['upload_path']			= 'media/avatar/';
		$config['allowed_types']		= 'jpg';
		$config['max_size']				= '10000';
		$config['max_width']			= '800';
		$config['max_height']			= '1024';
		$config['encrypt_name']			= true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload()){
			$this->session->set_flashdata('message','maaf foto gagal di upload, foto bertipe jpg, ukuran kurang dari 1mb');
			redirect('member/profil');
		}else{
			$a = $this->upload->data();
			$this->load->library('image_moo');
			$this->image_moo
				->load('media/avatar/'.$a['file_name'])
				->resize_crop(150,200)
				->save('media/avatar/'.$a['file_name'],true);
			return $a['file_name'];
		}
	}

	function jurnal_add(){
		if (is_post()) {
			$this->load->library('upload');
			$post 			= $this->input->post();
			$post['url'] 	= $this->upload_pdf($post);
			$post['user_id']= get_user('id');
			$cover 			= $this->upload_cover();

			if ($cover[0]) {
				$post['cover']	= $cover[1];
			}
			$this->db->insert('jr_jurnal',$post);	
			$this->session->set_flashdata('message','jurnal berhasil di upload');
			redirect('member/jurnal_add');
		}
		
		$data['main']		= "member_jurnal_add";
		$data['kategori']	= $this->db->order_by('sort')->get_where('jr_kategori',array('status'=>1));
		$this->load->view('template',$data);	
	}

	function upload_pdf($post){
		$_FILES['pdf']['name']			= strtolower($_FILES['pdf']['name']);
		$config['upload_path']			= 'media/jurnal/';
		$config['allowed_types']		= 'pdf';
		$config['max_size']				= '10000';
		$config['encrypt_name']			= true;
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('pdf')){
			$this->session->set_flashdata('message','maaf document gagal di upload, hanya tipe pdf yang di ijinkan');
			$this->session->set_flashdata('form',$post);
			redirect('member/jurnal_add');
		}else{
			$a = $this->upload->data();
			return $a['file_name'];
		}
	}

	function upload_cover(){
		$_FILES['cover']['name']	= strtolower($_FILES['cover']['name']);
		$cfg['upload_path']		= 'media/jurnal_cover/';
		$cfg['allowed_types']	= 'jpg';
		$cfg['max_size']			= '10000';
		$cfg['max_width']		= '800';
		$cfg['max_height']		= '1024';
		$cfg['encrypt_name']		= true;
		$this->upload->initialize($cfg);
		if ( ! $this->upload->do_upload('cover')){
			return array(false,$this->upload->display_errors());
		}else{
			$a = $this->upload->data();
			$this->load->library('image_moo');
			$this->image_moo
				->load('media/jurnal_cover/'.$a['file_name'])
				->resize_crop(300,400)
				->save('media/jurnal_cover/'.$a['file_name'],true);
			return array(true,$a['file_name']);
		}
	}

	function jurnal(){
		$data['main']		= "member_jurnal";
		$data['ds']			= $this->db->order_by('terbitan','desc')->get_where('v_jurnal',array('status'=>1,'user_id'=>get_user('id')));
		$this->load->view('template',$data);			
	}

	function jurnal_delete($id){
		$data = array('status' => 0);
		$this->db->where('id', $id);
		$this->db->update('jr_jurnal', $data);
		$this->session->set_flashdata('message', 'data berhasil dihapus');
		redirect('member/jurnal');
	}
}