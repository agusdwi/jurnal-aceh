<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="widget-main">
				<div class="widget-main-title">
					<h4 class="widget-title">Pendaftaran Keanggotaan</h4>
				</div>
				<div class="widget-inner">
					<div class="request-information">
						<?=form_open('',array('class'=>'request-info clearfix'))?>
							<div class="full-row">
								<label for="cat">Tipe Anggota:</label>                
								<div class="input-select">
									<select name="type" id="type" class="postform">
										<option value="">-- select --</option>
										<option value="dosen">Dosen</option>
										<option value="mahasiswa">Mahasiswa</option>
									</select>
								</div>
							</div>
							<div class="full-row">
								<label for="yourname">Email:</label>
								<input type="text" id="email" name="email">
							</div>
							<div class="full-row">
								<label for="yourname">Password:</label>
								<input type="text" id="password" name="password">
							</div>
							<div class="full-row">
								<label for="yourname">Nama Lengkap:</label>
								<input type="text" id="nama" name="nama">
							</div>
							<div class="full-row">
								<div class="submit_field">
									<input class="mainBtn pull-right" type="submit" name="" value="Register">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("form").validate({
			rules: {
				email		: {required:true,email:true},
				password	: "required",
				nama		: "required",
				type		: "required"
			}
		});
	})
</script>