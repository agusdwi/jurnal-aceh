<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {	

	function __construct(){
		parent::__construct();
		$this->encrypt = $this->config->item('encryption_key');
	}

	function register(){
		if (is_post()) {
			$post = $this->input->post();
			$exist = $this->db->get_where('jr_users',array('email'=>$post['email']));
			$data['result']			= 'true';
			if ($exist->num_rows() > 0 ) {
				$data['result']		= 'false';
			}else{
				$post['password'] = md5($this->encrypt.$post['password']);
				$this->db->insert('jr_users',$post);	
			}
			$data['main']		= "user_register_msg";
			$this->load->view('template',$data);
		}else{
			$data['main']		= "user_register";
			$this->load->view('template',$data);
		}
	}

	function login(){
		if (is_login()) {redirect('member');}
		if (is_post()) {
			if(login_cek($this->input->post('email'),$this->input->post('password'))){
				$this->session->set_flashdata('message','selamat datang :D');
				redirect(base_url('member'));
			}else{
				$this->session->set_flashdata('message','maaf username / password salah');
				redirect(base_url('user/login'));
			}
		}
		$data['main']		= "user_login";
		$this->load->view('template',$data);
	}

	function logout(){
		logout();redirect();
	}
}