<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fnb extends CI_Controller {	

	var $modul_ctn = '';

	function __construct(){
		parent::__construct();

		$menu = $this->db->get_where('gs_menu',array('menu_type'=>'modul','menu_link'=>'fnb'));
		$this->cur_menu = ($menu->num_rows() == 1) ? $menu->row()->menu_id : 0 ;
	}

	function index(){
		$data['main']	= "index";
		$data['ds']		= $this->db->get('food_menu');
		$this->load->view('template',$data);
	}
}