<div id="outerbeforecontent">
    <div class="container">
        <div class="row">
            <div class="twelve columns">
                <div id="beforecontent">
                    <h1 class="pagetitle">Food & Beverages</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="separator full"></div>
</div>
<div id="outermain">
    <div class="container">
        <div class="row">
            <section id="maincontent" class="twelve columns">
                <section class="content">
                    <div class="carousel">
                        <div data-index="0" class="carousel-item-container row" >
                            <?$i=0;foreach ($ds->result() as $key): $i++;?>
                                <div class="one_fourth columns item">
                                    <div class="ts-img">
                                        <a class="image" href="<?=base_url()?>media/fnb/<?=$key->mn_img;?>" data-rel="prettyPhoto[mixed]" >
                                            <span class="rollover"></span>
                                            <span class="zoom"></span>
                                            <img alt="" src="<?=base_url()?>media/fnb/<?=$key->mn_img;?>"/>
                                        </a>                        
                                    </div>
                                    <div class="ts-text">
                                        <h2><?=$key->mn_nama;?></h2>
                                        <p><?=$key->mn_desc;?></p>
                                    </div>
                                </div>
                            <?endforeach;?>
                        </div>
                        <nav class="carousel-nav">
                            <a class="port-nav left"></a><a class="port-nav right"></a>
                        </nav>
                    </div>
                </section>
            </section>
        </div>
    </div>
</div>