<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {	

	var $modul_ctn = '';

	function __construct(){
		parent::__construct();
	}

	function index($id){
		$ds = $this->db->get_where('jr_static',array('url'=>$id));
		if ($ds->num_rows() != 1) {
			show_404();
		}
		
		$data['main']		= "site_detail";
		$data['ds']			= $ds->row();
		$this->mn_active 	= $data['ds']->caption;
		$this->load->view('template',$data);
	}
}