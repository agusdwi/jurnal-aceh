<div class="container">
	<div class="row">
		<div class="col-md-8">
			<div class="widget-item">
				
				<div class="blog-post-inner">
					<h3 class="blog-post-title"><?=$ds->judul;?></h3>
					<?=$ds->text;?>
				</div>	
			
			</div>
		</div>
		<div class="col-md-4">
			<?=$this->load->view('widget/pencarian');?>		
			<?=$this->load->view('widget/jr_kategori');?>		
			<?=$this->load->view('widget/jr_tahun');?>				
		</div>
	</div>
</div>