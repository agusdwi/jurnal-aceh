<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artikel extends CI_Controller {	

	var $modul_ctn = '';

	function __construct(){
		parent::__construct();
		$this->mn_active 	= 'artikel';
	}

	function index($i=0){
		$this->load->library('pagination');

		$config['base_url'] 	= base_url('artikel');
		$config['total_rows'] 	= $this->db->get('gs_artikel')->num_rows();
		$config['per_page'] 	= 5; 
		$config['uri_segment']	= 2; 

		$this->pagination->initialize($config); 

		$data['pagin']	= $this->pagination->create_links();
		$data['vtitle']	= "Artikel";
		$data['main']	= "index";
		$data['ds']		= $this->db->order_by('id','desc')->limit($config['per_page'],$i)->get('gs_artikel');
		$this->load->view('template',$data);
	}

	function read($link){
		$data['main']	= "read";
		$data['ds']		= $this->db->get_where('gs_artikel',array('link'=>$link))->row();
		$data['vtitle']	= $data['ds']->judul;
		$this->load->view('template',$data);	
	}
}